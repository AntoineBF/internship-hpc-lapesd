# **AWS Cloud Practitioner Essentials**

[***AWS Course link***](https://explore.skillbuilder.aws/learn/course/134/aws-cloud-practitioner-essentials)

ID: E-N0539V

## **Course objectives**

In this course, you will learn to:

 * Summarize the working definition of AWS
 * Differentiate between on-premises, hybrid-cloud, and all-in cloud
 * Describe the basic global infrastructure of the AWS Cloud
 * Explain the six benefits of the AWS Cloud
 * Describe and provide an example of the core AWS services, including compute, network, databases, and storage
 * Identify an appropriate solution using AWS Cloud services with various use cases
 * Describe the AWS Well-Architected Framework
 * Explain the shared responsibility model
 * Describe the core security services within the AWS Cloud
 * Describe the basics of AWS Cloud migration
 * Articulate the financial benefits of the AWS Cloud for an organization’s cost management
 * Define the core billing, account management, and pricing models
 * Explain how to use pricing tools to make cost-effective choices for AWS services


## **Prerequisites**

We recommend that attendees of this course have:- General IT business knowledge- General IT technical knowledge

## **Course outline**

[Module 1: Introduction to Amazon Web Services](#module-1-introduction)
```
    Summarize the benefits of AWS
    Describe differences between on-demand delivery and cloud deployments
    Summarize the pay-as-you-go pricing model
```

[Module 2: Compute in the Cloud](#module-2-introduction)
```
    Describe the benefits of Amazon Elastic Compute Cloud (Amazon EC2) at a basic level
    Identify the different Amazon EC2 instance types
    Differentiate between the various billing options for Amazon EC2
    Describe the benefits of Amazon EC2 Auto Scaling
    Summarize the benefits of Elastic Load Balancing
    Give an example of the uses for Elastic Load Balancing
    Summarize the differences between Amazon Simple Notification Service (Amazon SNS) and Amazon Simple Queue Services (Amazon SQS)
    Summarize additional AWS compute options
```

[Module 3: Global Infrastructure and Reliability](#module-3-introduction)
```
    Summarize the benefits of the AWS Global Infrastructure
    Describe the basic concept of Availability Zones
    Describe the benefits of Amazon CloudFront and Edge locations
    Compare different methods for provisioning AWS services
```

[Module 4: Networking](#module-4-introduction)
```
    Describe the basic concepts of networking
    Describe the difference between public and private networking resources
    Explain a virtual private gateway using a real life scenario
    Explain a virtual private network (VPN) using a real life scenario
    Describe the benefit of AWS Direct Connect
    Describe the benefit of hybrid deployments
    Describe the layers of security used in an IT strategy
    Describe which services are used to interact with the AWS global network
```

[Module 5: Storage and Databases](#module-5-introduction)
```
    Summarize the basic concept of storage and databases
    Describe benefits of Amazon Elastic Block Store (Amazon EBS)
    Describe benefits of Amazon Simple Storage Service (Amazon S3)
    Describe the benefits of Amazon Elastic File System (Amazon EFS)
    Summarize various storage solutions
    Describe the benefits of Amazon Relational Database Service (Amazon RDS)
    Describe the benefits of Amazon DynamoDB
    Summarize various database services
```

[Module 6: Security](#module-6-introduction)
```
    Explain the benefits of the shared responsibility model
    Describe multi-factor authentication (MFA)
    Differentiate between the AWS Identity and Access Management (IAM) security levels
    Describe security policies at a basic level
    Explain the benefits of AWS Organizations
    Summarize the benefits of compliance with AWS
    Explain primary AWS security services at a basic level
```

[Module 7: Monitoring and Analytics](#module-7-introduction)
```
    Summarize approaches to monitoring your AWS environment
    Describe the benefits of Amazon CloudWatch
    Describe the benefits of AWS CloudTrail
    Describe the benefits of AWS Trusted Advisor
```

[Module 8: Pricing and Support](#module-8-introduction)
```
    Understand AWS pricing and support models
    Describe the AWS Free Tier
    Describe key benefits of AWS Organizations and consolidated billing
    Explain the benefits of AWS Budgets
    Explain the benefits of AWS Cost Explorer
    Explain the primary benefits of the AWS Pricing Calculator
    Distinguish between the various AWS Support Plans
    Describe the benefits of AWS Marketplace
```

[Module 9: Migration and Innovation](#module-9-introduction)
```
    Understand migration and innovation in the AWS Cloud
    Summarize the AWS Cloud Adoption Framework (AWS CAF)
    Summarize six key factors of a cloud migration strategy
    Describe the benefits of various AWS data migration solutions, such as AWS Snowcone, AWS Snowball, and AWS Snowmobile
    Summarize the broad scope of innovative solutions that AWS offers
```

[Module 10: The Cloud Journey](#module-10-introduction)
```
    Summarize the five pillars of the AWS Well-Architected Framework
    Explain the six benefits of cloud computing
```

[Module 11: AWS Certified Cloud Practitioner Basics](#module-11--introduction)
```
    Determine resources for preparing for the AWS Certified Cloud Practitioner examination
    Describe benefits of becoming AWS Certified
```

---

## **Cloud Computing Model**

### **module 1 introduction**

### **Client-server model**

Client: (example) an Amazon Elastic Compute Cloud, or EC2, an EC2 instance, a virtual server. 

Key value: **Pay for what you need**

### **Cloud definition**

Cloud computing is the on-demand delivery of IT resources over the internet with pay-as-you-go pricing. 

the undifferentiated heavy lifting of IT

pay-as-you-go pricing

### **Cloud Computing Deployment Model**

When selecting a cloud strategy, a company must consider factors such as required cloud application components, preferred resource management tools, and any legacy IT infrastructure requirements.

* cloud-based

    Run all parts of the application in the cloud.
    Migrate existing applications to the cloud.
    Design and build new applications in the cloud.

In a cloud-based deployment model, you can migrate existing applications to the cloud, or you can design and build new applications in the cloud. You can build those applications on low-level infrastructure that requires your IT staff to manage them. Alternatively, you can build them using higher-level services that reduce the management, architecting, and scaling requirements of the core infrastructure.

For example, a company might create an application consisting of virtual servers, databases, and networking components that are fully based in the cloud.

* on-premises

    Deploy resources by using virtualization and resource management tools.
    Increase resource utilization by using application management and virtualization technologies.

On-premises deployment is also known as a private cloud deployment. In this model, resources are deployed on premises by **using virtualization** and **resource management tools**.

For example, you might have applications that run on technology that is fully kept in your on-premises data center. Though this model is much like legacy IT infrastructure, its incorporation of application management and virtualization technologies helps to increase resource utilization.

* and hybrid

    Connect cloud-based resources to on-premises infrastructure.
    Integrate cloud-based resources with legacy IT applications.

In a hybrid deployment, cloud-based resources are connected to on-premises infrastructure. You might want to use this approach in a number of situations. For example, you have legacy applications that are better maintained on premises, or government regulations require your business to keep certain records on premises.

For example, suppose that a company wants to use cloud services that can automate batch data processing and analytics. However, the company has several legacy applications that are more suitable on premises and will not be migrated to the cloud. With a hybrid deployment, the company would be able to keep the legacy applications on premises while benefiting from the data and analytics services that run in the cloud.

### **Benefits of cloud computing**

* **Trade upfront expense for variable expense**

Upfront expense refers to data centers, physical servers, and other resources that you would need to invest in before using them. 
Variable expense means you **only pay for computing resources you consume** instead of investing heavily in data centers and servers before you know how you’re going to use them.

By taking a cloud computing approach that offers the benefit of variable expense, companies can implement innovative solutions while **saving on costs**.

* **Stop spending money to run and maintain data centers**

Computing in data centers often requires you to spend more money and time managing infrastructure and servers. 

A benefit of cloud computing is the ability to focus less on these tasks and more on your applications and customers.

* **Stop guessing capacity**

With cloud computing, you don’t have to predict how much infrastructure capacity you will need before deploying an application. 

For example, you can launch Amazon EC2 instances when needed, and pay only for the compute time you use. Instead of paying for unused resources or having to deal with limited capacity, you can access only the capacity that you need. You can also scale in or scale out in response to demand.

* **Benefit from massive economies of scale**

By using cloud computing, you can achieve a lower variable cost than you can get on your own.

Because usage from hundreds of thousands of customers can aggregate in the cloud, providers, such as AWS, can achieve higher economies of scale. The economy of scale translates into lower pay-as-you-go prices. 

* **Increase speed and agility**

The flexibility of cloud computing makes it easier for you to **develop and deploy applications**.

This flexibility provides you with more time to experiment and innovate. When computing in data centers, it may take weeks to obtain new resources that you need. By comparison, cloud computing enables you to access new resources within minutes.

* **Go global in minutes**

The global footprint of the AWS Cloud enables you to deploy applications to customers around the world quickly, while providing them with low latency. This means that even if you are located in a different part of the world than your customers, customers are able to access your applications with minimal delays. 

Later in this course, you will explore the AWS global infrastructure in greater detail. You will examine some of the services that you can use to deliver content to customers around the world.

### **more**

[AWS glossary - What is Cloud Computing](https://aws.amazon.com/fr/what-is-cloud-computing/)

### **Quiz**

How does the scale of cloud computing help you to save costs?

> The aggregated cloud usage from a large number of customers results in lower pay-as-you-go prices.

### **module 2 introduction**

## **AWS compute services / Amazon Elastic Compute Cloud (Amazon EC2)**

Using [EC2](https://aws.amazon.com/fr/ec2/) for compute is highly flexible, cost effective, and quick 
You only pay for what you use

 EC2 gives you a great deal of flexibility and control, and provides secure, resizable compute capacity in the cloud as Amazon EC2 instances. 

 you also have the flexibility and control over the configuration of those instances.

 EC2 instances are also resizable.

on an EC2 instance, you can run internal buisness apps, web apps, databases and third-aprty software.

### **Hypervisor**

 a hypervisor running on the host machine is responsible for **sharing the underlying physical resources** between **the virtual machines**. This idea of sharing underlying hardware is called **multitenancy**. The hypervisor is responsible for coordinating this multitenancy and it is managed by AWS. The hypervisor is responsible for **isolating** the virtual machines from each other as they share resources from the host. This means EC2 instances are secure. 

### **Benefit EC2**

with an Amazon EC2 instance you can use a virtual server to run applications in the AWS Cloud.

* You can provision and launch an Amazon EC2 instance within minutes.
* You can stop using it when you have finished running a workload.
* You pay only for the compute time you use when an instance is running, not when it is stopped or terminated.
* You can save costs by paying only for server capacity that you need or want.

### **How Amazon EC2 works**

* Launch

* Connect

* Use

## **Amazon EC2 instance types**

instance types offer varying combinations of CPU, memory, storage, and networking capacity, and give you the flexibility to choose the appropriate mix of resources for your applications. 

### **Amazon EC2 instance families**

* **General purpose** => a variety of diverse workloads like web service or code repositories, application servers, gaming servers, backend servers for enterprise applications, small and medium databases

provide a good balance of compute, memory, and networking resources

* **Compute optimized** => gaming servers, high performance computing or HPC, and even scientific modeling. 

ideal for compute-intensive

* **Memory optimized**

good for memory-intensive

* **Accelerated computing**

good for floating point number calculations, graphics processing, or data pattern matching as they use hardware accelerators.

* **Storage optimized**

Workloads that require high performance for locally stored data

## **Amazon EC2 price**

* **On-demand** =>  ideal for short-term, irregular workloads

* Amazon EC2 **Savings Plans** enable you to reduce your compute costs by committing to a consistent amount of compute usage for a **1-year or 3-year term**. This term commitment results in savings of up to **72%** over On-Demand costs.

* **Reserved Instances**

require a contract length of either 1 year or 3 years. The workload in this scenario will only be running for 6 months.

up to a 75% discount versus On-Demand pricing. 

* **Spot instances**

withstand interruptions

up to 90% off of the On-Demand price

do not require contracts or commitment to a consistent amount of compute usage (unlike Savings Plans)

* Dedicated Hosts are physical servers with Amazon EC2 instance capacity that is fully dedicated to your use. 

## **Scaling Amazon EC2**

### **Scalability**

Scalability involves beginning with only the resources you need and designing your architecture to automatically respond to changing demand by scaling out or in. As a result, you pay for only the resources you use. You don’t have to worry about a lack of computing capacity to meet your customers’ needs.

If you wanted the scaling process to happen automatically, which AWS service would you use? The AWS service that provides this functionality for Amazon EC2 instances is **Amazon EC2 Auto Scaling**.

Amazon EC2 Auto Scaling enables you to automatically **add or remove Amazon EC2 instances** in response to changing application demand. By automatically scaling your instances in and out as needed, you are able to maintain a greater sense of application availability.

Within Amazon EC2 Auto Scaling, you can use two approaches: dynamic scaling and predictive scaling.

* Dynamic scaling responds to changing demand. 
* Predictive scaling automatically schedules the right number of Amazon EC2 instances based on predicted demand.


minimum, desired and maximum Amazon EC2 instances (and Scale as needed)

:warning: If you **do not specify** the desired number of Amazon EC2 instances in an Auto Scaling group, the desired capacity defaults to **your minimum capacity**.

Because Amazon EC2 Auto Scaling uses Amazon EC2 instances, you pay for only the instances you use, when you use them. You now have a cost-effective architecture that provides the best customer experience while reducing expenses.

## **Directing traffic with Elastic Load Balancing (ELB)**

Elastic Load Balancing is the AWS service that automatically distributes incoming application traffic across multiple resources, such as Amazon EC2 instances. 

properly distribute traffic in a high performance, cost-efficient, highly available, automatically scalable system that you can just set and forget. 

ELB is **regional**, a single URL

ELB => automatically scalable

A load balancer acts as a single point of contact for all incoming web traffic to your Auto Scaling group.

## **Messaging and queuing**

tightly coupled architecture => :x: cascading failures

Loosely coupled architecture => :check: single failure won't cause cascading failures
And storage message in a queue

*Example*: Amazon SQS and Amazon SNS

Amazon Simple Queue Service (Amazon SQS): Send, store, receive messages between software components and any volume. (without losing messages)
Amazon Simple Notification Service (Amazon SNS): an SNS topic is just a channel for messages to be delivered

Payload is data contained within a message 

**Amazon Simple Notification Service** (Amazon **SNS**) is a publish/subscribe service. Using Amazon SNS topics, a publisher publishes messages to subscribers. It  is used for sending messages like emails, text messages, push notifications, or even HTTP requests. Once a message is published, it is sent to all of these subscribers. 

## **Additional compute services**

### **Serverless**

If you have applications that you want to run in Amazon EC2, you must do the following:

1. Provision instances (virtual servers).
2. Upload your code.
3. Continue to manage the instances while your application is running.

**Serverless**: You cannot see or access the underlying infrastructure or instances that are hosting your application.

The term “serverless” means that your code runs on servers, but you do **not need** to provision or manage these servers. With serverless computing, you can focus more on innovating new products and features instead of maintaining servers.

#### **benefit**

Another benefit of serverless computing is the flexibility to scale serverless applications automatically. Serverless computing can adjust the applications' capacity by modifying the units of consumptions, such as throughput and memory. 

### **AWS Lambda**

AWS Lambda is one **serverless** Computer option.

AWS Lambda is a service that lets you run code without provisioning or managing servers. (autonomous servers)

#### **How it works**

allows to upload your code into a Lambda function. Configure trigger and from there, the service waits for the trigger

1. You upload your code to Lambda. 
2. You set your code to trigger from an event source, such as AWS services, mobile applications, or HTTP endpoints.
3. Lambda runs your code only when triggered.
4. You pay only for the compute time that you use. In the previous example of resizing images, you would pay only for the compute time that you use when uploading new images. Uploading the images triggers Lambda to run code for the image resizing function.

### **Containers**

container = package for your application's code and dependencies into a single object.

=> security, reliability, and scalability.

### **Container orchestration**

 **Docker** is a software platform that enables you to build, test, and deploy applications quickly.

 **Kubernetes** is open-source software that enables you to deploy and manage containerized applications at scale. A large community of volunteers maintains Kubernetes, and AWS actively works together with the Kubernetes community. As new features and functionalities release for Kubernetes applications, you can easily apply these updates to your applications managed by Amazon EKS.

Amazon Elastic Container Service (ECS)

* supports Docker containers
* you can use API calls to launch and stop Docker-enabled applications.

Amazon Elastic Kubernetes Service (EKS)

* a fully managed service that you can use to run Kubernetes on AWS. 

### **Amazon Fargate**

Amazon Fargate is a serverless compute platform for containers (ECS and EKS).

When using AWS Fargate, you do not need to provision or manage servers. AWS Fargate manages your server infrastructure for you. You can focus more on innovating and developing your applications, and you pay only for the resources that are required to run your containers.

### **Module 2 summary**

We define cloud computing as the on-demand delivery of IT resources over the internet with pay-as-you-go pricing. This means that you can make requests for IT resources like compute, networking, storage, analytics, or other types of resources, and then they're made available for you on demand. You don't pay for the resource upfront. Instead, you just provision and pay at the end of the month. 

#### **Vertically and Horizontally**

You can scale your EC2 instances either vertically by resizing the instance, or horizontally by launching new instances and adding them to the pool. You can set up automated horizontal scaling, using Amazon EC2 Auto Scaling. 

### **Module 3 introduction**

You need high availability and fault tolerance.

### **AWS global infrastructure**

#### **Regions**

Each region can be connected with high speed fibre connection controlled by AWS.

Each region contains many data centers.

Region data souverainty

There's four business factors that go into choosing a Region: (Key business factors)
* **Compliance** with data governance and legal requirements

* **Proximity** to your customers (a story of faster) => Latency is importante factor (the time it takes for data to be sent and received)

* **Feature availability**

*example of new quantum computing platform*: **Amazon Braket** 

* **Pricing**

It isn't the same price of all regions.

#### Availability Zones

Each region contains many data centers. A single or many data centers in an availability zone within a Region. (AZ)

An Availability Zone is a fully isolated portion of the AWS global infrastructure.

-> 10s of miles between each AZ.

:+1: Run across at least two AZ in a Region.

any service that is listed as a regionally scoped service.

### **Edge locations**

Caching copies of data closer to the customers all around the world uses the concept of content delivery networks, or CDNs. => **CDN Amazon CloudFront**

Edge locations are separate from Regions +> to accelerate communication and content delivery.

An edge location is a site that Amazon CloudFront uses to store cached copies of your content closer to your customers for faster delivery. => uses cache

#### **How to provision AWS resources**

 In AWS, everything is an API call (application programming interface)

You can use the AWS Management Console, the AWS Command Line Interface, the AWS Software Development Kits, or various other tools like AWS CloudFormation, to create requests to send to AWS APIs to create and manage AWS resources. 

 the **AWS Management Console** is useful for building out **test environments** or **viewing AWS bills**, **viewing monitoring** and working with other non technical resources. 

:+1: The AWS Management Console is most likely the first place you will go when you are learning about AWS. 

the **AWS Command Line Interface**, or **CLI**, allows you to make API calls using the terminal on your machine.

the **AWS Software Development Kits**, or **SDKs**, allows you to interact with AWS resources through various programming languages (C++, Java, .NET, and more.).

**AWS Elastic Beanstalk** is a service that helps you provision Amazon EC2-based environments.

With AWS Elastic Beanstalk, you provide code and configuration settings, and Elastic Beanstalk deploys the resources necessary to perform the following tasks:

* Adjust capacity
* Load balancing
* Automatic scaling
* Application health monitoring

you can instead provide **your application code and desired configurations** to the **AWS Elastic Beanstalk service**, which then takes that information and **builds out your environment** for you. 

AWS Elastic Beanstalk also makes it easy **to save environment configurations**, so they **can be deployed** again easily.

**AWS CloudFormation** helps you to create automated and repeatable deployments.

**AWS CloudFormation** is an **infrastructure as code tool** that allows you to define a wide variety of AWS resources in a declarative way using **JSON or YAML** text-based documents called **CloudFormation templates**.

### **Module 3 summary**

**AWS Outposts** which allow you to run AWS infrastructure right in your own data center. 
**AWS Outposts** is a service that enables you to run infrastructure in a hybrid cloud approach.
Extend AWS infrastructure and services to your on-premises data center.

### **Module 4 introduction**

#### **Connectivity to AWS**

A VPC, or Virtual Private Cloud, is essentially your own private network in AW

Amazon Virtual Private Cloud, or VPCs provides an isolatied section of the AWS Cloud.

The virtual private cloud, the way that you isolate your workload in AWS, the fundamentals of network security, including gateways, network ACLs, and security groups,

The public and private grouping of resources are known as subnets and they are ranges of IP addresses in your VPC.

=>  The **public and private subnet** 

Subnets are chunks of IP addresses in your VPC that allow you to group resources together.

**Internet Gateway** allows to public traffic from the internet to access your VPC

**Virtual Private Gateway** allows you to create a VPN connection between private network.

**AWS Direct Connect** allows you to establish a completely private, dedicated fiber connection from your data center to AWS. 

#### **Subnets and network access control lists**

**Public subnets** contain resources that need to be accessible by the public, such as an online store’s website.

**Private subnets** contain resources that should be accessible only through your private network, such as a database that contains customers’ personal information and order histories. 

**Network access control lists (ACLs)** is a virtual firewall that controls inbound and outbound traffic at the subnet level.

=> Stateless packet filtering (check the list)

**Security groups** is a virtual firewall that controls inbound and outbound traffic for an Amazon EC2 instance.

=> Stateful packet filtering

#### **QUIZ**

Which statement best describes an AWS account’s default network access control list?

> It is stateless and allows all inbound and outbound traffic.

### **Global networking**

#### **Domain Name System (DNS)**

DNS resolution is the process of translating a domain name to an IP address.

#### **Amazon Route 53** is a DNS web service. 

#### **QUIZ**

Which statement best describes **security groups**?

They are **stateful** and **deny** all inbound traffic by default.

### **Module 5 introduction**

#### **Instance stores and Amazon Elastic Block Store (Amazon EBS)**

You can think of **block-level storage** as a place to store files. A file being a series of bytes that are **stored in blocks** on disc. When a file is updated, it updates just the pieces that change. All block-level storage is in this case is your hard drive.

Instance store volumes (local storage)

An instance store is disk storage that is physically attached to the host computer for an EC2 instance.

If you stop / terminate your EC2 instance, all data written to the instance store volume will be deleted.

Amazon Elastic Block Store (EBS) => we can create virtual hard drive

an EBS volume is defined to 3 metrics: Size, Type and Configurations

EBS allows to do EBS snapshots.

An EBS snapshot is an incremental backup

To create an EBS volume, you define the configuration (such as volume size and type) and provision it. After you create an EBS volume, it can attach to an Amazon EC2 instance.

:+1: It is important to take regular snapshots of your EBS volumes.

#### **Amazon Simple Storage Service (Amazon S3)**

is a service that provides **object-level storage**. Amazon S3 **stores data** as objects in **buckets**.

**Store** and **retrieve** an unlimited amount of data. (unlimited storage space)

The **maximum file size** for an object in Amazon S3 is **5TB**.

In object storage, each object consists of **data**, **metadata**, and a **key**.

Metadata contains information about what the data is, how it is used, the object size, and so on.

An object's key is its unique identifier.

You might use Amazon S3 to store backup files, media files for a website, or archived documents.

When you upload a file to Amazon S3, you can **set permissions** to control visibility and access to it. You can also use the Amazon S3 **versioning** feature to track changes to your objects over time.

**Amazon S3 storage classes** =>  You can choose from a range of storage classes; Consider these two factors:

* How often you plan to retrieve your data
* How available you need your data to be

* Amazon S3 Standard
    Designed for frequently accessed data
    Stores data in a minimum of three Availability Zones
    
* Amazon S3 Standard-Infrequent Access
    Ideal for infrequently accessed data
    Similar to Amazon S3 Standard but has a lower storage price and higher retrieval price

* Amazon S3 One Zone-Infrequent Access
    Stores data in a single AZ
    Has a lower storage price than Amazon S3 Standard-IA
if:
    You want to save costs on storage.
    You can easily reproduce your data in the event of an Availability Zone failure.

* Amazon S3 Intelligent-Tiering
    Ideal for data with unknown or changing access patterns
    Requires a small monthly monitoring and automation fee per object

* Amazon S3 Glacier Instant Retrieval
    Works well for **archived data** that requires **immediate access**
    Can retrieve objects within a few milliseconds

* Amazon S3 Glacier Flexible Retrieval
    Low-cost storage designed for data **archiving**
    Able to retrieve objects within a few minutes to hours

* Amazon S3 Glacier Deep Archive
    Lowest-cost object storage class ideal for **archiving**
    Able to retrieve objects within 12 hours

* Amazon S3 Outposts
    Creates S3 buckets on Amazon S3 Outposts
    Makes it easier to **retrieve, store, and access data** on AWS Outposts


#### **Amazon Elastic File System (Amazon EFS)** is a **managed file system**.

allows you to have multiple instances accessing the data in EFS at the same time.

| Amazon EBS | Amazon EFS |
|:---|:---|
| An Amazon EBS volume stores data in a single Availability Zone. | Amazon EFS is a regional service. It stores data in and across multiple Availability Zones. |
| To attach an Amazon EC2 instance to an EBS volume, both the Amazon EC2 instance and the EBS volume must reside within the same Availability Zone. | The duplicate storage enables you to access data concurrently from all the Availability Zones in the Region where a file system is located. Additionally, on-premises servers can access Amazon EFS using AWS Direct Connect. |

#### **Amazon Relational Database Service (Amazon RDS)**

Relational databases use structured query language (SQL) to store and query data.

* Amazon Relational Database Service (Amazon RDS) is a service that enables you to run **relational databases** in the AWS Cloud.

Amazon RDS provides a number of different **security options**. Many Amazon RDS database engines offer **encryption at rest** (protecting data while it is stored) and **encryption in transit** (protecting data while it is being sent and received).

* Amazon RDS database engines

Amazon RDS is available on six database engines, which **optimize** for memory, performance, or input/output (I/O). Supported database engines include:

 * Amazon Aurora
 * PostgreSQL
 * MySQL
 * MariaDB
 * Oracle Database
 * Microsoft SQL Server

* Amazon Aurora is an enterprise-class relational database. (compatible MySQL and PostgreSQL)

:+1: It is up to five times faster than standard MySQL 

helps to reduce your database costs by reducing unnecessary input/output (I/O) operations, while ensuring that your database resources remain reliable and available. 

#### **Amazon DynamoDB**

Nonrelational databases are sometimes referred to as “NoSQL databases” because they use structures other than rows and columns to organize data.

In a key-value database, you can add or remove attributes from items in the table at any time. 

Amazon DynamoDB is a **key-value** database service. It delivers **single-digit millisecond performance at any scale.**

DynamoDB is serverless, which means that you do not have to provision, patch, or manage servers. 

You also do not have to install, maintain, or operate software.

#### **QUIZ**

What are the scenarios in which you should use Amazon Relational Database Service (Amazon RDS)?
    Using SQL to organize data
    Storing data in an Amazon Aurora database

What are the scenarios in which you should use Amazon DynamoDB
    Storing data in a key-value database
    Scaling up to 10 trillion requests per day
    Running a serverless database

#### **Amazon Redshift**

It is a **data warehousing service** that you can use for **big data analytics**. It offers the ability to **collect data** from **many sources** and helps you to understand relationships and trends across your data.

massivelly scalable

#### **AWS Database Migration Service (DMS)**

to help you migrate your existing database

he best part is that the source database remains fully operational during the migration, minimizing downtime to applications that rely on that database.

Migration with the same type for the source and the target => homogenous migration 
Migration with differentes types for the source and the target => heterogeneous migration 

heterogeneous migration => using the AWS Schema Conversion Tool to convert Schema structures and database code.

But these are not the only use cases for DMS. Others include development and test database migrations, database consolidation, and even continuous database replication. 
Development and test migration is when you want to develop this to test against production data, but without affecting production users. In this case, you use DMS to migrate a copy of your production database to your dev or test environments, either once-off or continuously. 

:+1: Database consolidation is when you have several databases and want to consolidate them into one central database.

#### **Additional database services**

* Amazon DocumentDB => a **document** database service that supports **MongoDB workloads**
* Amazon Neptune => a **graph** database service
* Amazon Quantum Ledger Database => a **ledger** database service. 
* Amazon Managed Blockchain => a service that you can use **to create** and **manage blockchain networks** with open-source frameworks. 

Blockchain is a distributed ledger system that lets multiple parties run transactions and share data without a central authority.

* Amazon ElastiCache => a service that adds caching layers on top of your databases to help **improve the read times** of common requests. (It supports two types of data stores: Redis and Memcached.)
* Amazon DynamoDB Accelerator => an in-memory **cache** for DynamoDB. (It helps improve response times from single-digit milliseconds to microseconds.)

### **Module 6 introduction**

#### **Shared responsibility model**

AWS is responsible for security **of** the cloud.

Customers are responsible for the security of everything that they create and put **in** the AWS Cloud.

#### **User permissions and access**

* **AWS Identity and Access Management (IAM)** gives you the flexibility to configure access based on your company’s specific operational and security needs. You do this by using a combination of IAM features, which are explored in detail in this lesson:
* IAM users, groups, and roles
* IAM policies
* Multi-factor authentication

:+1: Do **not** use the root user for everyday tasks. 

An IAM user is an identity that you create in AWS. It represents the person or application that interacts with AWS services and resources. It consists of a name and credentials.

an IAM user default has no permissions

:+1: We recommend that you create individual IAM users for **each** person who needs to access AWS.  

Identify federation to create link between IAM User and account in a company.

On **multi-factor authentication**, or **MFA** to ensure that you need not only the **email** and **password**, but also a **randomized token** to log in.

You control access in a granular way by using the AWS service, **AWS Identity and Access Management**, or **IAM**. 

=> You can create IAM user in IAM


IAM Policies (a JSON document) enable you to customize users’ levels of access to resources.
```JSON
{
    "Version": "2012-12-21",
    "Statement": {
        "Effect": "Allow", // or Deny
        "Action": "s3:ListBucket",  // a specific action
        "Resource": "arn:aws:s3:::AWSDOC-EXAMPLE-BUCKET" // a specific bucket ID
    }
}
```

We can use **IAM groups**.

**IAM roles** associated permissions (Allow or Deny), assumed for temporary amounts of time, and hsa no username and password. It is in identificate. 


**the least privilege principle.**

#### **AWS Organizations**

=> A central location to manage multiple AWS accounts

**Service control policies**, or **SCPs**, enable you to **place restrictions** on the **AWS services**, **resources**, and **individual API actions** that users and roles in each account can access and to specify the **maximum permissions** for member accounts in the organization.

**Organizational units (OU)**

When you apply a policy to an OU, **all** the accounts in the OU **automatically inherit** the permissions specified in the policy.  

You can put these accounts into one OU and then attach a policy to the OU to blocks access to all AWS services that do not meet the regulatory requirements.

Les comptes sont couramment utilisés pour isoler les charges de travail, les environnements, les équipes ou les applications.

#### **QUIZ**

Which identities and resources can SCPs be applied to?

An individual member account and an organizational unit (OU).

:+1: An SCP **affects** all **IAM users**, **groups**, and **roles** within **an account**, including **the AWS account root user**.

#### Compliance

You may need to uphold specific standards. An audit or inspection will ensure that the company has met those standards.

**AWS Artifact** is a service that provides **on-demand access** to **AWS security** and **compliance reports** and select online agreements.

2 main sections:
* AWS Artifact Agreements
 - you can review, accept, and manage agreements for an individual account and for all your accounts in AWS Organizations. 

If several services must to access to the same AWS services or resources without overlap.

* AWS Artifact Reports => provide compliance reports from third-party auditors. 

In the **Customer Compliance Center** , you can read customer compliance stories to discover how companies in regulated industries have solved various compliance, governance, and audit challenges.

#### **Denial-of-service attacks - DOS attacks**

 DOS -> a single source of attack

 DDOS -> a multiple sources

 UDP floods => Solution, security groups.

 Slowloris attacks => Solution Elastic Load Balancer (ELB)

AWS Shield with AWS WAF which uses a web application firewall to filter incoming traffic for the signatures of bad actors. It has extensive machine learning capabilities, and can recognize new threats as they evolve and proactively help defend your system against an ever-growing list of destructive vectors. 

AWS WAF works in a similar way to **block** or **allow** traffic.

#### Additional security services

**AWS Key Management Service (AWS KMS)** enables you to perform encryption operations through the use of cryptographic keys. A **cryptographic key** is a random string of digits used for locking (encrypting) and unlocking (decrypting) data. You can use AWS KMS to **create**, **manage**, and **use** cryptographic keys. You can also control the use of keys across a wide range of services and in your applications.

you can specify which IAM users and roles are able to manage keys. Ans you can temporarily disable keys.

:+1: **Your keys never leave AWS KMS, and you are always in control of them.**

**AWS WAF** is a web application firewall that lets you monitor network requests that come into your web applications. 

It does this by using a web access control list (ACL) to protect your AWS resources. 

**Amazon Inspector** is an automatic solution to improve the security and compliance of appliations by running assassments

Assassment is a list of security findings prioritizes be severity level. However, AWS does not guarantee that following the provided recommendations resolves every potential security issue. 

**Amazon GuardDuty** is a service that provides intelligent threat detection for your AWS infrastructure and resources. It looks at your network traffic.


### **Module 7 introduction**

monitoring: observing systems, collecting metrics, evaluating those metrics over time, and then using them to make decisions or take actions,

#### **Amazon CloudWatch**

is a web service that enables you to monitor (in real time) and manage various metrics and configure alarm actions based on data from those metrics. (Amazon CloudWatch Alarm which integrate in SNS)

we can use CloudWatch's **dashboard** feature.

**Benefits CloudWatch**
* you can have access to all your metrics from a central location
* you can also get visibility across your applications, infrastructure, and services
* you can reduce mean time to resolution, or MTTR, and improve total cost of ownership, or TCO.
* you can drive insights to optimize applications and operational resource

#### **AWS CloudTrail**

records API calls on your account and you can view a complete history of user activity and API calls for your applications and resources. 

The recorded information includes the identity of the API caller, the time of the API call, the source IP address of the API caller.

**CloudTrail Insights** is **an optional feature** allows CloudTrail to automatically detect **unusual API activities** in your AWS account. 

#### **AWS Trusted Advisor**

is a web service that inspects your AWS environment and provides real-time recommendations in accordance with AWS best practices.

AWS best practices in five categories: 
* cost optimization
* performance
* security
* fault tolerance
* service limits.

You can receive recommandation in real time.

AWS Trusted Advisor **dashboard** on these 5 metrics.

### **Module 8 introduction**

#### [**AWS pricing concepts**](https://calculator.aws/#/)

#### [**AWS Billing & Cost Management dashboard**](https://docs.aws.amazon.com/awsaccountbilling/latest/aboutv2/billing-what-is.html)

#### **AWS Cost Explorer**

is a console-based service.

We can create tag and use it to filter data. 

We can create custom reports.

AWS Cost Explorer includes a default report of the costs and usage for your top five cost-accruing AWS services. You can apply custom filters and groups to analyze your data.

#### **AWS Support plans**

different Support plans to help you troubleshoot issues, lower costs, and efficiently use AWS services:
* Basic (free for all)
* Developer (Client-side diagnostic tools)
* Business (All AWS Trusted Advisor checks)
* Enterprise On-Ramp (include access to a Technical Account Manager (TAM))
* Enterprise (include access to a Technical Account Manager (TAM))

**Technical Account Manager (TAM)** provide expert engineering guidance.

#### **AWS Marketplace**

is a digital catalog that includes thousands of software listings from independent software vendors.

#### **Module 8 summary**

**AWS Pricing Calculator** enables you to create an estimate for the cost of your use cases on AWS.

In **AWS Budgets**, you can set custom alerts that will notify you when your service usage exceeds (or is forecasted to exceed) the amount that you have budgeted.

### **Module 9 introduction**

#### **AWS Cloud Adoption Framework (AWS CAF)**

* can help you manage this process through guidance. 
* to provide advice to your company to enable a quick and smooth migration to AWS. 

The different AWS CAF Perspectives (areas of focus) are:
- on the business capabilities
    * Business
    * People
    * Governance
- on the technical capabilities
    * Platfrom
    * Security
    * Operations

AWS Cloud Adoption Framework Action Plan => created from inputs which is uncover gaps recorded.

#### **Migration strategies**

six of the most common migration strategies that you can implement are:

* Rehosting (Réhébergement)

 * **moving** applications **without** changes
 *  migration and scale quickly

* Replatforming (Remaniement)

 * Rehosting, but **instead of** a pure one-to-one, you might make a **few cloud optimizations** without code modification. 

* Refactoring/re-architecting (Refactorisation/réorganisation)

 * reimagining how an application is architected and developed by using cloud-native features.

* Repurchasing (Rachat)

 * moving from a traditional license to a software-as-a-service (SaaS) model. 

* Retaining (Retenue)

 * keeping applications that are critical for the business in the source environment. 

* Retiring (Retrait)

 * is the process of removing applications that are no longer needed.

#### **AWS Snow Family**

* **AWS Snowcone**

It's a device that holds up to **eight terabytes** of data and contains **edge computing**.

 :+1: Edge computing options are Amazon EC2 instances and AWS IoT Greengrass.

AWS Snowcone is a small, rugged, and secure edge computing and data transfer device. 

It features 2 CPUs, 4 GB of memory, and 8 TB of usable storage.

* **AWS Snowball**

devices are well suited for large-scale data migrations and recurring transfer workflows, in addition to local computing with higher capacity needs.

 - Storage Optimized

  for large-scale data migrations and recurring transfer workflows, 

 - Compute Optimized

 provides powerful computing resources for use cases such as machine learning, full motion video analysis, analytics, and local computing stacks. 

* **AWS Snowmobile**

is an exabyte-scale data transfer service used to move large amounts of data to AWS. 

You can transfer up to 100 petabytes of data per Snowmobile, a 45-foot long ruggedized shipping container, pulled by a semi trailer truck.


#### **Innovation with AWS**

**Amazon SageMaker**: Quickly build, train, and deploy machine learning models at scale. 

**Amazon SageMaker** and **Amazon Augmented AI**, or **Amazon A2I**, provide a machine learning platform

Convert speech to text with **Amazon Transcribe**.

Discover patterns in text with **Amazon Comprehend**.

Identify potentially fraudulent online activities with **Amazon Fraud Detector**.

Build voice and text chatbots with **Amazon Lex**, the heart of Alexa (ready-to-go AI solutions).

**Amazon Textract**: Extracting text and data from documents to make them more usable for your enterprise instead of them just being locked away in a repository. 

**AWS DeepRace**: to experiment with reinforcement learning for yours developers.

Internet of Things: Enabling connected devices to communicate all around the world. 

**AWS Ground Station**: only pay for the satellite time you actually need.


### **Module 10 introduction**

#### **The AWS Well-Architected Framework**

helps you understand how to design and operate reliable, secure, efficient, and cost-effective systems in the AWS Cloud. 

It is based on six pillars: 

* Operational excellence
* Security
* Reliability
* Performance efficiency
* Cost optimization
* Sustainability

#### **Benefits of the AWS Cloud**

six advantages of cloud computing:
* Trade upfront expense for variable expense.
* Benefit from massive economies of scale.
* Stop guessing capacity.
* Increase speed and agility.
* Stop spending money running and maintaining data centers.
* Go global in minutes.

### **Module 11 : introduction**

=> **EXAMEN**

| In English | In French |
|:---:|:---:|
| [![AWS_Cloud_Practioner_Essentials_EN](./img/AWS_Cloud_Practioner_Essentials_EN.png)](https://explore.skillbuilder.aws/learn/course/134/aws-cloud-practitioner-essentials) | [![AWS_Cloud_Practioner_Essentials_FR](./img/AWS_Cloud_Practioner_Essentials_FR.png)](https://explore.skillbuilder.aws/learn/course/1650/notions-essentielles-de-laws-cloud-practitioner-francais-aws-cloud-practitioner-essentials-french) |

***

:arrow_backward: [README.md](./README.md)

***
