# **Environment Creation**

## **Environment Installation**

First of all, we need to create a special job. We do an Interactive job `-I` of the deploy type `-t deploy` on one machine `-l host=1`. For this, I'm planning to have 2 hours and a half `walltime=2:30`.
```UNIX
oarsub -I -p "dahu-8" -l host=1,walltime=2:30
```

To see all CentOS with x86_64 architecture among reference environments of Grid'5000:
```UNIX
kaenv3 -l | grep centos | grep x86_64
```

You can check information about this environment:
```UNIX
kaenv3 centos7-min --env-version 2023042413 --env-arch x86_64 -u deploy
```

To use this:
```UNIX
kadeploy3 centos7-min --env-version 2023042413
```

Once deployed, you can use the root account to add missing libraries that you need, or remove packages that you don't need. In this example, I used dahu-10 cluster in Grenoble site.
```UNIX
ssh root@dahu-10.grenoble.grid5000.fr
```

Now, I am executing to create an environment with `OpenMPI 5.1.0a1`:

### **Update OpenMPI and mpirun**

```UNIX
yum update
yum upgrade
yum install make gcc-gfortran gcc
ymu install git
git clone https://github.com/open-mpi/ompi.git
cd ompi/
perl -MData::Dumper -e 1
yum install perl
yum install perl-CPAN
cpan Data::Dumper
./autogen.pl
yum install autocon
yum install libtool
git submodule update --init --recursive
./autogen.pl 
./configure
yum install flex
./configure
make
make install
mpirun --version
```

If you don't find `mpirun` and `mpiexec` when:
```UNIX
mpirun --version
mpiexec --version
```

Then:
```UNIX
which mpiexec
which mpirun
find / -name mpiexec
ln -s /usr/lib64/openmpi/bin/mpiexec /usr/local/bin/mpiexec
find / -name mpirun
ln -s /usr/lib64/openmpi/bin/mpirun /usr/local/bin/mpirun
mpirun --version
```

### **Update GCC (gcc, mpicc and mpif90)**

```UNIX
yum update
yum -y install wget
wget http://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-7.3.0/gcc-7.3.0.tar.gz
tar zxf gcc-7.3.0.tar.gz
cd gcc-7.3.0/
./contrib/download_prerequisites
./configure --disable-multilib --enable-languages=c,c++,fortran
yum search gcc-c++
yum -y install gcc-c++
export LD_LIBRARY_PATH=/usr/local/lib64:/usr/local/lib/gcc/x86_64-pc-linux-gnu/7.3.0/plugin:/usr/local/libexec/gcc/x86_64-pc-linux-gnu/7.3.0:$LD_LIBRARY_PATH
echo $LD_LIBRARY_PATH
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/gcc-7.3.0:PATH
echo $PATH
make -j 8
make install
gcc --version
mpicc --version
mpif90 --version
rm -rf *.tar.bz2 *.tar.gz
cd ..
rm -rf gcc-7.3.0.tar.gz 
adduser user1
passwd user1
ls -al /tmp/
rm -rf /tmp/*
yum clean all
```

Once you add and remove what you want, we do archive the environment image. 
```UNIX
tgz-g5k -m dahu-10.grenoble.grid5000.fr -f ~/centos-env.tar.zst
```

To create the environment description file, I recommend you to use this following command to start with a basic file, and to modify it.
```UNIX
kaenv3 centos7-min --env-version 2023042413 --env-arch x86_64 -u deploy > myCentos.yaml
```

example of file:
```UNIX
---
name: myCentos7-env
version: 2
arch: x86_64
alias: centos7-x64-min
description: mycentos7_x86_64_env
author: <name-user>@lists.grid5000.fr
visibility: private
destructive: false
os: linux
image:
  file: /home/<name-user>/centos-env.tar.zst
  kind: tar
  compression: zstd
postinstalls:
- archive: server:///grid5000/postinstalls/g5k-postinstall.tgz
  compression: gzip
  script: g5k-postinstall --net redhat --fstab nfs --restrict-user current --disk-aliases
boot:
  kernel: "/vmlinuz"
  initrd: "/initramfs.img"
  kernel_params: biosdevname=0 crashkernel=no
filesystem: ext4
partition_type: 131
multipart: false
```
Of course, your file will be different, for example, author value will be different because `<name-user>` must be replace by yours.

Once this is done, your customized environment is ready to be deployed:
```UNIX
kadeploy3 -a myCentos.yaml
```

If you agree about this result, It is the time to add it to the Kadeploy3 environment registry: 
```UNIX
kaenv3 -a myCentos.yaml 
```

Now, you can deploy your environment:
```UNIX
kadeploy3 myCentos7-env
```

Normally, you can see it with this command:
```UNIX
kaenv3 -l -u <name-user>
```

Every time I want to use this environment, I deploy it and log in with the `user1` account.

If you have a conflict problem with libmpi_usempi.so3/4, you can do this:

```UNIX
cd ompi/
yum list compat-libgfortran-41
yum install compat-libgfortran-41.x86_64
yum reinstall libgfortran
./autogen.pl
./configure 
```

If you have a fortran problem, check the libgfortran.so.4 PATH, and add it on $LD_LIBRARY_PATH:

```UNIX
find / -name libgfortran.so.4
echo $LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/lib64:/usr/local/lib/gcc/x86_64-pc-linux-gnu/7.3.0/plugin:/usr/local/libexec/gcc/x86_64-pc-linux-gnu/7.3.0:$LD_LIBRARY_PATH
make disclean
./configure 
```

And we continue:

```
make
make install
ldconfig /usr/local/lib64
```

## **Network tools Installation**

To install htop and traceroute on this environment:
```UNIX
yum list htop
yum install -y htop.x86_64
yum list traceroute
yum install -y traceroute.x86_64
```

To install libtool (for OSU MB):
```UNIX
yum remove libtool
wget https://ftp.gnu.org/gnu/libtool/libtool-2.4.6.tar.gz
tar -xzf libtool-2.4.6.tar.gz
rm -f libtool-2.4.6.tar.gz
cd libtool-2.4.6
./configure
make
make install
```

To install wireshark on this environment:
```UNIX
yum install -y wireshark
```

Command example:

```UNIX
tshark -i enp24s0f0 host dahu-32
```

***

:arrow_backward: [README.md](./README.md)

***

