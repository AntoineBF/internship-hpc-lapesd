# Point-to-point tests (on dahu clusters)

(CPUtype: `Intel Xeon Gold 6130`, OS: `debian11`, OpenMPI: `4.1.0`, GCC: `10.2.1`)

## Standard

### Grid'5000 (Debian)

* osu_bibw:

```UNIX
# OSU MPI Bi-Directional Bandwidth Test v7.1
# Size      Bandwidth (MB/s)
# Datatype: MPI_CHAR.
1                       3.92
2                      14.43
4                      29.08
8                      57.39
16                    112.58
32                    159.40
64                    312.71
128                   607.80
256                  1163.52
512                  2298.98
1024                 3847.09
2048                 6113.96
4096                10481.27
8192                10863.68
16384               11777.39
32768               16550.66
65536               21705.61
131072              26231.00
262144              28478.95
524288              29287.60
1048576             26092.25
2097152             20090.91
4194304             19810.87
```

* osu_bw:

```UNIX
# OSU MPI Bandwidth Test v7.1
# Size      Bandwidth (MB/s)
# Datatype: MPI_CHAR.
1                       3.74
2                       7.34
4                      14.61
8                      43.33
16                     94.23
32                    128.44
64                    258.84
128                   491.98
256                   950.29
512                  1808.24
1024                 2975.67
2048                 5176.88
4096                10209.58
8192                 7693.88
16384                7029.89
32768                9900.77
65536               12100.35
131072              14132.38
262144              15256.86
524288              15140.73
1048576             13324.06
2097152             10165.54
4194304             10270.00
```

* osu_latency:

```UNIX
# OSU MPI Latency Test v7.1
# Size          Latency (us)
# Datatype: MPI_CHAR.
1                       0.37
2                       0.31
4                       0.32
8                       0.31
16                      0.32
32                      0.43
64                      0.42
128                     0.44
256                     0.49
512                     0.50
1024                    0.62
2048                    0.76
4096                    0.96
8192                    1.93
16384                   2.91
32768                   3.84
65536                   5.80
131072                 10.04
262144                 16.29
524288                 33.72
1048576                80.06
2097152               205.49
4194304               414.10
```

* osu_latency_mp:

```UNIX
# Number of forked processes in sender: 2
# Number of forked processes in receiver: 2
# OSU MPI Multi-process Latency Test v7.1
# Size          Latency (us)
# Datatype: MPI_CHAR.
1               0.43
2               0.39
4               0.40
8               0.40
16              0.39
32              0.48
64              0.47
128             0.49
256             0.54
512             0.56
1024            0.68
2048            0.81
4096            1.04
8192            2.01
16384           2.99
32768           3.97
65536           6.09
131072         10.50
262144         18.71
524288         35.68
1048576        79.73
2097152       204.26
4194304       406.83
```

* osu_latency_mt:

```UNIX
# Number of Sender threads: 1 
# Number of Receiver threads: 2
# OSU MPI Multi-threaded Latency Test v7.1
# Size          Latency (us)
# Datatype: MPI_CHAR.
1                       1.89
2                       1.90
4                       1.89
8                       1.89
16                      1.91
32                      1.98
64                      1.95
128                     1.95
256                     1.94
512                     1.97
1024                    2.00
2048                    2.03
4096                    2.15
8192                    3.11
16384                   4.47
32768                   5.66
65536                   8.18
131072                 11.92
262144                 21.64
524288                 42.94
1048576                97.78
2097152               213.50
4194304               511.18
```

* osu_mbw_mr:

```UNIX
# OSU MPI Multiple Bandwidth / Message Rate Test v7.1

# [ pairs: 1 ] [ window size: 64 ]
# Size                  MB/s        Messages/s
# Datatype: MPI_CHAR.
1                       3.77        3771336.78
2                       7.65        3826831.11
4                      15.21        3801646.71
8                      30.62        3827462.76
16                     81.55        5097124.11
32                    129.53        4047704.73
64                    255.37        3990189.12
128                   493.40        3854692.55
256                   978.46        3822093.49
512                  1897.04        3705148.88
1024                 3037.33        2966139.20
2048                 5424.93        2648890.40
4096                 9250.11        2258326.43
8192                 8211.82        1002419.12
16384                7416.49         452666.89
32768               10851.04         331147.55
65536               13301.32         202962.00
131072              15078.57         115040.38
262144              16136.23          61554.83
524288              15902.15          30330.95
1048576             13095.26          12488.61
2097152             10113.43           4822.46
4194304             10147.39           2419.33
```

* osu_multi_lat:

```UNIX
# OSU MPI Multi Latency Test v7.1
# Size          Latency (us)
# Datatype: MPI_CHAR.
1                       0.41
2                       0.39
4                       0.37
8                       0.41
16                      0.40
32                      0.48
64                      0.48
128                     0.49
256                     0.52
512                     0.57
1024                    0.69
2048                    0.81
4096                    1.03
8192                    2.05
16384                   2.98
32768                   3.88
65536                   6.11
131072                 10.71
262144                 19.24
524288                 37.67
1048576                83.80
2097152               203.99
4194304               401.75
```

### Grid'5000 (CentOS)

* osu_bibw:

```UNIX
```

* osu_bw:

```UNIX
```

* osu_latency:

```UNIX
```

* osu_latency_mp:

```UNIX
```

* osu_latency_mt:

```UNIX
```

* osu_mbw_mr:

```UNIX
```

* osu_multi_lat:

```UNIX
```

### AWS

* osu_bibw:

```UNIX
```

* osu_bw:

```UNIX
```

* osu_latency:

```UNIX
```

* osu_latency_mp:

```UNIX
```

* osu_latency_mt:

```UNIX
```

* osu_mbw_mr:

```UNIX
```

* osu_multi_lat:

```UNIX
```


## Persistent

### Grid'5000 (Debian)

* osu_bibw_persistent:

```UNIX
# OSU MPI Bi-Directional Bandwidth Persistent Test v7.1
# Size      Bandwidth (MB/s)
# Datatype: MPI_CHAR.
1                       5.31
2                      16.65
4                      33.55
8                      64.62
16                    136.42
32                    178.09
64                    365.74
128                   711.12
256                  1329.27
512                  2613.04
1024                 4081.61
2048                 6541.13
4096                11479.11
8192                 9355.32
16384               11745.24
32768               17407.19
65536               22589.43
131072              26652.55
262144              29232.39
524288              30825.38
1048576             26279.59
2097152             20185.17
4194304             18608.15
```

* osu_bw_persistent:

```UNIX
# OSU MPI Bandwidth Persistent Test v7.1
# Size      Bandwidth (MB/s)
# Datatype: MPI_CHAR.
1                       4.49
2                       9.06
4                      18.10
8                      58.98
16                    112.99
32                    142.29
64                    222.22
128                   536.11
256                  1044.06
512                  1947.48
1024                 3240.82
2048                 5639.98
4096                10666.45
8192                 8040.46
16384                6886.58
32768                9700.07
65536               12138.34
131072              13877.52
262144              14718.83
524288              17802.20
1048576             12999.30
2097152             10037.09
4194304              9993.01
```

* osu_latency_persistent:

```UNIX
# OSU MPI Latency Persistent Test v7.1
# Size          Latency (us)
# Datatype: MPI_CHAR.
1                       0.42
2                       0.31
4                       0.31
8                       0.31
16                      0.31
32                      0.43
64                      0.42
128                     0.45
256                     0.50
512                     0.51
1024                    0.63
2048                    0.76
4096                    0.95
8192                    1.93
16384                   2.87
32768                   3.47
65536                   5.65
131072                  9.84
262144                 17.69
524288                 34.82
1048576                78.34
2097152               205.89
4194304               407.91
```

### Grid'5000 (CentOS)

* osu_bibw_persistent:

```UNIX
```

* osu_bw_persistent:

```UNIX
```

* osu_latency_persistent:

```UNIX
```

### AWS

* osu_bibw_persistent:

```UNIX
```

* osu_bw_persistent:

```UNIX
```

* osu_latency_persistent:

```UNIX
```

