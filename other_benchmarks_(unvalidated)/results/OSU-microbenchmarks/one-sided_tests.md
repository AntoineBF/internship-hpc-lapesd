# One-sided tests

* osu_acc_latency

```UNIX
# OSU MPI_Accumulate latency Test v7.1
# Window creation: MPI_Win_allocate
# Synchronization: MPI_Win_flush
# Datatype: MPI_CHAR.
# Size          Latency (us)
1                       0.24
2                       0.22
4                       0.10
8                       0.10
16                      0.10
32                      0.10
64                      0.11
128                     0.11
256                     0.11
512                     0.11
1024                    0.12
2048                    0.13
4096                    0.18
8192                    0.24
16384                   0.46
32768                   1.02
65536                   1.86
131072                  3.34
262144                  6.39
524288                 29.11
1048576                77.38
2097152               158.95
4194304               317.50
```

* osu_cas_latency

```UNIX
# OSU MPI_Compare_and_swap latency Test v7.1
# Window creation: MPI_Win_allocate
# Synchronization: MPI_Win_flush
# Datatype: MPI_CHAR.
# Size          Latency (us)
1                       0.10
```

* osu_fop_latency

```UNIX
# OSU MPI_Fetch_and_op latency Test v7.1
# Window creation: MPI_Win_allocate
# Synchronization: MPI_Win_flush
# Datatype: MPI_CHAR.
# Size          Latency (us)
1                       0.15
```
