# Other benchmarks (**unvalidated** and **OSU Micro-Benchmarks**)

## [OSU Micro-Benchmarks](https://mvapich.cse.ohio-state.edu/benchmarks/): 

The OSU micro-benchmarks is a set of Host-based benchmarks that have the ability to evaluate the correctness of the data exchanged in addition to evaluating the communication performance.

  - **Point-to-Point MPI Benchmarks**: Latency, multi-threaded latency, multi-pair latency, multiple bandwidth / message rate test bandwidth, bidirectional bandwidth.
  - **Collective MPI Benchmarks**: Collective latency tests for various MPI collective operations such as MPI_Allgather, MPI_Alltoall, MPI_Allreduce, MPI_Barrier, MPI_Bcast, MPI_Gather, MPI_Reduce, MPI_Reduce_Scatter, MPI_Scatter and vector collectives.
  - **Non-Blocking Collective (NBC) MPI Benchmarks**: Collective latency and Overlap tests for various MPI collective operations such as MPI_Iallgather, MPI_Iallreduce, MPI_Ialltoall, MPI_Ibarrier, MPI_Ibcast, MPI_Igather, MPI_Ireduce, MPI_Iscatter and vector collectives.
  - **One-sided MPI Benchmarks**: one-sided put latency (active/passive), one-sided put bandwidth (active/passive), one-sided put bidirectional bandwidth, one-sided get latency (active/passive), one-sided get bandwidth (active/passive), one-sided accumulate latency (active/passive), compare and swap latency (passive), and fetch and operate (passive) for MVAPICH2 (MPI-2 and MPI-3).


```UNIX
tar zxf osu-micro-benchmarks-7.1-1.tar.gz
cd osu-micro-benchmarks-7.1-1/
```

If you have this warning when you tried to do `make`:

```UNIX
WARNING: 'automake-1.16' is probably too old.          You should only need it if you modified 'Makefile.am' or          'configure.ac' or m4 files included by 'configure.ac'.          The 'automake' program is part of the GNU Automake package:          <https://www.gnu.org/software/automake>          It also requires GNU Autoconf, GNU m4 and Perl in order to run:          <https://www.gnu.org/software/autoconf>          <https://www.gnu.org/software/m4/>          <https://www.perl.org/> make: *** [Makefile:335: Makefile.in] Error 1
```

You can run the following command and retry:

```UNIX
autoreconf
```

Then:
```UNIX
make clean
./configure CC=$(which mpicc) CXX=$(which mpicxx)
make
```

You can see the test results in the folder [results/OSU-microbenchmarks/](./results/OSU-microbenchmarks/).

## :x: **(*not realistic enough*)** [IMB (Intel MPI Benchmarks)](https://www.intel.com/content/www/us/en/developer/articles/technical/intel-mpi-benchmarks.html):

  - Software Requirements: cpp, ANSI C compiler, libstdc++-devel, gmake on Linux* OS.
  - Memory and Disk Space Requirements (it is determined by the number of active processes with the **default settings** (standard mode) or the maximum size of the MPI message with **user-defined settings** (optional mode))

The Intel® MPI Benchmarks package includes many components but:

* Only the following components *may be* to interest to us:
  - Two components covering MPI-1 functionality:
    - IMB-MPI1 - benchmarks for MPI-1 functions
    - IMB-P2P – shared memory transport-oriented benchmarks for MPI-1 point-to-point communications (redundancy with OSU micro benchmarks)

* The rest looks like some OSU micro benchmarks application, plus other components that don't interest us: 
  - Two components covering MPI-2 functionality:
    - IMB-EXT - one-sided communications benchmarks
    - IMB-IO - input/output (I/O) benchmarks
  - Two components covering MPI-3 functionality:
    - IMB-NBC - non-blocking collectives benchmarks that provide measurements of the computation/communication overlap and of the pure communication time
    - IMB-RMA - Remote Memory Access (RMA) benchmarks that use passive target communication to measure one-sided communication
  - IMB-MT - eliminates most of the cross-thread synchronization points in the MPI workflow. Available for Intel® MPI Benchmarks 2019 only.

## :x: **(*too old and a little redundant with NPB*)** [HPCC (HPC Challenge)](https://hpcchallenge.org/hpcc/index.html): 

It is a benchmark suite that measures a range memory access patterns. It is essentially made up of several tests:
  - [HPL (High-Performance Linpack)](https://icl.utk.edu/hpl/index.html): It measures the floating point rate of execution for solving a linear system of equations.
  - DGEMM: It measures the floating point rate of execution of double precision real matrix-matrix multiplication.
  - [STREAM (Sustainable Memory Bandwidth in High Performance Computers)](https://www.cs.virginia.edu/stream/): It is a simple synthetic benchmark program that measures sustainable memory bandwidth (in MB/s) and the corresponding computation rate for simple vector kernels. (like OSU Micro-Benchmarks)
  - [PTRANS / PARKBENCH (PARallel Kernels and BENCHmarks)](https://www.netlib.org/parkbench/html/matrix-kernels.html): Too old (October 03, 1996)
  - [RandomAccess](https://hpcchallenge.org/projectsfiles/hpcc/RandomAccess.html): It measures the rate of integer random updates of memory (GUPS).
  - [FFTE (A Fast Fourier Transform Package)]():  A package to compute Discrete Fourier Transforms of 1-, 2- and 3- dimensional sequences of length $ (2^p)*(3^q)*(5^r) $. 

[HPCC github](https://github.com/icl-utk-edu/hpcc)


## :x: **(*requires too much memory*)** [Graph500](https://graph500.org/?page_id=12#sec-1):

[Graph500 github](https://github.com/graph500/graph500)

This benchmark (“Search” and “Shortest-Path”) has multiple analysis techniques (3 kernels) accessing a single data structure representing a weighted, undirected graph.

The first kernel constructs an undirected graph in a format usable by all subsequent kernels This is a scalable data generator which produces edge tuples containing the start vertex and end vertex for each edge. The second kernel performs a **breadth-first search** (**BFS**) of this graph. The third kernel performs multiple **single-source shortest path** (**SSSP**) computations on this graph (after the second kernel).

There are five problem classes defined by their input size:

| 5 problem classes | Input size | \| | Scale (first parameter*) | Edge Factor (*second parameter*) |
|:---:|:---:|:---:|:---:|:---:|
| toy | 17GB ($10^{10}$ bytes) | \| | 26 | 16 |
| mini | 140GB ($10^{11}$ bytes) | \| | 29 | 16 |
| small | 1TB ($10^{12}$ bytes) | \| | 32 | 16 |
| medium | 17TB ($10^{13}$ bytes) | \| | 36 | 16 |
| large | 140TB ($10^{14}$ bytes) | \| | 39 | 16 |
| huge | 1.1PB ($10^{15}$ bytes) | \| | 42 | 16 |

*Notice*: For toy class with 8 processes:

```UNIX
mpirun -np 8 ./graph500_reference_bfs 26 16
```

This benchmark follows this algorithm:

```
1. Generate the edge list.

2. Construct a graph from the edge list (timed, kernel 1).

3. Randomly sample 64 unique search keys with degree at least one, not counting self-loops.

4. For each search key:
  1. Compute the parent array (timed, kernel 2).

  2. Validate that the parent array is a correct BFS search tree for the given search tree.

5. For each search key:
  1. Compute the parent array and the distance array (timed, kernel 3).

  2. Validate that the parent array/distance vector is a correct SSSP search tree with shortest paths for the given search tree.

6. Compute and output performance information.
```

The performance information includes only the sections that are marked as *timed*. The information in results are: the **time** and the number of Traversed Edges Per Second (**TEPS**) for each BFS search. Of course, the time used by the three kernels.

You can see the results ([graph500_reference_bfs](./results/Graph500/g5_bfs_8_16_16_64.txt) and [graph500_reference_bfs_sssp](./results/Graph500/g5_sssp_8_16_16_64.txt)) of the first tests on Grid'5000.

If you have multiple definition problem for the compilation, you can run the following commands in src folder:

```UNIX
sed -i -e 's/int64_t \*column;/extern int64_t \*column;/' csr_reference.c
sed -i -e 's/float \*weights;/extern float \*weights;/' csr_reference.c
```

**Vocabulary Recap**:
* BFS: a **B**readth-**F**irst **S**earch
* SSSP: a **S**ingle-**S**ource **S**hortest **P**ath
* NBFS: **N**umber of **BFS** searches run
* TEPS: The number of **T**raversed **E**dges **P**er **S**econd

## :x: **(*irrelevant*)** [HPCG (High-Performance Conjugate Gradient)](https://hpcg-benchmark.org/): 

It's similar to NPB-CG, but with a few differences in the values of the matrix elements (NPB-CG's calculation and communication schemes are non-physical, unlike those of HPCG).

***

:arrow_backward: [README.md](../README.md)

***
