# **Interpretation of results**

Once the tests had been carried out on Grid'5000 and AWS, I was able to calculate the speedup and efficiency of each application. We can see the graphs in [NPB_result_strong_scaling.md](./results/NPB/NPB_result_strong_scaling.md) file below each paragraph dealing with the associated application.

## **Latency and Bandwidth tests (OSU Micro-Benchmarks)**

First, let's analyze the results obtained with the OSU tests.

![latency and bandwidth](./img/latency_n_bandwidth.png)

The values were derived from the average of 3 executions per experiment.

Latency is more or less the same overall. With messages between 2 and 4 MB (perhaps even more), latency is higher on AWS than on Grid'5000.

Bandwidth have more or less the same behavior with small messages transmetted (until 4 kB with 600 MB/s). Bandwidth becomes more important in Grid'5000, as twice as much bandwidth as in AWS.

## **Collective Communication tests (OSU Micro-Benchmarks)**

The 5 NPB applications I've tested use collective communications operators. The latency and bandwidth tests use point-to-point communication operators. These operators (MPI_Send() and MPI_IRecv()) are used by IS, CG and MG applications.

|  | IS | EP | CG | MG | FT |
|:---|:---|:---|:---|:---|:---|
| `MPI_Bcast()` | :heavy_check_mark: | :heavy_check_mark: |  |  | :heavy_check_mark: |
| `MPI_Alltoall()` | :heavy_check_mark: |  |  |  | :heavy_check_mark: |
| `MPI_Allreduce()` | :heavy_check_mark: | :heavy_check_mark: |  | :heavy_check_mark: |  |
| `MPI_Reduce()` |  | :heavy_check_mark: | :heavy_check_mark: |  | :heavy_check_mark: |

![OSU_Collective_Communication](./img/osu_collective_comm.png)

The values were also derived from the average of 3 executions per experiment.

But the results on AWS and Grid'5000 are too similar to use them as a basis for comparison.

## **NPB applications**

For each application, the values were also derived from the average of 10 executions per experiment.

### **IS**

IS tests both integer computation speed and communication performance. The IS application uses both point-to-point and collective communication operators. 

Looking at the efficiency curves, we can see that the results of the tests carried out on AWS have 4 different slopes as it evolves. Each change in slope corresponds to a change in the number of nodes used, i.e. the first slope is due to the use of a node, and so on.

This phenomenon isn't present in the Grid'5000 test results. This is probably due to the difference in network traffic observed when using wireshark. Experiments with wireshark show a significant difference between AWS, with its high network traffic, and Grid'5000, with its lower traffic. This is particularly visible for all applications that use communication operators. This is at first sight contradictory with the difference in MTU values. The MTU (Maximum Transmission Unit) is set to 1000 bytes on Grid'5000 and 9000 on AWS. But you have to remember that the instances in the cloud cluster are virtual machines (VMs). So, Virtualization isolation is important for this difference.

Overall, IS efficiency results on AWS follow the same trend as those on Grid'5000, with the exception of a few irregularities due to changes in the number of instances.

We can to conclude that IS has more or less the same scalability on bare-metal cluster as on cloud cluster, in our case mainly thanks to the difference in MTU. Without this difference, it's highly likely that efficiency results on bare-metal cluster would be greater than on cloud cluster.

![IS_Scaling](img/scaling/IS_scaling.png)

### **EP**

EP provides an estimation of the upper achievable limits for floating point performance (the performance without significant interprocessor communication), i.e a relatively little or negligible communication between processes. So, EP application is computationally intensive, but doesn't carry out too many communications. 

We can confirm this with its implementation by looking at its source code. We notice that the EP application doesn't use point-to-point communication operators like MPI_Send() and MPI_Irecv(), but rather Collective communication operators such as MPI_BCast() (global data broadcast) and reduction operators such as MPI_Reduce() and MPI_Allreduce() (equivalent to MPI_Reduce() followed by MPI_BCast()).

EP have more or less the same scalability, the same efficiency and the speedup very similair on Grid'5000 and AWS clusters because it doesn't have an important impact in terms of communication. According to the results obtained, efficiency remains at around 80% (~79% for AWS and ~82% for Grid'5000) from 16 MPI processes, despite a significant increase (in powers of 2) in the total number of MPI processes. That said, we can observe a gradual reduction on the Grid'5000 side, unlike AWS, which has a sudden drop. However, it's still negligible.

We can to conclude that EP have approximately the same scalability on bare-metal cluster as on cloud cluster. In both cases, the EP application displayed favorable scalability.

![EP_Scaling](img/scaling/EP_scaling.png)

### **CG**

CG is used to compute an approximation to the smallest eigenvalue of a large, sparse, symmetric positive definite matrix and it tests irregular long-distance communication. This is consistent with the use of the MPI_Send() and MPI_Irecv() point-to-point communication operators in the CG application implementation.

Despite the fact that the baseline execution time (single MPI process test on a single instance) was too high, resulting in efficiency values greater than 1, we still observed a similarity between the results obtained on Grid'5000 and on AWS. CG have a similar scalability and efficiency on Grid'5000 and AWS. Nevertheless, a growing difference in speedup is to be expected with the greater increase on AWS with intersecting efficiency curves between AWS and Grid'5000.

We can to conclude that CG have approximately the same scalability on bare-metal cluster as on cloud cluster.

![CG_Scaling](img/scaling/CG_scaling.png)

### **MG**

MG tests (short and long-distance) data communication that is consistent with the use of many MPI_send() and MPI_BCast() in its source code. According to results, MG has poor scalability with AWS because, on the one hand, it's an application that MPI processes communicate a lot, and on the other, latency is highest and bandwidth lower on AWS than Grid'5000.

The MTU (Maximum Transmission Unit) is set to 9000 on AWS. This is verified with Wireshark: 

![AWS_Wireshark](img/aws_wireshark.png)

With Wireshark, we can know messages size between nodes.

| Bar graph of results on Grid'5000 | Diagram of packets transmitted during the first experiment (without connection and disconnection packets) |
|:---|:---|
| ![MG_Wireshark](img/MG_wireshark.png) | ![MG_Wireshark_Diagram](img/MG_Wireshark_diagram.jpg) |

Moreover, this is also due to the fact that network traffic is higher on AWS than on Grid'5000.
According to tests carried out on wireshark, we noticed that, when running the application, the number of packets captured between 2 nodes/instances is much higher on AWS than on Grid'5000, i.e. ~30000 packets for AWS and ~75 for Grid'5000. Despite a difference in MTU, with a higher value for AWS.

We can conclude that MG is more efficient and has a higher speedup on a bare-metal cluster than on a cloud cluster, in our case mainly thanks to the difference of network traffic.

![MG_Scaling](img/scaling/MG_scaling.png)

### **FT**

FT performs the essence of many spectral codes and it tests long-distance communication performance. So, it is computationally intensive application that uses communication operators. According to the source code, the FT application doesn't use point-to-point communication operators but collective communication operators such as MPI_BCast() and MPI_Alltoall() (selective broadcast, by all processes, of distributed data).

Looking at the curves obtained from the experimental results, we note similarities in behavior with the curves from the IS application. The efficiency curves obtained from the experiments on Grid'5000 decrease rapidly, but remain progressive. This contrasts with the AWS curves, which evolve more strongly, each with a result outside the intuitive evolution. For IS and FT, this is the result for a total of 32 MPI processes. The similarity in the evolution of results is due to the same choice of test combinations and the use of the same collective communication operators. 

Let's not forget to point out that FT's efficiencies are better than those of IS. This is due to the fact that FT does not use point-to-point communication.

The discrepancy between the values from AWS and Grid'5000 is due to the difference between the MTUs, network traffic and bandwidths as explained in the paragraph for the IS application.

We can conclude that FT has more or less the same scalability on a bare-metal cluster as on a cloud cluster, in our case mainly thanks to the values of MTU (Maximum Transmission Unit). Nevertheless, the efficiency results on the cloud cluster are greater than on bare-metal cluster, thanks to the difference in MTU values, but they tend towards the same efficiency value. The differences between AWS and Grid'5000 are more obvious with FT than with IS because the lower use of communication operators (no point-to-point communication) compared to IS.

![FT_Scaling](img/scaling/FT_scaling.png)

## **Conclusion**

Overall, we've noticed that the evolution of results obtained on Grid'5000 is smoother than those obtained on AWS. Let's not forget that the conclusion may be somewhat biased because of minor differences between the two supports, such as the processor model, OS environment, network configuration and library version. 

However, we can conclude that EP has the best scaling among these 5 NPB applications because its speedups are linear and the efficiency curves converge at 80%, despite the increase in MPI processes, as communication time has little influence for EP.

Other applications have scaling whose speedup increases rapidly and converges, and efficiency which decreases rapidly and converges too.

This is due to the greater or lesser degree of communication, or more precisely, the use of communication operators. This influences the convergence value of efficiency, with of course the increase in node and MPI processes.

To conclude, for communication-intensive applications, bare-metal clusters may be more suitable thanks to the bandwidth. Otherwise, for other applications, there's no great difference in performance, and cloud clusters are a good option from a financial point of view.

