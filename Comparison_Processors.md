# Processor comparison between Grid'5000 and AWS instances

You can skip these tables, and see the [explanation of my choices](#explanation-of-choices)

## [Grid'5000](https://www.grid5000.fr/w/Hardware)

| Name | Product Collection | Launch Date | #Cores | #Threads | Basic Frequency | Maximum Frequency | Caches | Site |
|:---|:---|:---|:---|:---|:---|:---|:---|:---|
| [Intel Xeon E5-2620 (Sandy Bridge)](https://www.intel.fr/content/www/us/en/products/sku/64594/intel-xeon-processor-e52620-15m-cache-2-00-ghz-7-20-gts-intel-qpi/specifications.html) | Intel® Xeon® Processor E5 Family | Q1 2012 | 6 | 12 | 2.00GHz | 2.50GHz | 15MB Intel® Smart Cache | [Lyon hercule](https://www.grid5000.fr/w/Lyon:Hardware#hercule) | 
| [Intel Xeon E5-2630 (Sandy Bridge)](https://www.intel.fr/content/www/us/en/products/sku/64593/intel-xeon-processor-e52630-15m-cache-2-30-ghz-7-20-gts-intel-qpi/specifications.html) | Intel® Xeon® Processor E5 Family  | Q1 2012 | 6 | 12 | 2.30GHz | 2.80GHz | 15MB Intel® Smart Cache | [Lyon taurus](https://www.grid5000.fr/w/Lyon:Hardware#taurus) / [orion](https://www.grid5000.fr/w/Lyon:Hardware#orion) |
| [Intel Xeon E5-2630L (Sandy Bridge)](https://www.intel.fr/content/www/us/en/products/sku/64586/intel-xeon-processor-e52630l-15m-cache-2-00-ghz-7-20-gts-intel-qpi/specifications.html) | Intel® Xeon® Processor E5 Family | March 2012 | 6 | 12 | 2.00GHz | 2.50GHz | 15MB Intel® Smart Cache | [Luxembourg petitprince](https://www.grid5000.fr/w/Luxembourg:Hardware#petitprince) |
| [Intel Xeon X5670 (Westmere)](https://www.intel.fr/content/www/us/en/products/sku/47920/intel-xeon-processor-x5670-12m-cache-2-93-ghz-6-40-gts-intel-qpi/specifications.html) | Legacy Intel® Xeon® Processors  | Q1 2010 | 6 | 12 | 2.93GHz | 3.33GHz | 12MB Intel® Smart Cache | [Sophia uvb](https://www.grid5000.fr/w/Sophia:Hardware#uvb) |
| [Intel Xeon E5-2630 v3 (Haswell)](https://www.intel.fr/content/www/us/en/products/sku/83356/intel-xeon-processor-e52630-v3-20m-cache-2-40-ghz/specifications.html) | Intel® Xeon® Processor E5 v3 Family | Q3 2014 | 8 | 16 | 2.40GHz | 3.20GHz | 20MB Intel® Smart Cache | [Rennes parasilo](https://www.grid5000.fr/w/Rennes:Hardware#parasilo) / [paravance](https://www.grid5000.fr/w/Rennes:Hardware#paravance) - [Nancy grimoire](https://www.grid5000.fr/w/Nancy:Hardware#grimoire) / [grisou](https://www.grid5000.fr/w/Nancy:Hardware#grisou) |
| [Intel Xeon E5-2620 v4 (Broadwell)](https://www.intel.fr/content/www/us/en/products/sku/92986/intel-xeon-processor-e52620-v4-20m-cache-2-10-ghz/specifications.html) | Intel® Xeon® Processor E5 v4 Family | Q1 2016 | 8 | 16 | 2.10GHz | 3.00GHz | 20MB Intel® Smart Cache | [Lyon nova](https://www.grid5000.fr/w/Lyon:Hardware#nova) |
| [Intel Xeon E5-2650 (Sandy Bridge)](https://www.intel.fr/content/www/us/en/products/sku/64590/intel-xeon-processor-e52650-20m-cache-2-00-ghz-8-00-gts-intel-qpi/specifications.html) | Intel® Xeon® Processor E5 Family | Q1 2012 | 8 | 16 | 2.00GHz | 2.80GHz | 20MB Intel® Smart Cache | [Nancy graphite](https://www.grid5000.fr/w/Nancy:Hardware#graphite) |
| [Intel Xeon E5-2660 (Sandy Bridge)](https://www.intel.fr/content/www/us/en/products/sku/64584/intel-xeon-processor-e52660-20m-cache-2-20-ghz-8-00-gts-intel-qpi/specifications.html) | Intel® Xeon® Processor E5 Family  | Q1 2012 | 8 | 16 | 2.20GHz | 3.00GHz | 20MB Intel® Smart Cache | [Nantes econome](https://www.grid5000.fr/w/Nantes:Hardware#ecomone) |
| [Intel Xeon E5-2630 v4 (Broadwell)](https://www.intel.fr/content/www/us/en/products/sku/92981/intel-xeon-processor-e52630-v4-25m-cache-2-20-ghz/specifications.html) | Intel® Xeon® Processor E5 v4 Family | Q1 2016 | 10 | 20 | 2.20GHz | 3.10GHz | 25MB Intel® Smart Cache | [Lille chetemi](https://www.grid5000.fr/w/Lille:Hardware#chetemi) |
| [Intel Xeon E5-2630L v4 (Broadwell)](https://www.intel.fr/content/www/us/en/products/sku/92978/intel-xeon-processor-e52630l-v4-25m-cache-1-80-ghz/specifications.html) | Intel® Xeon® Processor E5 v4 Family | June 20, 2016 | 10 | 20 | 1.80GHz | 2.90GHz | 25MB Intel® Smart Cache | [Nantes ecotype](https://www.grid5000.fr/w/Nantes:Hardware#ecotype) |
| [Intel Xeon Gold 6126 (Skylake)](https://www.intel.fr/content/www/us/en/products/sku/120483/intel-xeon-gold-6126-processor-19-25m-cache-2-60-ghz/specifications.html) | Intel® Xeon® Scalable Processors | Q3 2017 | 12 | 24 | 2.60GHz | 3.70GHz | 19.25MB L3 Cache | [Lille chifflot](https://www.grid5000.fr/w/Lille:Hardware#chifflot) |
| [Intel Xeon E5-2680 v4 (Broadwell)](https://www.intel.fr/content/www/us/en/products/sku/91754/intel-xeon-processor-e52680-v4-35m-cache-2-40-ghz/specifications.html) | Intel® Xeon® Processor E5 v4 Family | Q1 2016 | 14 | 28 | 2.40GHz | 3.30GHz | 35MB Intel® Smart Cache | [Lille chifflet](https://www.grid5000.fr/w/Lille:Hardware#chifflet) |
| (exotic) [Intel Xeon Gold 5218 (Cascade Lake)](https://www.intel.fr/content/www/us/en/products/sku/192444/intel-xeon-gold-5218-processor-22m-cache-2-30-ghz/specifications.html) | 2nd Gen Intel® Xeon® Scalable Processors | Q2 2019 | 16 | 32 | 2.30GHz | 3.90GHz | 22MB | [Grenoble troll](https://www.grid5000.fr/w/Grenoble:Hardware#troll) |
| [Intel Xeon Gold 6130 (Skylake)](https://www.intel.fr/content/www/us/en/products/sku/120492/intel-xeon-gold-6130-processor-22m-cache-2-10-ghz/specifications.html) | Intel® Xeon® Scalable Processors | Q3 2017 | 16 | 32 | 2.10GHz | 3.70GHz | 22MB L3 Cache | [Grenoble dahu](https://www.grid5000.fr/w/Grenoble:Hardware#dahu) / [yeti](https://www.grid5000.fr/w/Grenoble:Hardware#yeti) (exotic) |
| [AMD EPYC 7301](https://www.amd.com/en/support/cpu/amd-epyc/amd-epyc-7001-series/amd-epyc-7301) | AMD EPYC™ 7001 Series | June 20, 2017 | 16 | 32 | 2.20GHz | 2.70GHz | 64MB L3 Cache | [Lille chiclet](https://www.grid5000.fr/w/Lille:Hardware#chiclet) |
| [Intel Xeon Gold 5220 (Cascade Lake)](https://www.intel.fr/content/www/us/en/products/sku/193388/intel-xeon-gold-5220-processor-24-75m-cache-2-20-ghz/specifications.html) | 2nd Gen Intel® Xeon® Scalable Processors | Q2 2019 | 18 | 36 | 2.20GHz | 3.90GHz | 24.75MB | [Nancy gros](https://www.grid5000.fr/w/Nancy:Hardware#gros) |
| (exotic) [Intel Xeon E5-2698 v4 (Broadwell)](https://www.intel.fr/content/www/us/en/products/sku/91753/intel-xeon-processor-e52698-v4-50m-cache-2-20-ghz/specifications.html) | Intel® Xeon® Processor E5 v4 Family | Q1 2016 | 20 | 40 | 2.20GHz | 3.60GHz | 50MB Intel® Smart Cache | [Lyon gemini](https://www.grid5000.fr/w/Lyon:Hardware#gemini) |
| (exotic) [AMD EPYC 7352](https://www.amd.com/en/product/8806) | AMD EPYC™ 7002 Series | Q3 2019 | 24 | 48 | 2.30GHz | 3.20GHz | 128MB L3 Cache | [Nancy gruss](https://www.grid5000.fr/w/Nancy:Hardware#gruss) - [Grenoble servan](https://www.grid5000.fr/w/Grenoble:Hardware#servan) |
| (exotic) [AMD EPYC 7452](https://www.amd.com/en/product/8801) | AMD EPYC™ 7002 Series | Q3 2019 | 32 | 64 | 2.35GHz | 3.35GHz | 128MB L3 Cache | [Nancy grouille](https://www.grid5000.fr/w/Nancy:Hardware#grouille) |
| (exotic) [AMD EPYC 7642](https://www.amd.com/en/product/8786) | AMD EPYC™ 7002 Series | Q3 2019 | 48 | 96 | 2.30GHz | 3.30GHz | 256MB L3 Cache | [Lyon neowise](https://www.grid5000.fr/w/Lyon:Hardware#neowise) |
| (exotic) [AMD EPYC 7742](https://www.amd.com/en/product/8761) | AMD EPYC™ 7002 Series | Q3 2019 | 64 | 128 | 2.25GHz | 3.4GHz | 256MB L3 Cache | [Lyon sirius](https://www.grid5000.fr/w/Lyon:Hardware#sirius) |


## AWS: [Instances](https://aws.amazon.com/fr/ec2/instance-types/) and [Processors Characteristics](https://instances.vantage.sh/aws/ec2/t2.micro#Storage)

| Name | Product Collection | Launch Date | #Cores | #Threads | Basic Frequency | Maximum Frequency | Caches | Instances | Baremetal Instances |
:---|:---|:---|:---|:---|:---|:---|:---|:---|:---|
| [Intel Xeon Platinum 8252 (Cascade Lake)](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+Platinum+8252C+%40+3.80GHz&id=3895&cpuCount=2) | 2nd Gen Intel®  Xeon®  Scalable Processors | Q4 2020 | 12 | 24 | 3.80GHz | 4.50GHz | L1: 256KB, L2: 4.0MB, L3: 99MB | x2iezn, m5zn | m5zn.metal, x2iezn.metal |
| [AWS Graviton Processor](https://en.wikipedia.org/wiki/AWS_Graviton) | AWS Graviton | 2018 | 16 [Neoverse-N1](https://en.wikipedia.org/wiki/ARM_Neoverse) | 32 | 2.30GHz | Turbo not supported | :x: not found | a1 | a1.metal |
| [Intel Xeon 8375C (Ice Lake)](https://gadgetversus.com/processor/intel-xeon-platinum-8375c-specs/) | 3rd Gen Intel®  Xeon®  Scalable Processors | Q2 2021 | 32 | 64 | 2.90GHz | 3.5GHz | 54MB | c6i, c6id, c6in, m6i, m6in, m6id, m6idn, i4i, r6in, r6i, r6id, r6idn | c6i.metal, c6id.metal, c6in.metal, m6i.metal, m6id.metal, m6idn.metal, m6in.metal, r6in.metal, r6i.metal, r6id.metal, r6idn.metal, i4i.metal |
| [AMD EPYC 7571](https://www.cpubenchmark.net/cpu.php?cpu=AMD+EPYC+7571&id=3543) | AMD EPYC™ 7001 Series | Q3 2019 | 32 | 64 | 2.1GHz | 2.9GHz | L1: 192KB, L2: 1.0MB, L3: 8MB | t3a, m5a, m5ad, r5ad, r5a | none |
| [AMD EPYC 7R13](https://gadgetversus.com/processor/amd-epyc-7r13-specs/) | AMD EPYC™ 7003 Series | Q4 2021 | 48 | 96 | 2.70GHz | :x: not found | 48MB | c6a, m6a, r6a | m6a.metal, c6a.metal, r6a.metal |
| [AMD EPYC 7R32](https://www.cpubenchmark.net/cpu.php?cpu=AMD+EPYC+7R32&id=3894) | AMD EPYC™ 7002 Series | Q4 2020 | 48 | 96 | 2.8GHz | 3.30GHz | L1: 128KB, L2: 1.0MB, L3: 8MB | c5a, c5ad, c5d, g4ad | none |
| [AWS Graviton2 Processor](https://en.wikipedia.org/wiki/AWS_Graviton) | AWS Graviton | December 2019 | 64 [Neoverse-N1](https://en.wikipedia.org/wiki/ARM_Neoverse) | 128 | 2.50GHz | Turbo not supported | L1: 64KB, L2: 1MB  | t4g, c6g, c6gd, c6gn, m6gn, m6g, m6gd, x2gd, r6gd, r6g, i4g | m6g.metal, m6gd.metal, c6g.metal, c6gd.metal, x2gd.metal, r6gd.metal, r6g.metal |
| [AWS Graviton3 Processor](https://github.com/aws/aws-graviton-getting-started/tree/main#additional-resources) | AWS Graviton | May 2022 | 64 [Neoverse-N1](https://en.wikipedia.org/wiki/ARM_Neoverse) | :x: not found | 2.60GHz | Turbo not supported | L1: 64KB, L2:1MB | c7g, c7gn, m7g, r7g | m7g.metal, c7g.metal, r7g.metal |
| [Intel Xeon Platinum 8259 (Cascade Lake)](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+Platinum+8259CL+%40+2.50GHz&id=3671) | 2nd Gen Intel®  Xeon®  Scalable Processors  | Q1 2020 | 24 | 48 | 2.50GHz | 3.50GHz | L1: 128KB, L2: 2.0MB, L3: 36MB | m5n, m5dn, r5b, r5n, r5dn | m5n.metal, m5dn.metal, r5b.metal, r5n.metal, r5dn.metal |
| [Intel Xeon Platinum 8124M (Skylake)](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+Platinum+8124M+%40+3.00GHz&id=3352) | Intel® Xeon® Scalable Processors | Q4 2018 | 18 | 36 | 3.00GHz | 3.50GHz | L1: 1024KB, L2: 16.0MB, L3: 25MB | c5, c5n | c5n.metal |
| [Intel Xeon Platinum 8151 (Skylake)](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+Platinum+8151+%40+3.40GHz&id=3458) | Intel® Xeon® Scalable Processors | Q2 2019 | 2 | 4 | 3.4GHz | not supported | L1: 64KB, L2: 1.0MB, L3: 25MB | z1d | z1d.metal |
| [Intel Xeon E5-2686 v4 (Broadwell)](https://www.cpubenchmark.net/cpu.php?cpu=Intel+Xeon+E5-2686+v4+%40+2.30GHz&id=2870) | Intel® Xeon® Processor E5 v4 Family | Q4 2016 | 18 | 36 | 2.30GHz | 3.00GHz | L1: 128KB, L2: 0.5MB, L3: 45MB | i3 | i3.metal |
| Intel Xeon Platinum 8175 (Skylake) | Intel® Xeon® Scalable Processors | :x: not found | :x: not found | :x: not found | 3.10GHz | :x: not found | :x: not found | m5, m5d, r5, r5d, i3en | m5.metal, m5d.metal, r5.metal, r5d.metal, i3en.metal |

## Couple AWS / Grid'5000 (Suggestion)

**Priority order**: Generation > #cores > Launch date > caches size

*First possibility*: Only AWS, I compare the performance of VM instances vs. Baremetal instances. 

*Second possibility*: On AWS and on Grid’5000, I carry out experiments.

| Grid'5000 | AWS | Validation | Arguments |
|:---|:---|:---|:---|
| (exotic) Intel Xeon Gold 5218 (Cascade Lake) | Intel Xeon Platinum 8252 (Cascade Lake) | :x: | #core (16 / 12), launch date (Q2 2019 / Q4 2020) and cache (22MB / L1: 256KB, L2: 4.0MB, L3: 99MB) |
| Intel Xeon Gold 5220 (Cascade Lake) | Intel Xeon Platinum 8252 (Cascade Lake) | :x: | #core (18 / 12), launch date (Q2 2019 / Q4 2020) and cache (24.75MB / L1: 256KB, L2: 4.0MB, L3: 99MB) |
| Intel Xeon Gold 5220 (Cascade Lake) | Intel Xeon Platinum 8259 (Cascade Lake) | :x: | #core (18 / 24), launch date (Q2 2019 / Q1 2020) and cache (24.75MB / L1: 128KB, L2: 2.0MB, L3: 36MB) |
| (exotic) Intel Xeon Gold 5218 (Cascade Lake) | Intel Xeon Platinum 8259 (Cascade Lake) | :x: | #core (16 / 24), launch date (Q2 2019 / Q1 2020) and cache (22MB / L1: 128KB, L2: 2.0MB, L3: 36MB) |
|  |  |  |
| (exotic) AMD EPYC 7452 | AMD EPYC 7571 | :x: | #core (32 / 32),launch date (Q3 2019 / Q3 2019) and cache (128MB L3 Cache / L1: 192KB, L2: 1.0MB, L3: 8MB) |
| (exotic) AMD EPYC 7642 | AMD EPYC 7R32 | :x: | #core (48 / 48), launch date (Q4 2020 / Q4 2021) and cache (256MB L3 Cache / L1: 128KB, L2: 1.0MB, L3: 8MB) |
|  |  |  |
| Intel Xeon Gold 6130 (Skylake) | Intel Xeon Platinum 8124M (Skylake) | :heavy_check_mark: | #core (16 / 18), launch date (Q3 2017 / Q4 2018) and cache (22MB L3 Cache / L1: 1024KB, L2: 16.0MB, L3: 25MB) |
|  |  |  |
| Intel Xeon E5-2680 v4 (Broadwell) | Intel Xeon E5-2686 v4 (Broadwell) | :x: | #core (14 / 18), launch date (Q1 2016 / Q4 2016) and cache (35MB / L1: 128KB, L2: 0.5MB, L3: 45MB) |
| (exotic) Intel Xeon E5-2698 v4 (Broadwell) | Intel Xeon E5-2686 v4 (Broadwell) | :x: | #core (20 / 18), launch date (Q1 2016 / Q4 2016) and cache (50MB / L1: 128KB, L2: 0.5MB, L3: 45MB) |

Legend:
* :heavy_check_mark: : My final choice
* :x: : Altenative

### Explanation of choices

* **Cascade Lake** selection (the first four)

Among Cascade Lake processors, I found four fair combinations.
 - Intel Xeon Platinum 8252 is use for `x2iezn` (**memory optimized**) and `m5zn` (**general purpose**) instances.
 
 - Intel Xeon Platinum 8259 is use for `m5n`, `m5dn` (**general purpose**), `r5b`, `r5n` and `r5dn` (**memory optimized**).

So, if I compare cache size, the best Cascade Lake combination is **Intel Xeon Gold 5220 (Grid'5000)** and **Intel Xeon Platinum 8259 (AWS)**.

* **AMD** selection (immediately after)

Among AMD processors, I selected two more or less fair combinations. They aren't use by baremetal instances.

* **Skylake** selection (just after)

Among Skylake processors, I found a single fair combination. This is use for `c5` and `c5n` (**Compute Optimized**) instances.

* **Broadwell** selection (the last two)

Among Broadwell processors, I found two fair combinations, but Broadwell is **quite old**, the only AWS instance which used this processor is `i3`, an instance for **storage optimized**.

#### **Finally**

Between the Skylake selection and Cascade Lake selection, I have chosen **Skylake** selection because these instances is for **Compute Optimized**.

So, **Intel Xeon Gold 6130** and **Intel Xeon Platinum 8124M** is the best combinaison for the 2 possibilities.

*Notice*: 
```TXT
Clusters with the exotic type either have a non-x86 architecture, or are specific enough to warrant this type.
```

***

:arrow_backward: [README.md](./README.md)

***
