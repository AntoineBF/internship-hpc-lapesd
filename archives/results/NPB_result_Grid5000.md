# **NPB Results - Grid'5000** 

*old file*

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For BT (Operation type: floating point): (square number)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 223.88 | 12802.86 | 3200.72 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 227.62 | 12760.54 | 3190.14 |
| 9 | 104.63 | 27393.93 | 3043.77 |
| 16 | 67.83 | 42254.76 | 2640.92 |
| 25 | 47.60 | 60221.70 | 2408.87 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 224.61 | 12761.30 | 3190.32 |
| 9 | 105.25 | 27233.08 | 3025.90 |
| 16 | 65.34 | 43866.86 | 2741.68 |
| 25 | 48.02 | 59693.41 | 2387.74 |

## **For EP (Operation type: Random numbers generated): (all)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 50.75 | 169.26 | 42.31 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 50.95 | 168.59 | 42.15 |
| 8 | 26.26 | 327.15 | 40.89 |
| 16 | 14.50 | 592.45 | 37.03 |
| 32 | 8.06 | 1065.17 | 33.29 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 51.19 | 167.82 | 41.96 |
| 8 | 26.33 | 326.20 | 40.78 |
| 16 | 14.44 | 595.00 | 37.19 |
| 32 | 8.10 | 1060.03 | 33.13 |

## **For FT (Operation type: floating point): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 50.18 | 7899.42 | 1974.86 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 49.61 | 7990.60 | 1997.65 |
| 8 | 26.26 | 15095.21 | 1886.90 |
| 16 | 15.01 | 26410.61 | 1650.66 |
| 32 | 9.45 | 41935.50 | 1310.48 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 49.95 | 7935.19 | 1983.80 |
| 8 | 26.49 | 14961.15 | 1870.14 |
| 16 | 15.08 | 26282.56 | 1642.66 |
| 32 | 9.77 | 40576.69 | 1268.02 |

## **For IS (Operation type: keys ranked): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 48.91 | 439.02 | 109.76 |
| 8 | 27.56 | 779.15 | 97.39 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 49.11 | 437.31 | 109.33 |
| 8 |  |  |  |
| 16 | 17.18 | 1249.83 | 78.11 |
| 32 | 12.42 | 1728.95 | 54.03 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 49.84 | 430.89 | 107.72 |
| 8 | 27.83 | 771.72 | 96.47 |
| 16 | 16.72 | 1284.12 | 80.26 |
| 32 | 12.44 | 1725.96 | 53.94 |

## **For CG (Operation type: floating point): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 30.62 | 4682.18 | 1170.54 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 25.79 | 5559.05 | 1389.76 |
| 8 | 12.83 | 11169.97 | 1396.25 |
| 16 | 7.53 | 19024.44 | 1189.03 |
| 32 | 5.57 | 25724.34 | 803.89 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 26.48 | 5413.79 | 1353.45 |
| 8 | 12.98 | 11047.88 | 1380.99 |
| 16 |  |  |  |
| 32 |  |  |  |

Fourth:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 26.32 | 5446.46 | 1361.62 |
| 8 | 13.14 | 10911.54 | 1363.94 |
| 16 | 7.62 | 18803.13 | 1175.20 |
| 32 | 5.72 | 25046.84 | 782.71 |

## **For LU (Operation type: floating point): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 118.85 | 17155.41 | 4288.85 |
| 8 | 64.63 | 31547.16 | 3943.40 |
| 16 | 36.70 | 55561.19 | 3472.57 |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 119.76 | 17025.15 | 4256.29 |
| 8 | 65.34 | 31203.81 | 3900.48 |
| 16 | 37.44 | 54456.90 | 3403.56 |
| 32 | 25.21 | 80865.40 | 2527.04 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 120.38 | 16937.48 | 4234.37 |
| 8 |  |  |  |
| 16 | 37.47 | 54411.44 | 3400.72 |
| 32 | 25.48 | 80034.22 | 2501.07 |

## **For MG (Operation type: floating point): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 9.79 | 15906.91 | 3976.73 |
| 8 | 4.85 | 32134.21 | 4016.78 |
| 16 | 3.01 | 51692.40 | 3230.78 |
| 32 | 2.75 | 56602.23 | 1768.82 |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 10.04 | 15513.54 | 3878.39 |
| 8 | 4.73 | 32888.82 | 4111.10 |
| 16 | 3.04 | 51161.13 | 3197.57 |
| 32 | 2.72 | 57167.86 | 1786.50 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 10.02 | 15540.06 | 3885.02 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

## **For SP (Operation type: floating point): (square number)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 165.96 | 8737.85 | 2184.46 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 168.22 | 8620.46 | 2155.11 |
| 9 | 73.20 | 19808.96 | 2201.00 |
| 16 | 48.86 | 29679.62 | 1854.98 |
| 25 | 41.81 | 34680.11 | 1387.20 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 166.74 | 8696.70 | 2174.18 |
| 9 | 73.46 | 19739.69 | 2193.30 |
| 16 | 49.01 | 29589.78 | 1849.36 |
| 25 | 42.08 | 34460.25 | 1378.41 |

## **For BT-IO: (square number)**

 ### **Full:**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 228.53 | 12542.29 | 3135.57 |
| 9 | 109.97 | 26064.58 | 2896.06 |
| 16 |  |  |  |
| 25 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 229.64 | 12481.48 | 3120.37 |
| 9 | 109.91 | 26078.45 | 2897.61 |
| 16 | 73.48 | 39006.33 | 2437.90 |
| 25 | 54.37 | 52718.20 | 2108.73 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 229.54 | 12486.80 | 3121.70 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |

 ### **Simple:**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 233.95 | 12251.61 | 3062.90 |
| 9 | 116.94 | 24511.73 | 2723.53 |
| 16 |  |  |  |
| 25 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 232.62 | 12321.83 | 3080.45 |
| 9 | 116.45 | 24613.58 | 2734.84 |
| 16 | 82.13 | 34897.85 | 2181.12 |
| 25 | 65.07 | 44047.87 | 1761.91 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 234.38 | 12229.11 | 3057.28 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |

 ### **Fortran:**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 236.19 | 12135.23 | 3033.81 |
| 9 | 115.21 | 24877.96 | 2764.22 |
| 16 |  |  |  |
| 25 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 235.52 | 12170.05 | 3042.51 |
| 9 | 115.63 | 24787.42 | 2754.16 |
| 16 | 80.32 | 35684.60 | 2230.29 |
| 25 | 64.07 | 44733.96 | 1789.36 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 236.13 | 12138.37 | 3034.59 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |

 ### **Epio:**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 229.96 | 12464.50 | 3116.12 |
| 9 | 109.22 | 26242.71 | 2915.86 |
| 16 |  |  |  |
| 25 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 230.75 | 12421.48 | 3105.37 |
| 9 | 108.60 | 26393.45 | 2932.61 |
| 16 | 75.27 | 38079.59 | 2379.97 |
| 25 | 53.68 | 53395.87 | 2135.83 |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 230.83 | 12417.44 | 3104.36 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |

## **For BT-MZ (Operation type: floating point):**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 145.22 | 16712.28 | 4178.07 |
| 8 | 75.50 | 32146.97 | 4018.37 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 145.91 | 16633.61 | 4158.40 |
| 8 | 75.46 | 32163.60 | 4020.45 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 145.18 | 16716.97 | 4179.24 |
| 8 | 75.59 | 32105.19 | 4013.15 |
| 16 |  |  |  |
| 32 |  |  |  |

Fourth:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 146.62 | 16552.46 | 4138.12 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

## **For LU-MZ (Operation type: floating point):**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 115.16 | 16676.64 | 4169.16 |
| 8 | 65.93 | 29128.31 | 3641.04 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 114.55 | 16765.46 | 4191.36 |
| 8 | 65.43 | 29350.89 | 3668.86 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 113.05 | 16988.74 | 4247.18 |
| 8 | 66.04 | 29080.23 | 3635.03 |
| 16 |  |  |  |
| 32 |  |  |  |

Fourth:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 112.59 | 17057.76 | 4264.44 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

## **For SP-MZ (Operation type: floating point):**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 78.08 | 15686.29 | 3921.57 |
| 8 | 40.64 | 30133.90 | 3766.74 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 78.28 | 15645.01 | 3911.25 |
| 8 | 40.63 | 30147.20 | 3768.40 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 78.28 | 15646.22 | 3911.56 |
| 8 | 40.66 | 30118.69 | 3764.84 |
| 16 |  |  |  |
| 32 |  |  |  |

Fourth:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 78.21 | 15659.17 | 3914.79 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

***

:arrow_backward: [README.md](../../README.md)

***
