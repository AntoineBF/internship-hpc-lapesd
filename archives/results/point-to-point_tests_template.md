# Point-to-point tests - 2 nodes

## Standard

### Grid'5000

* osu_bibw:

```UNIX
```

* osu_bw:

```UNIX
```

* osu_latency:

```UNIX
```

* osu_latency_mt:

```UNIX
```

* osu_mbw_mr:

```UNIX
```

* osu_multi_lat:

```UNIX
```

### AWS

* osu_bibw:

```UNIX
```

* osu_bw:

```UNIX
```

* osu_latency:

```UNIX
```

* osu_latency_mt:

```UNIX
```

* osu_mbw_mr:

```UNIX
```

* osu_multi_lat:

```UNIX
```


## Persistent

### Grid'5000

* osu_bibw_persistent:

```UNIX
```

* osu_bw_persistent:

```UNIX
```

* osu_latency_persistent:

```UNIX
```

### AWS

* osu_bibw_persistent:

```UNIX
```

* osu_bw_persistent:

```UNIX
```

* osu_latency_persistent:

```UNIX
```

