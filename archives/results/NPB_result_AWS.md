# **NPB Results - AWS (c5n instances)**

*old file*

(CPUtype: `Intel Xeon Platinum 8124M`, OS: `Amazon Linux 2`, OpenMPI: `5.1.0a1`, GCC: `7.3.1`)

## **For BT (Operation type: floating point): (square number)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 390.83 | 7338.73 | 1834.68 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 390.39 | 7342.12 | 1835.53 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 391.00 | 7330.68 | 1832.67 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

## **For EP (Operation type: Random numbers generated): (all)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 54.60 | 157.32 | 39.33 |
| 8 | 27.89 | 307.95 | 38.49 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 54.50 | 157.61 | 39.40 |
| 8 | 27.93 | 307.96 | 38.50 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 54.51 | 157.58 | 39.39 |
| 8 | 27.40 | 313.50 | 39.19 |
| 16 |  |  |  |
| 32 |  |  |  |

## **For FT (Operation type: floating point): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 78.06 | 5078.04 | 1269.51 |
| 8 | 39.43 | 10052.90 | 1256.61 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 77.77 | 5097.23 | 1274.31 |
| 8 | 39.77 | 9968.19 | 1246.02 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 78.08 | 5076.83 | 1269.21 |
| 8 | 39.44 | 10051.61 | 1256.45 |
| 16 |  |  |  |
| 32 |  |  |  |

## **For IS (Operation type: keys ranked): (power 2)**  C

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 3.83 | 350.59 | 87.65 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 3.79 | 354.24 | 88.56 |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |
| 32 |  |  |  |

## **For CG (Operation type: floating point): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 107.22 | 1336.95 | 334.24 |
| 8 | 24.05 | 5959.77 | 744.97 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 107.21 | 1337.02 | 334.26 |
| 8 | 24.18 | 5927.93 | 740.99 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 107.06 | 1338.98 | 334.74 |
| 8 | 24.05 | 5961.54 | 745.19 |
| 16 |  |  |  |
| 32 |  |  |  |

## **For LU (Operation type: floating point): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 198.83 | 10254.89 | 2563.72 |
| 8 | 106.76 | 19098.06 | 2387.26 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 200.06 | 10192.15 | 2548.04 |
| 8 | 106.77 | 19097.44 | 2387.18 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 199.58 | 10217.04 | 2554.26 |
| 8 | 106.30 | 19181.63 | 2397.70 |
| 16 |  |  |  |
| 32 |  |  |  |

## **For MG (Operation type: floating point): (power 2)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 20.47 | 7604.77 | 1901.19 |
| 8 | 8.85 | 17597.57 | 2199.70 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 20.02 | 7776.55 | 1944.14 |
| 8 | 8.83 | 17638.98 | 2204.87 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 20.92 | 7443.60 | 1860.90 |
| 8 | 8.85 | 17591.58 | 2198.95 |
| 16 |  |  |  |
| 32 |  |  |  |

## **For SP (Operation type: floating point): (square number)**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 309.77 | 4681.18 | 1170.29 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 311.98 | 4648.01 | 1162.00 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 311.27 | 4658.60 | 1164.65 |
| 4 | 315.41 | 4597.47 | 1149.37 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

## **For BT-IO: (square number)**

 ### **Full:**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 395.29 | 7251.00 | 1812.75 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 399.67 | 7171.56 | 1792.89 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 |  |  |  |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

 ### **Simple:**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 392.97 | 7293.87 | 1823.47 |:x:
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 397.04 | 7219.18 | 1804.80 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 397.12 | 7217.58 | 1804.40 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

 ### **Fortran:**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 407.72 | 7030.07 | 1757.52 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 411.48 | 6965.86 | 1741.47 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 |  |  |  |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

 ### **Epio:**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 399.82 | 7169.01 | 1792.25 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 402.28 | 7125.02 | 1781.25 |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 |  |  |  |
| 9 |  |  |  |
| 16 |  |  |  |
| 25 |  |  |  |
| 36 |  |  |  |

## **For BT-MZ (Operation type: floating point):**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 279.38 | 8687.15 | 2171.79 |
| 8 | 137.64 | 17632.30 | 2204.04 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 279.27 | 8690.69 | 2172.67 |
| 8 | 137.76 | 17617.23 | 2202.15 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 279.80 | 8674.06 | 2168.51 |
| 8 | 137.47 | 17654.72 | 2206.84 |
| 16 |  |  |  |
| 32 |  |  |  |

## **For LU-MZ (Operation type: floating point):**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 208.50 | 9211.31 | 2302.83 |
| 8 | 109.25 | 17578.97 | 2197.37 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 208.19 | 9224.99 | 2306.25 |
| 8 | 108.95 | 17627.29 | 2203.41 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 209.46 | 9168.93 | 2292.23 |
| 8 | 109.11 | 17601.74 | 2200.22 |
| 16 |  |  |  |
| 32 |  |  |  |

## **For SP-MZ (Operation type: floating point):**

First:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 142.74 | 8580.52 | 2145.13 |
| 8 | 71.42 | 17149.65 | 2143.71 |
| 16 |  |  |  |
| 32 |  |  |  |

Second:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 142.67 | 8584.66 | 2146.16 |
| 8 | 71.48 | 17135.31 | 2141.91 |
| 16 |  |  |  |
| 32 |  |  |  |

Third:
| #Cores | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 4 | 142.88 | 8571.66 | 2142.92 |
| 8 | 71.47 | 17135.89 | 2141.99 |
| 16 |  |  |  |
| 32 |  |  |  |

***

:arrow_backward: [README.md](../../README.md)

***
