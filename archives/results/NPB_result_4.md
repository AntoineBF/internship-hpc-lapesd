# **NPB Results - 4 cores**

(CPUtype: `Intel Xeon Gold 6130`, OS: `debian11`, OpenMPI: `4.1.0`, GCC: `10.2.1`)

## **For BT (Operation type: floating point): (square number)**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | 0.02 | 13129.08 | 3282.27 |
| W | 0.47 | 16260.82 | 4065.21 |
| A | 12.28 | 13705.98 | 3426.50 |
| B | 52.36 | 13409.37 | 3352.34 |
| C | 218.92 | 13092.97 | 3273.24 |
| D | 3273.24 | 13766.50 | 3441.63 |

## **For EP (Operation type: Random numbers generated): (power 2)**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | 0.07 | 475.48 | 118.87 |
| W | 0.14 | 478.77 | 119.69 |
| A | 1.11 | 484.77 | 121.19 |
| B | 4.42 | 485.50 | 121.38 |
| C | 17.71 | 485.00 | 121.25 |
| D | 283.42 | 484.93 | 121.23 |

## **For FT (Operation type: floating point): (power 2)**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | 0.02 | 10492.86 | 2623.22 |
| W | 0.04 | 10434.11 | 2608.53 |
| A | 0.82 | 8684.05 | 2171.01 |
| B | 11.00 | 8370.62 | 2092.66 |
| C | 46.05 | 8607.29 | 2151.82 |
| D | 1214.32 | 7381.71 | 1845.43 |

## **For IS (Operation type: keys ranked): (power 2)**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | 0.00 | 549.91 | 137.48 |
| W | 0.02 | 510.19 | 127.55 |
| A | 0.23 | 365.13 | 91.28 |
| B | 0.71 | 472.60 | 118.15 |
| C | 2.77 | 484.28 | 121.07 |
| D | 48.73 | 440.71 | 110.18 |

## **For CG (Operation type: floating point): (power 2)**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | 0.01 | 4937.14 | 1234.28 |
| W | 0.07 | 6079.80 | 1519.95 |
| A | 0.22 | 6829.01 | 1707.25 |
| B | 9.26 | 5911.04 | 1477.76 |
| C | 26.23 | 5465.19 | 1366.30 |
| D | 2073.88 | 1756.56 | 439.14 |

## **For LU (Operation type: floating point): (power 2)**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | anticipation: too short |  |  |
| W | 1.13 | 16036.34 | 4009.09 |
| A | 6.79 | 17571.42 | 4392.85 |
| B | 29.65 | 16821.46 | 4205.36 |
| C | 123.99 | 16445.29 | 4111.32 |
| D | anticipation: too long |  |  |

## **For MG (Operation type: floating point): (power 2)**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | 0.00 | 5791.92 | 1447.98 |
| W | 0.03 | 15846.85 | 3961.71 |
| A | 0.26 | 14845.25 | 3711.31 |
| B | 1.21 | 16120.50 | 4030.12 |
| C | 9.86 | 15782.61 | 3945.65 |
| D | :x: error | :x: | :x: |

## **For SP (Operation type: floating point): (square number)**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | 0.01 | 7816.42 | 1954.10 |
| W | 0.98 | 14444.38 | 3611.10 |
| A | 6.57 | 12933.57 | 3233.39 |
| B | 33.37 | 10638.59 | 2659.65 |
| C | 161.01 | 9006.45 | 2251.61 |
| D | 3536.34 | 8352.14 | 2088.03 |

## **For BT-IO: (square number)**

### **Full:**

| Classes | Time in seconds | Mop/s total | Mop/s/process | data written (MB) |
|:---|:---|:---|:---|:---|
| S | anticipation: too short |  |  |  |
| W | 0.58 | 13337.00 | 3334.25 | 22.12 |
| A | 13.56 | 12556.25 | 3064.06 | 419.43 |
| B | 57.53 | 12205.96 | 3051.49 | 1697.93 |
| C | 237.72 | 12057.40 | 3014.35 | 6802.44 |
| D | anticipation: too long |  |  |  |

### **Simple:**

| Classes | Time in seconds | Mop/s total | Mop/s/process | data written (MB) |
|:---|:---|:---|:---|:---|
| S | 0.79 | 289.02 | 72.26 | 0.83 |
| W | 12.48 | 618.36 | 154.59 | 22.12 |
| A | 99.07 | 1698.74 | 424.69 | 419.43 |
| B | 326.86 | 2148.29 | 537.07 | 1697.93 |
| C | anticipation: too long |  |  |  |
| D | anticipation: too long |  |  |  |

### **Fortran:**

| Classes | Time in seconds | Mop/s total | Mop/s/process | data written (MB) |
|:---|:---|:---|:---|:---|
| S | 0.03 | 8149.03 | 2037.26 | 0.83 |
| W | 0.73 | 10579.03 | 2644.76 | 22.12 |
| A | 18.68 | 9008.39 | 2252.10 | 419.43 |
| B | 70.09 | 10017.78 | 2504.45 | 1697.93 |
| C | 341.78 | 8386.23 | 2096.56 | 6802.44 |
| D | anticipation: too long |  |  |  |

### **Epio:**

| Classes | Time in seconds | Mop/s total | Mop/s/process | data written (MB) |
|:---|:---|:---|:---|:---|
| S | anticipation: too short |  |  |  |
| W | 0.51 | 15175.98 | 3794.00 | 22.12 |
| A | 12.64 | 13310.04 | 3327.51 | 419.43 |
| B | 54.08 | 12983.47 | 3245.87 | 1697.93 |
| C | 222.14 | 12903.04 | 3225.76 | 6802.44 |
| D | anticipation: too long |  |  |  |

## **For BT-MZ (Operation type: floating point):**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | anticipation: too short |  |  |
| W | 0.87 | 16406.27 | 4101.57 |
| A | 10.85 | 13477.97 | 3369.49 |
| B | 36.85 | 16313.81 | 4078.45 |
| C | 148.50 | 16342.98 | 4085.75 |
| D | anticipation: too long |  |  |

## **For LU-MZ (Operation type: floating point):**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | anticipation: too short |  |  |
| W | 0.61 | 19136.43 | 4784.11 |
| A | 5.41 | 18987.86 | 4746.96 |
| B | 26.87 | 16707.21 | 4176.80 |
| C | 134.48 | 14280.88 | 3570.22 |
| D | anticipation: too long |  |  |

## **For SP-MZ (Operation type: floating point):**

| Classes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| S | anticipation: too short |  |  |
| W | 0.47 | 15450.40 | 3862.60 |
| A | 8.20 | 8991.79 | 2247.95 |
| B | 26.77 | 11330.06 | 2832.52 |
| C | 82.43 | 14858.72 | 3714.68 |
| D | anticipation: too long |  |  |

***

:arrow_backward: [README.md](../../README.md)

***
