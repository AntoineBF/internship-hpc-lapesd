# **NPB Results - Grid'5000 - 2nd series**

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.84 | 151.75 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.61 | 290.86 | 145.43 |
| 4 | 2.86 | 469.63 | 117.41 |
| 8 | 1.52 | 881.44 | 110.18 |
| 16 | 0.84 | 1597.79 | 99.86 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.59 | 373.93 | 93.48 |
| 4 | 2.45 | 547.67 | 547.67 |
| 8 | 1.83 | 733.04 | 45.81 |
| 16 | 1.53 | 878.62 | 27.46 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.05 | 653.70 | 81.71 |
| 4 | 1.50 | 892.32 | 55.77 |
| 8 | 1.44 | 935.16 | 29.22 |
| 16 | 1.10 | 1216.61 | 19.01 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.12 | 1201.14 | 75.07 |
| 4 | 0.85 | 1572.77 | 49.15 |
| 8 | 0.81 | 1655.69 | 25.87 |
| 16 | 4.42 | 303.82 | 2.37 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 195.10 | 44.03 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 99.16 | 86.63 | 43.31 |
| 4 | 50.45 | 170.23 | 45.56 |
| 8 | 26.61 | 322.86 | 40.36 |
| 16 | 14.67 | 585.46 | 36.59 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 48.93 | 175.56 | 43.89 |
| 4 | 25.91 | 331.47 | 41.43 |
| 8 | 13.34 | 644.10 | 40.26 |
| 16 | 7.27 | 1182.07 | 36.94 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.47 | 351.00 | 43.87 |
| 4 | 12.95 | 663.49 | 41.47 |
| 8 | 6.67 | 1288.38 | 40.26 |
| 16 | 3.67 | 2341.38 | 36.58 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.29 | 699.01 | 43.69 |
| 4 | 6.49 | 1323.70 | 41.37 |
| 8 | 3.42 | 2512.88 | 39.26 |
| 16 | 1.84 | 4662.81 | 36.43 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 221.37 | 647.55 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.99 | 2867.35 | 1433.68 |
| 4 | 26.36 | 5439.00 | 1359.75 |
| 8 | 12.77 | 11222.20 | 1402.78 |
| 16 | 7.55 | 18984.48 | 1186.53 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 26.75 | 5358.96 | 1339.74 |
| 4 | 14.28 | 10035.91 | 1254.49 |
| 8 | 9.96 | 14397.54 | 899.85 |
| 16 | 7.13 | 20106.63 | 628.33 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.72 | 9119.63 | 1139.95 |
| 4 | 9.83 | 14581.48 | 911.34 |
| 8 | 6.92 | 20837.55 | 651.17 |
| 16 | 6.80 | 21072.11 | 329.25 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.96 | 14393.94 | 899.62 |
| 4 | 7.40 | 19371.69 | 605.37 |
| 8 | 5.51 | 26012.95 | 406.45 |
| 16 | 4.52 | 31712.42 | 247.75 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 36.68 | 4244.96 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 19.44 | 8010.25 | 4005.13 |
| 4 | 9.63 | 16160.84 | 4040.21 |
| 8 | 4.75 | 32807.35 | 4100.92 |
| 16 | 3.02 | 51525.63 | 3220.35 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.72 | 16018.35 | 4004.59 |
| 4 | 5.07 | 30696.38 | 3837.05 |
| 8 | 2.96 | 52619.72 | 3288.73 |
| 16 | 1.90 | 82087.26 | 2565.23 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.91 | 31735.85 | 3966.98 |
| 4 | 2.96 | 52565.15 | 3285.32 |
| 8 | 1.76 | 88387.45 | 2762.11 |
| 16 | 1.07 | 145251.28 | 2269.55 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.79 | 55808.17 | 3488.01 |
| 4 | 1.59 | 98167.74 | 3067.74 |
| 8 | 0.88 | 176363.61 | 2755.68 |
| 16 | 0.78 | 199885.83 | 1561.61 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 171.10 | 2311.76 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 96.71 | 4098.88 | 2049.44 |
| 4 | 49.57 | 7995.97 | 1998.99 |
| 8 | 26.31 | 15067.66 | 1883.46 |
| 16 | 14.97 | 26470.92 | 1654.43 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 56.58 | 7006.28 | 1751.57 |
| 4 | 32.79 | 12089.78 | 1511.22 |
| 8 | 21.74 | 18229.92 | 1139.37 |
| 16 | 17.45 | 22713.39 | 709.79 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 30.76 | 12886.75 | 1610.84 |
| 4 | 19.12 | 20732.02 | 1295.75 |
| 8 | 14.12 | 28082.53 | 877.58 |
| 16 | 11.18 | 35460.82 | 554.08 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.06 | 24689.32 | 1543.08 |
| 4 | 10.80 | 36692.44 | 1146.64 |
| 8 | 8.23 | 48138.53 | 752.16 |
| 16 | 8.99 | 44092.28 | 344.47 |


***

:arrow_backward: [README.md](../../../README.md)

***
