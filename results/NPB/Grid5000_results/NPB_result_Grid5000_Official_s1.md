# **NPB Results - Grid'5000 - 1st series** 

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

command:
```UNIX
mpirun -np 4 --mca orte_rsh_agent oarsh -machinefile ../hostfolder/hostfile.ompi.g5k2/hostfile.ompi2 --mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib --mca btl_tcp_if_include enp24s0f0 bin/ep.C.x
```

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.86 | 151.51 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.63 | 293.03 | 146.52 |
| 4 | 2.85 | 467.69 | 116.92 |
| 8 | 1.53 | 878.96 | 109.87 |
| 16 | 0.85 | 1572.52 | 98.28 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.64 | 368.98 | 92.24 |
| 4 | 2.47 | 544.07 | 68.01 |
| 8 | 1.83 | 731.70 | 45.73 |
| 16 | 1.53 | 875.80 | 27.37 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.11 | 634.98 | 79.37 |
| 4 | 1.50 | 892.62 | 55.79 |
| 8 | 1.30 | 1034.14 | 32.32 |
| 16 | 1.14 | 1182.04 | 18.47 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.10 | 1224.44 | 76.53 |
| 4 | 0.85 | 1571.31 | 49.10 |
| 8 | 0.81 | 1665.61 | 26.03 |
| 16 | 4.31 | 311.20 | 2.43 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 195.27 | 43.98 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 98.49 | 88.26 | 44.13 |
| 4 | 50.85 | 168.94 | 42.23 |
| 8 | 26.66 | 326.96 | 40.87 |
| 16 | 14.20 | 605.62 | 37.85 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.05 | 175.13 | 43.78 |
| 4 | 25.82 | 332.68 | 41.58 |
| 8 | 13.43 | 639.59 | 39.97 |
| 16 | 7.25 | 1184.86 | 37.03 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.45 | 351.30 | 43.91 |
| 4 | 12.97 | 662.16 | 41.38 |
| 8 | 6.79 | 1266.00 | 39.56 |
| 16 | 3.68 | 2332.85 | 36.45 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.33 | 696.68 | 43.54 |
| 4 | 6.65 | 1292.08 | 40.38 |
| 8 | 3.38 | 2544.51 | 39.76 |
| 16 | 1.84 | 4663.13 | 36.43 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 223.01 | 642.79 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 48.30 | 2967.75 | 1483.87 |
| 4 | 26.36 | 5437.12 | 1359.28 |
| 8 | 13.00 | 10977.12 | 1372.14 |
| 16 | 7.73 | 18815.72 | 1175.98 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 36.29 | 3949.66 | 987.42 |
| 4 | 14.77 | 9705.59 | 1213.20 |
| 8 | 9.92 | 14454.85 | 903.43 |
| 16 | 6.83 | 20974.91 | 655.47 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.66 | 9154.82 | 1144.35 |
| 4 | 9.84 | 14573.29 | 910.83 |
| 8 | 7.06 | 20312.75 | 634.77 |
| 16 | 6.98 | 20535.90 | 320.87 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.63 | 14890.94 | 930.68 |
| 4 | 7.11 | 20172.24 | 630.38 |
| 8 | 5.53 | 25932.29 | 405.19 |
| 16 | 4.53 | 31658.69 | 247.33 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 36.95 | 4213.43 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 19.59 | 7949.33 | 3974.67 |
| 4 | 9.85 | 15802.15 | 3950.54 |
| 8 | 4.79 | 32608.40 | 4076.05 |
| 16 | 3.06 | 51344.81 | 3209.05 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.92 | 15698.58 | 3924.65 |
| 4 | 5.19 | 30002.46 | 3750.31 |
| 8 | 2.92 | 53286.54 | 3330.41 |
| 16 | 1.90 | 81744.71 | 2554.52 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 5.01 | 31094.08 | 3886.76 |
| 4 | 3.06 | 50975.56 | 3185.97 |
| 8 | 1.80 | 87086.12 | 2721.44 |
| 16 | 1.10 | 143883.81 | 2248.18 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.72 | 57171.03 | 3573.19 |
| 4 | 1.57 | 98960.98 | 3092.53 |
| 8 | 0.88 | 176581.14 | 2759.08 |
| 16 | 0.80 | 195819.90 | 1529.84 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 171.60 | 2309.89 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 96.64 | 4183.73 | 2091.86 |
| 4 | 50.27 | 7884.59 | 1971.15 |
| 8 | 26.49 | 15078.66 | 1884.83 |
| 16 | 15.00 | 26651.83 | 1665.74 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 56.96 | 6958.84 | 1739.71 |
| 4 | 33.42 | 11861.24 | 1482.65 |
| 8 | 21.82 | 18162.76 | 1135.17 |
| 16 | 16.95 | 23382.17 | 730.69 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 30.60 | 12951.82 | 1618.98 |
| 4 | 19.05 | 20812.31 | 1300.77 |
| 8 | 13.50 | 29352.15 | 917.25 |
| 16 | 11.16 | 35531.21 | 555.18 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.87 | 24971.91 | 1560.74 |
| 4 | 10.80 | 36703.40 | 1146.98 |
| 8 | 8.21 | 48297.97 | 754.66 |
| 16 | 8.72 | 45439.91 | 355.00 |


***

:arrow_backward: [README.md](../../../README.md)

***
