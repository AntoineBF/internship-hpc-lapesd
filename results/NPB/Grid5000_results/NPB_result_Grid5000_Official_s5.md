# **NPB Results - Grid'5000 - 5th series**

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.69 | 152.55 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.62 | 290.16 | 145.08 |
| 4 | 2.87 | 466.03  | 111.51 |
| 8 | 1.54 | 873.91 | 109.34 |
| 16 | 0.85 | 1572.62 | 98.29 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.60 | 372.90 | 93.22 |
| 4 | 2.47 | 542.46 | 67.81 |
| 8 | 1.83 | 732.41 | 45.78 |
| 16 | 1.52 | 883.70 | 27.62 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.10 | 638.57 | 79.82 |
| 4 | 1.57 | 855.47 | 53.47 |
| 8 | 1.31 | 1021.03 | 31.91 |
| 16 | 1.19 | 1124.00 | 17.56 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.12 | 1202.65 | 75.17 |
| 4 | 1.08 | 1245.28 | 38.92 |
| 8 | 0.82 | 1627.75 | 25.43 |
| 16 | 3.99 | 336.71 | 2.63 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 195.16 | 43.99 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 98.55 | 87.08 | 43.54 |
| 4 | 51.12 | 168.05 | 42.01 |
| 8 | 26.69 | 321.93 | 40.24 |
| 16 | 14.46 | 595.32 | 37.21 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.15 | 174.75 | 43.69 |
| 4 | 25.95 | 331.03 | 41.38 |
| 8 | 13.32 | 644.98 | 40.31 |
| 16 | 7.32 | 1173.60 | 36.68 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.46 | 351.16 | 43.90 |
| 4 | 12.89 | 666.16 | 41.64 |
| 8 | 6.67 | 1287.78 | 40.24 |
| 16 | 3.64 | 2359.74 | 36.87 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.44 | 690.23 | 43.14 |
| 4 | 6.69 | 1284.34 | 40.14 |
| 8 | 3.37 | 2552.01 | 39.88 |
| 16 | 1.84 | 4666.04 | 36.45 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 96.04 | 2319.78 |
| 1 | 222.15 | 645.28 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.77 | 2880.16 | 1440.08
| 4 | 25.32 | 5661.24 | 1415.31 |
| 8 | 12.48 | 11490.23 | 1436.28 |
| 16 | 7.46 | 19223.32 | 1201.46 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 26.64 | 5380.47 | 1345.12 |
| 4 | 14.38 | 9966.40 | 1245.80 |
| 8 | 9.86 | 14544.61 | 909.04 |
| 16 | 7.02 | 20424.23 | 638.26 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.44 | 9285.08 | 1160.64 |
| 4 | 9.83 | 14586.14 | 911.63 |
| 8 | 6.71 | 21353.75 | 667.30 |
| 16 | 6.86 | 20897.14 | 326.52 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.84 | 14572.41 | 910.78 |
| 4 | 7.31 | 19615.55 | 612.99 |
| 8 | 5.53 | 25904.68 | 404.76 |
| 16 | 4.70 | 30498.38 | 238.27 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 36.04 | 4342.69 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 18.78 | 8289.83 | 4144.91 |
| 4 | 9.46 | 16458.44 | 4114.61 |
| 8 | 4.72 | 33007.71 | 4125.96 |
| 16 | 3.01 | 51687.93 | 3230.50 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.74 | 15979.00 | 3994.75 |
| 4 | 5.13 | 30327.29 | 3790.91 |
| 8 | 2.95 | 52855.88 | 3303.49 |
| 16 | 1.91 | 81349.13 | 2542.16 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 5.02 | 30984.21 | 3873.03 |
| 4 | 2.89 | 53941.30 | 3371.33 |
| 8 | 1.76 | 88530.68 | 2766.58 |
| 16 | 1.08 | 144292.27 | 2254.57 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.75 | 56681.69 | 3542.61 |
| 4 | 1.58 | 98594.31 | 3081.07 |
| 8 | 0.88 | 176867.00 | 2763.55 |
| 16 | 0.81 | 193001.18 | 1507.82 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 179.27 | 2214.5 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 94.48 | 4195.35 | 2097.68 |
| 4 | 49.74 | 7969.81 | 1992.45 |
| 8 | 26.39 | 15018.62 | 1877.33 |
| 16 | 15.04 | 26362.99 | 1647.69 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 56.67 | 6994.86 | 1748.72 |
| 4 | 33.03 | 12000.16 | 1500.02 |
| 8 | 22.32 | 17760.72 | 1110.04 |
| 16 | 16.28 | 24345.16 | 760.79 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 31.41 | 12620.47 | 1577.56 |
| 4 | 19.04 | 20823.99 | 1301.50 |
| 8 | 14.11 | 28098.44 | 878.08 |
| 16 | 11.19 | 35416.02 | 553.38 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.46 | 24080.94 | 1505.06 |
| 4 | 11.00 | 36030.80 | 1125.96 |
| 8 | 8.24 | 48086.06 | 751.34 |
| 16 | 8.67 | 45702.41 | 357.05 |


***

:arrow_backward: [README.md](../../../README.md)

***
