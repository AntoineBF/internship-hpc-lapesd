# **NPB Results - Grid'5000 - 4th series**

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.59 | 155.24 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.63 | 290.04 | 145.02 |
| 4 | 2.83 | 474.40 | 118.60 |
| 8 | 1.54 | 873.86 | 109.23 |
| 16 | 0.83 | 1625.69 | 101.61 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.63 | 369.97 | 92.49 |
| 4 | 2.48 | 541.64 | 67.70 |
| 8 | 1.81 | 741.97 | 46.37 |
| 16 | 1.76 | 764.32 | 23.89 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.08 | 646.69 | 80.84 |
| 4 | 1.49 | 901.53 | 56.35 |
| 8 | 1.32 | 1018.15 | 31.82 |
| 16 | 1.20 | 1116.03 | 17.44 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.12 | 1196.60 | 74.79 |
| 4 | 0.85 | 1579.54 | 49.36 |
| 8 | 0.83 | 1625.19 | 25.39 |
| 16 | 5.39 | 249.18 | 1.95 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 195.20 | 44.01 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 98.61 | 87.11 | 43.55 |
| 4 | 51.86 | 165.63 | 41.41 |
| 8 | 26.63 | 322.56 | 40.32 |
| 16 | 14.53 | 591.25 | 36.95 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 48.92 | 175.60 | 43.90 |
| 4 | 25.70 | 334.29 | 41.79 |
| 8 | 13.34 | 643.93 | 40.25 |
| 16 | 7.34 | 1169.82 | 36.56 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.47 | 351.10 | 43.89 |
| 4 | 12.89 | 666.24 | 41.64 |
| 8 | 6.69 | 1283.40 | 40.11 |
| 16 | 3.67 | 2341.57 | 36.59 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.24 | 701.93 | 43.87 |
| 4 | 6.48 | 1324.60 | 41.39 |
| 8 | 3.35 | 2564.49 | 40.07 |
| 16 | 1.84 | 4664.22 | 36.44 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 217.05 | 660.43 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 50.20 | 2855.54 | 1427.77 |
| 4 | 25.97 | 5519.58 | 1379.89 |
| 8 | 13.11 | 10934.02 | 1366.75 |
| 16 | 8.05 | 17801.79 | 1112.61 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 28.12 | 5097.04 | 1274.26 |
| 8 | 14.64 | 9794.09 | 1224.26 |
| 8 | 9.94 | 14424.11 | 901.51 |
| 16 | 6.90 | 20777.10 | 649.28 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.51 | 9243.03 | 1155.38 |
| 4 | 9.83 | 14585.78 | 911.61 |
| 8 | 7.00 | 20473.65 | 639.80 |
| 16 | 7.11 | 19691.11 | 307.67 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.81 | 14617.42 | 913.59 |
| 4 | 7.13 | 20090.95 | 627.84 |
| 8 | 5.55 | 25847.05 | 403.86 |
| 16 | 4.71 | 30446.22 | 237.86 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 36.87 | 4222.92 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 19.36 | 8042.57 | 4021.28 |
| 4 | 9.60 | 16220.93 | 4055.23 |
| 8 | 4.79 | 32499.51 | 4062.44 |
| 16 | 3.11 | 49983.49 | 3123.97 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.88 | 15751.21 | 3937.80 |
| 4 | 5.17 | 30137.03 | 3767.13 |
| 8 | 2.96 | 52609.82 | 3288.11 |
| 16 | 1.90 | 81868.11 | 2558.38 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.97 | 31307.34 | 3913.42 |
| 4 | 2.92 | 53350.09 | 3334.38 |
| 8 | 1.76 | 88571.32 | 2767.85 |
| 16 | 1.06 | 146485.23 | 2288.83 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.76 | 56467.13 | 3529.20 |
| 4 | 1.59 | 97632.95 | 3051.03 |
| 8 | 0.88 | 176086.09 | 2751.35 |
| 16 | 0.80 | 194509.58 | 1519.61 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 171.05 | 2317.41 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 95.66 | 4143.54 | 2071.77 |
| 4 | 50.20 | 7896.23 | 1974.06 |
| 8 | 26.50 | 14957.49 | 1869.69 |
| 16 | 15.17 | 26128.77 | 1633.05 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 56.55 | 7009.26 | 1752.32 |
| 4 | 32.80 | 12083.58 | 1510.45 |
| 8 | 21.52 | 18422.98 | 1151.44 |
| 16 | 16.37 | 24208.39 | 756.51 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 30.29 | 13085.53 | 1635.69 |
| 4 | 19.37 | 20460.09 | 1278.76 |
| 8 | 14.08 | 28147.86 | 879.62 |
| 16 | 11.37 | 34834.16 | 544.28 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.06 | 24682.92 | 1542.68 |
| 4 | 11.30 | 35077.43 | 1096.17 |
| 8 | 9.44 | 41990.08 | 656.10 |
| 16 | 9.21 | 43026.46 | 336.14 |


***

:arrow_backward: [README.md](../../../README.md)

***
