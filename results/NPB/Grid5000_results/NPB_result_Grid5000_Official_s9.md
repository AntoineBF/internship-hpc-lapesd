# **NPB Results - Grid'5000 - template series**

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.70 | 154.23 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.58 | 293.10 | 146.55 |
| 4 | 2.84 | 472.35 | 118.09 |
| 8 | 1.53 | 874.86 | 109.36 |
| 16 | 0.82 | 1627.41 | 101.71 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.69 | 363.57 | 90.89 |
| 4 | 2.51 | 534.63 | 66.83 |
| 8 | 1.86 | 722.84 | 45.18 |
| 16 | 1.52 | 880.87 | 27.53 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.19 | 612.86 | 76.61 |
| 4 | 1.53 | 878.25 | 54.89 |
| 8 | 1.22 | 1097.95 | 34.31 |
| 16 | 1.11 | 1205.27 | 18.83 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.11 | 1207.90 | 75.49 |
| 4 | 0.86 | 1559.33 | 48.73 |
| 8 | 0.85 | 1584.14 | 24.75 |
| 16 | 6.40 | 209.82 | 1.64 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 195.20 | 44.01 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 99.11 | 86.67 | 43.34 |
| 4 | 51.35 | 167.28 | 41.82 |
| 8 | 26.53 | 323.78 | 40.47 |
| 16 | 14.54 | 590.78 | 36.92 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 48.88 | 175.73 | 43.93 |
| 4 | 25.95 | 330.97 | 41.37 |
| 8 | 13.31 | 645.48 | 40.34 |
| 16 | 7.26 | 1183.70 | 36.99 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.62 | 348.94 | 43.62 |
| 4 | 12.86 | 667.92 | 41.74 |
| 8 | 6.70 | 1282.99 | 40.09 |
| 16 | 3.67 | 2338.16 | 36.53 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.25 | 700.97 | 43.81 |
| 4 | 6.50 | 1320.95 | 41.28 |
| 8 | 3.36 | 2553.58 | 39.90 |
| 16 | 1.86 | 4630.68 | 36.18 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 217.91 | 657.84 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 59.47 | 2410.52 | 1205.26 |
| 4 | 31.23 | 4589.59 | 1147.40 |
| 8 | 12.99 | 11032.00 | 1379.00 |
| 16 | 7.80 | 18382.34 | 1148.90 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 36.56 | 3920.79 | 980.20 |
| 4 | 14.81 | 9682.33 | 1210.29 |
| 8 | 9.81 | 14605.52 | 912.84 |
| 16 | 6.94 | 20643.12 | 645.10 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.58 | 9202.17 | 1150.27 |
| 4 | 9.93 | 14429.31 | 901.83 |
| 8 | 6.85 | 20930.30 | 654.07 |
| 16 | 6.92 | 20723.07 | 323.80 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.90 | 14483.87 | 905.24 |
| 4 | 7.22 | 19843.15 | 620.10 |
| 8 | 5.53 | 25899.66 | 404.68 |
| 16 | 4.57 | 31383.78 | 245.19 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 35.33 | 4407.41 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 19.20 | 8110.25 | 4055.13 |
| 4 | 9.72 | 16025.70 | 4006.42 |
| 8 | 4.77 | 32633.81 | 4079.23 |
| 16 | 3.08 | 50615.88 | 3163.49 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.94 | 15659.10 | 3914.77 |
| 4 | 5.22 | 29843.00 | 3730.37 |
| 8 | 2.93 | 53169.05 | 3323.07 |
| 16 | 1.91 | 81726.24 | 2553.95 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.93 | 31607.71 | 3950.96 |
| 4 | 3.00 | 51933.45 | 3245.84 |
| 8 | 1.77 | 88086.14 | 2752.69 |
| 16 | 1.07 | 145152.14 | 2268.00 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.78 | 56034.32 | 3502.14 |
| 4 | 1.60 | 97569.83 | 3049.06 |
| 8 | 0.89 | 175320.04 | 2739.38 |
| 16 | 0.77 | 201119.96 | 1571.25 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 169.01 | 2345.36 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 95.41 | 4154.70 | 2077.35 |
| 4 | 49.90 | 7943.12 | 1985.78 |
| 8 | 26.34 | 15050.35 | 1881.29 |
| 16 | 15.04 | 26347.37 | 1646.71 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 57.37 | 6909.63 | 1727.41 |
| 4 | 34.05 | 11640.15 | 1455.02 |
| 8 | 21.90 | 18100.66 | 1131.29 |
| 16 | 16.33 | 24266.60 | 758.33 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 31.20 | 12706.72 | 1588.34 |
| 4 | 19.10 | 20750.44 | 1296.90 |
| 8 | 13.98 | 28350.13 | 885.94 |
| 16 | 11.17 | 35483.43 | 554.43 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.34 | 24252.30 | 1515.77 |
| 4 | 11.14 | 35568.70 | 1111.52 |
| 8 | 8.28 | 47889.10 | 748.27 |
| 16 | 8.85 | 44767.03 | 349.74 |


***

:arrow_backward: [README.md](../../../README.md)

***
