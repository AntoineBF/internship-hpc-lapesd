# **NPB Results - Grid'5000 - 3rd series**

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.88 | 150.92 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.61 | 291.39 | 145.69 |
| 4 | 2.84 | 472.87 | 118.22 |
| 8 | 1.53 | 878.64 | 109.83 |
| 16 | 0.84 | 1597.36 | 99.83 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.65 | 367.99 | 92.00 |
| 4 | 2.47 | 543.20 | 67.90 |
| 8 | 1.84 | 729.37 | 45.59 |
| 16 | 1.53 | 877.37 | 27.42 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.10 | 639.27 | 79.91 |
| 4 | 1.52 | 883.15 | 55.20 |
| 8 | 1.24 | 1080.74 | 33.77 |
| 16 | 1.11 | 1209.78 | 18.90 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.12 | 1201.19 | 75.07 |
| 4 | 0.86 | 1560.63 | 48.77 |
| 8 | 0.81 | 1648.73 | 25.76 |
| 16 | 4.82 | 278.37 | 2.17 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 195.20 | 44.01 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 98.67 | 87.06 | 43.53 |
| 4 | 51.25 | 167.60 | 41.90 |
| 8 | 26.58 | 323.20 | 40.40 |
| 16 | 14.28 | 601.42 | 37.59 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.25 | 174.43 | 43.61 |
| 4 | 25.82 | 332.69 | 41.59 |
| 8 | 13.34 | 643.80 | 40.24 |
| 16 | 7.31 | 1175.16 | 36.72 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.48 | 350.84 | 43.86 |
| 4 | 12.94 | 663.99 | 41.50 |
| 8 | 6.70 | 1281.79 | 40.06 |
| 16 | 3.69 | 2326.67 | 36.35 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.31 | 697.84 | 43.61 |
| 4 | 6.61 | 1300.01 | 40.63 |
| 8 | 3.34 | 2568.00 | 40.13 |
| 16 | 1.84 | 4668.27 | 36.47 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 220.90 | 648.91 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.52 | 2894.80 | 1447.40 |
| 4 | 26.37 | 5436.31 | 1359.08 |
| 8 | 13.04 | 10989.00 | 1373.62 |
| 16 | 7.68 | 18669.20 | 1166.82 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 32.98 | 4346.49 | 1086.62 |
| 4 | 14.56 | 9847.94 | 1230.99 |
| 8 | 9.78 | 14649.96 | 915.62 |
| 16 | 6.91 | 20730.77 | 647.84 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.25 | 9401.86 | 1175.23 |
| 4 | 9.81 | 14618.54 | 913.66 |
| 8 | 6.81 | 21039.08 | 657.47 |
| 16 | 6.89 | 20814.33 | 325.22 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.74 | 14714.57 | 919.66 |
| 4 | 7.19 | 19923.22 | 622.60 |
| 8 | 5.56 | 25770.96 | 402.67 |
| 16 | 4.53 | 31673.23 | 247.45 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 36.17 | 4305.00 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 19.41 | 8021.29 | 4010.64 |
| 4 | 9.79 | 15909.86 | 3977.47 |
| 8 | 4.80 | 32425.88 | 4053.23 |
| 16 | 3.03 | 51314.70 | 3207.17 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.85 | 15803.44 | 3950.86 |
| 4 | 5.06 | 30741.87 | 3842.73 |
| 8 | 2.95 | 52859.41 | 3303.71 |
| 16 | 1.93 | 80831.48 | 2525.98 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.90 | 31796.06 | 3974.51 |
| 4 | 2.90 | 53701.17 | 3356.32 |
| 8 | 1.77 | 87740.56 | 2741.89 |
| 16 | 1.08 | 144440.68 | 2256.89 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.78 | 56010.59 | 3500.66 |
| 4 | 1.64 | 94878.15 | 2964.94 |
| 8 | 0.88 | 176216.11 | 2753.38 |
| 16 | 0.80 | 194847.71 | 1522.25 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 169.82 | 2334.16 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 95.48 | 4151.56 | 2075.78 |
| 4 | 50.03 | 7922.87 | 1980.72 |
| 8 | 26.52 | 14945.45 | 1868.18 |
| 16 | 15.01 | 26404.72 | 1650.30 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 56.42 | 7025.80 | 1756.45 |
| 4 | 33.20 | 11941.16 | 1492.64 |
| 8 | 21.72 | 18250.09 | 1140.63 |
| 16 | 16.10 | 24624.99 | 769.53 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 31.44 | 12607.86 | 1575.98 |
| 4 | 19.17 | 20677.58 | 1292.35 |
| 8 | 13.65 | 29045.12 | 907.66 |
| 16 | 11.16 | 35530.92 | 555.17 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.12 | 24583.55 | 1536.47 |
| 4 | 11.07 | 35798.65 | 1118.71 |
| 8 | 8.23 | 48179.25 | 752.80 |
| 16 | 9.17 | 43206.44 | 337.55 |


***

:arrow_backward: [README.md](../../../README.md)

***
