# **NPB Results - Grid'5000 - 7th series**

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.73 | 152.82 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.58 | 293.24 | 146.62 |
| 4 | 2.84 | 472.03 | 118.01 |
| 8 | 1.52 | 883.27 | 110.41 |
| 16 | 0.85 | 1584.28 | 99.02 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.62 | 371.24 | 92.81 |
| 4 | 2.55 | 525.82 | 65.73 |
| 8 | 1.85 | 726.25 | 45.39 |
| 16 | 1.53 | 874.75 | 27.34 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.06 | 650.25 | 81.28 |
| 4 | 1.51 | 890.37 | 55.65 |
| 8 | 1.23 | 1087.03 | 33.97 |
| 16 | 1.11 | 1205.06 | 18.83 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.10 | 1216.29 | 76.02 |
| 4 | 0.85 | 1584.29 | 49.51 |
| 8 | 0.82 | 1627.72 | 25.43 |
| 16 | 4.52 | 297.45 | 2.32 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 196.12 | 43.90 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 98.64 | 87.08 | 43.54 |
| 4 | 51.62 | 166.41 | 41.60 |
| 8 | 26.58 | 323.17 | 40.40 |
| 16 | 14.52 | 591.51 | 36.97 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 48.88 | 175.74 | 43.93 |
| 4 | 25.74 | 333.73 | 41.72 |
| 8 | 13.43 | 639.49 | 39.97 |
| 16 | 7.29 | 1178.67 | 36.83 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.57 | 349.67 | 43.71 |
| 4 | 13.01 | 660.24 | 41.26 |
| 8 | 6.67 | 1288.05 | 40.25 |
| 16 | 3.69 | 2330.75 | 36.42 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.26 | 700.81 | 43.80 |
| 4 | 6.50 | 1321.99 | 41.31 |
| 8 | 3.35 | 2563.11 | 40.05 |
| 16 | 1.84 | 4656.20 | 36.38 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 219.87 | 651.97 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.90 | 2872.50 | 1436.25 |
| 4 | 26.52 | 5406.04 | 1351.51 |
| 8 | 12.86 | 11149.04 | 1393.63 |
| 16 | 7.84 | 18290.19 | 1143.14 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 31.82 | 4504.74 | 1126.18 |
| 4 | 14.80 | 9683.36 | 1210.42 |
| 8 | 9.75 | 14704.59 | 919.04 |
| 16 | 6.91 | 20759.05 | 648.72 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.05 | 8933.86 | 1116.73 |
| 4 | 9.87 | 14527.05 | 907.94 |
| 8 | 6.84 | 20951.66 | 654.74 |
| 16 | 6.86 | 20904.02 | 326.63 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.84 | 14565.37 | 910.34 |
| 4 | 7.18 | 19961.43 | 623.79 |
| 8 | 5.53 | 25912.27 | 404.88 |
| 16 | 4.57 | 31345.47 | 244.89 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 35.76 | 4358.86 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 19.10 | 8152.27 | 4076.13 |
| 4 | 9.78 | 15923.44 | 3980.86 |
| 8 | 4.76 | 32710.60 | 4088.82 |
| 16 | 3.09 | 50312.90 | 3144.56 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.96 | 15634.38 | 3908.60 |
| 4 | 5.23 | 29784.34 | 3723.04 |
| 8 | 2.95 | 52755.57 | 3297.22 |
| 16 | 1.89 | 82408.06 | 2575.25 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.97 | 31345.88 | 3918.23 |
| 4 | 2.93 | 53222.56 | 3326.41 |
| 8 | 1.79 | 86906.49 | 2715.83 |
| 16 | 1.07 | 145080.10 | 2266.88 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.79 | 55878.90 | 3492.43 |
| 4 | 1.58 | 98262.24 | 3070.69 |
| 8 | 0.88 | 176342.26 | 2755.35 |
| 16 | 0.79 | 195935.60 | 1530.75 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 170.74 | 2321.60 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 95.39 | 4155.40 | 2077.70 |
| 4 | 49.88 | 7946.98 | 1986.74 |
| 8 | 26.13 | 15169.71 | 1896.21 |
| 16 | 14.98 | 26460.74 | 1653.80 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 56.61 | 7002.14 | 1750.53 |
| 4 | 33.40 | 11868.89 | 1483.61 |
| 8 | 22.04 | 17984.26 | 1124.02 |
| 16 | 16.57 | 23922.73 | 747.59 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 30.82 | 12861.01 | 1607.63 |
| 4 | 19.10 | 20758.72 | 1297.42 |
| 8 | 13.74 | 28858.43 | 901.83 |
| 16 | 11.29 | 35123.10 | 548.80 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.23 | 24428.76 | 1526.80 |
| 4 | 11.64 | 34061.03 | 1064.41 |
| 8 | 9.03 | 43900.89 | 685.95 |
| 16 | 8.95 | 44302.76 | 346.12 |


***

:arrow_backward: [README.md](../../../README.md)

***
