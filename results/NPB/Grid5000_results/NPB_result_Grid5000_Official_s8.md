# **NPB Results - Grid'5000 - 8th series** 

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.77 | 153.06 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.60 | 291.91 | 145.96 |
| 4 | 2.85 | 471.15 | 117.79 |
| 8 | 1.53 | 880.02 | 110.00 |
| 16 | 0.86 | 1568.94 | 98.06 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.63 | 369.33 | 92.33 |
| 4 | 2.52 | 532.27 | 66.53 |
| 8 | 1.87 | 716.32 | 44.77 |
| 16 | 1.52 | 880.47 | 27.51 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.07 | 648.92 | 81.12 |
| 4 | 1.51 | 887.22 | 55.45 |
| 8 | 1.23 | 1088.92 | 34.03 |
| 16 | 1.11 | 1208.17 | 18.88 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.10 | 1225.41 | 76.59 |
| 4 | 0.85 | 1583.63 | 49.49 |
| 8 | 0.81 | 1655.63 | 25.87 |
| 16 | 4.59 | 292.19 | 2.28 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 195.18 | 44.01 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 98.37 | 87.33 | 43.66 |
| 4 | 51.82 | 165.77 | 41.44 |
| 8 | 26.56 | 323.40 | 40.43 |
| 16 | 14.34 | 598.85 | 37.43 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.20 | 174.59 | 43.65 |
| 4 | 26.07 | 329.44 | 41.18 |
| 8 | 13.32 | 645.07 | 40.32 |
| 16 | 7.29 | 1178.53 | 36.83 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.50 | 350.54 | 43.82 |
| 4 | 12.94 | 663.70 | 41.48 |
| 8 | 6.68 | 1286.34 | 40.20 |
| 16 | 3.68 | 2334.83 | 36.48 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.26 | 700.78 | 43.80 |
| 4 | 6.57 | 1307.52 | 40.86 |
| 8 | 3.36 | 2554.75 | 39.92 |
| 16 | 1.85 | 4651.42 | 36.34 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 216.49 | 662.15 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.80 | 2878.54 | 1439.27 |
| 4 | 26.26 | 5457.98 | 1364.49 |
| 8 | 12.99 | 11037.19 | 1379.65 |
| 16 | 7.84 | 18286.69 | 1142.92 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 28.61 | 5010.92 | 1252.73 |
| 4 | 14.63 | 9797.96 | 1224.74 |
| 8 | 9.72 | 14744.91 | 921.56 |
| 16 | 6.83 | 20976.70 | 655.52 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.61 | 9183.80 | 1147.98 |
| 4 | 9.99 | 14349.64 | 896.85 |
| 8 | 6.89 | 20795.19 | 649.85 |
| 16 | 6.84 | 20960.90 | 327.51 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.60 | 14930.34 | 933.15 |
| 4 | 7.16 | 20030.71 | 625.96 |
| 8 | 5.56 | 25804.83 | 403.20 |
| 16 | 4.55 | 31527.54 | 246.31 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 35.91 | 4335.05 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 19.40 | 8024.33 | 4012.16 |
| 4 | 9.84 | 15830.21 | 3957.55 |
| 8 | 4.82 | 32278.33 | 4034.79 |
| 16 | 3.09 | 50392.10 | 3149.51 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 10.02 | 15542.31 | 3885.58 |
| 4 | 5.17 | 30099.90 | 3762.49 |
| 8 | 2.95 | 52832.08 | 3302.01 |
| 16 | 1.90 | 82001.21 | 2562.54 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 5.01 | 31107.26 | 3888.41 |
| 4 | 2.99 | 52135.32 | 3258.46 |
| 8 | 1.77 | 87716.62 | 2741.14 |
| 16 | 1.07 | 145405.88 | 2271.97 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.76 | 56385.02 | 3524.06 |
| 4 | 1.57 | 99139.26 | 3098.10 |
| 8 | 0.88 | 176561.40 | 2758.77 |
| 16 | 0.78 | 199333.03 | 1557.29 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 170.99 | 2318.18 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 95.91 | 4132.82 | 2066.41 |
| 4 | 50.28 | 7883.48 | 1970.87 |
| 8 | 26.54 | 14933.73 | 1866.72 |
| 16 | 15.08 | 26286.51 | 1642.91 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 56.98 | 6957.23 | 1739.31 |
| 4 | 33.20 | 11938.74 | 1492.34 |
| 8 | 22.00 | 18014.21 | 1125.89 |
| 16 | 16.61 | 23863.70 | 745.74 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 30.88 | 12837.41 | 1604.68 |
| 4 | 18.97 | 20893.74 | 1305.86 |
| 8 | 14.67 | 27028.40 | 844.64 |
| 16 | 11.16 | 35507.18 | 554.80 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.85 | 25013.46 | 1563.34 |
| 4 | 10.84 | 36557.79 | 1142.43 |
| 8 | 8.22 | 48224.61 | 753.51 |
| 16 | 11.45 | 34609.68 | 270.39 |


***

:arrow_backward: [README.md](../../../README.md)

***
