# **NPB Results - Grid'5000 - 6th series**

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.79 | 152.71 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.56 | 294.50 | 147.25 |
| 4 | 2.82 | 476.37 | 119.09 |
| 8 | 1.51 | 886.29 | 110.79 |
| 16 | 0.85 | 1582.63 | 98.91 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.74 | 358.54 | 89.64 |
| 4 | 2.49 | 539.44 | 67.43 |
| 8 | 1.85 | 725.96 | 45.37 |
| 16 | 1.53 | 874.88 | 27.34 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.06 | 652.35 | 81.54 |
| 4 | 1.53 | 879.17 | 54.95 |
| 8 | 1.23 | 1094.90 | 34.22 |
| 16 | 1.11 | 1206.52 | 18.85 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.08 | 1237.56 | 77.35 |
| 4 | 0.85 | 1574.25 | 49.20 |
| 8 | 0.81 | 1657.74 | 25.90 |
| 16 | 4.56 | 294.45 | 2.30 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 196.38 | 43.74 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 99.06 |  86.71 | 43.36 |
| 4 | 51.52 | 166.73 | 41.68 |
| 8 | 26.60 | 322.96 | 40.37 |
| 16 | 14.60 | 588.18 | 36.76 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 48.93 | 175.55 | 43.89 |
| 4 | 26.01 | 330.31 | 41.29 |
| 8 | 13.40 | 640.95 | 40.06 |
| 16 | 7.27 | 1181.83 | 36.93 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.52 | 350.26 | 43.78 |
| 4 | 13.05 | 658.24 | 41.14 |
| 8 | 6.70 | 1282.74 | 40.09 |
| 16 | 3.70 | 2320.99 | 36.27 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.31 | 697.95 | 43.62 |
| 4 | 6.54 | 1312.61 | 41.02 |
| 8 | 3.35 | 2564.33 | 40.07 |
| 16 | 1.85 | 4650.70 | 36.33 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 219.80 | 652.17 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 51.01 | 2810.26 | 1405.13 |
| 4 | 34.95 | 4101.91 | 1025.48 |
| 8 | 12.88 | 11130.79 | 1391.35 |
| 16 | 7.77 | 18441.76 | 1152.61 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 30.32 | 4727.33 | 1181.83 |
| 4 | 14.51 | 9882.24 | 1235.28 |
| 8 | 9.90 | 14477.10 | 904.82 |
| 16 | 6.85 | 20929.31 | 654.04 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.54 | 9222.99 | 1152.87 |
| 4 | 9.82 | 14602.38 | 912.65 |
| 8 | 6.76 | 21208.98 | 662.78 |
| 16 | 6.98 | 20539.14 | 320.92 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.67 | 14829.56 | 926.85 |
| 4 | 7.13 | 20099.83 | 628.12 |
| 8 | 5.55 | 25817.24 | 403.39 |
| 16 | 4.55 | 31527.00 | 246.30 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 35.52 | 4383.84 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 18.97 | 8209.06 | 4104.53 |
| 4 | 9.66 | 16116.31 | 4029.08 |
| 8 | 4.78 | 32590.19 | 4073.77 |
| 16 | 3.09 | 50342.29 | 3146.39 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.90 | 15731.13 | 3932.78 |
| 4 | 5.13 | 30368.48 | 3796.06 |
| 8 | 2.96 | 52615.27 | 3288.45 |
| 16 | 1.91 | 81627.18 | 2550.85 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.89 | 31847.78 | 3980.97 |
| 4 | 2.96 | 52544.71 | 3284.04 |
| 8 | 1.77 | 87856.50 | 2745.52 |
| 16 | 1.08 | 144341.36 | 2255.33 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.74 | 56917.70 | 3557.36 |
| 4 | 1.59 | 98213.35 | 3069.17 |
| 8 | 0.87 | 177972.22 | 2780.82 |
| 16 | 0.81 | 192114.25 | 1500.89 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 169.72 | 2335.52 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 95.61 | 4145.68 | 2072.84 |
| 4 | 49.93 | 7938.91 | 1984.73 |
| 8 | 26.40 | 15013.80 | 1876.73 |
| 16 | 14.99 | 26450.79 | 1653.17 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 56.70 | 6991.36 | 1747.84 |
| 4 | 32.99 | 12016.46 | 1502.06 |
| 8 | 21.83 | 18154.86 | 1134.68 |
| 16 | 16.08 | 24647.93 | 770.25 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 31.02 | 12777.62 | 1597.20 |
| 4 | 19.21 | 20629.14 | 1289.32 |
| 8 | 13.43 | 29517.55 | 922.42 |
| 16 | 11.17 | 35491.90 | 554.56 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.28 | 24348.06 | 1521.75 |
| 4 | 11.56 | 34292.53 | 1071.64 |
| 8 | 9.00 | 44037.72 | 688.09 |
| 16 | 8.59 | 46131.63 | 360.40 |


***

:arrow_backward: [README.md](../../../README.md)

***
