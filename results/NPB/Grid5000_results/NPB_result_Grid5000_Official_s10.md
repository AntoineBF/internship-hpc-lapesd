# **NPB Results - Grid'5000 - 10th series**

(CPUtype: `Intel Xeon Gold 6130`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.0`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 8.86 | 151.53 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.57 | 293.66 | 146.83 |
| 4 | 2.84 | 472.27 | 118.07 |
| 8 | 1.54 | 873.24 | 109.15 |
| 16 | 0.85 | 1580.36 | 98.77 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 3.62 | 371.15 | 92.79 |
| 4 | 2.48 | 541.45 | 67.68 |
| 8 | 1.85 | 725.90 | 45.37 |
| 16 | 1.53 | 879.20 | 27.48 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.07 | 649.92 | 81.24 |
| 4 | 1.51 | 886.54 | 55.41 |
| 8 | 1.25 | 1077.90 | 33.68 |
| 16 | 1.11 | 1204.17 | 18.82 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 1.12 | 1201.14 | 75.07 |
| 4 | 0.87 | 1560.04 | 48.75 |
| 8 | 0.82 | 1649.68 | 25.78 |
| 16 | 4.58 | 295.15 | 2.31 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 195.14 | 44.02 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 98.23 | 87.45 | 43.72 |
| 4 | 51.54 | 166.67 | 41.67 |
| 8 | 26.71 | 321.61 | 40.20 |
| 16 | 14.48 | 593.09 | 37.07 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.00 | 175.31 | 43.83 |
| 4 | 25.82 | 332.64 | 41.58 |
| 8 | 13.31 | 645.16 | 40.32 |
| 16 | 7.25 | 1184.43 | 37.01 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 24.60 | 349.23 | 43.65 |
| 4 | 12.93 | 664.55 | 41.53 |
| 8 | 6.69 | 1284.78 | 40.15 |
| 16 | 3.68 | 2333.12 | 36.46 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 12.31 | 698.02 | 43.63 |
| 4 | 6.51 | 1325.64 | 41.43 |
| 8 | 3.36 | 2564.11 | 40.06 |
| 16 | 1.85 | 4651.10 | 36.34 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 220.86 | 649.03 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.10 | 2919.67 | 1459.83 |
| 4 | 25.89 | 5536.22 | 1384.05 |
| 8 | 12.83 | 11172.68 | 1396.58 |
| 16 | 7.59 | 18873.95 | 1179.62 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 28.64 | 5004.53 | 1251.13 |
| 4 | 14.60 | 9815.00 | 1226.88 |
| 8 | 9.97 | 14385.05 | 899.07 |
| 16 | 7.02 | 20405.63 | 637.68 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.75 | 9101.21 | 1137.65 |
| 4 | 9.88 | 14511.96 | 907.00 |
| 8 | 6.88 | 20849.97 | 651.56 |
| 16 | 6.96 | 20608.96 | 322.01 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.77 | 14702.18 | 918.89 |
| 4 | 7.21 | 19843.28 | 620.10 |
| 8 | 5.54 | 25904.48 | 404.76 |
| 16 | 4.53 | 31527.61 | 246.31 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 36.70 | 4242.06 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 19.10 | 8150.77 | 4075.39 |
| 4 | 9.69 | 16071.01 | 4017.75 |
| 8 | 4.76 | 32722.04 | 4090.25 |
| 16 | 3.03 | 51308.09 | 3206.76 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 9.83 | 15832.98 | 3958.25 |
| 4 | 5.14 | 30297.64 | 3787.20 |
| 8 | 2.92 | 53310.20 | 3331.89 |
| 16 | 1.92 | 80926.17 | 2528.94 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.98 | 31237.89 | 3904.74 |
| 4 | 2.97 | 52410.70 | 3275.67 |
| 8 | 1.77 | 87847.48 | 2745.23 |
| 16 | 1.10 | 141440.21 | 2210.00 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.75 | 56681.72 | 3542.61 |
| 4 | 1.58 | 98262.18 | 3070.69 |
| 8 | 0.88 | 176560.84 | 2758.76 |
| 16 | 0.77 | 201119.96 | 1571.25 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 171.45 | 2311.94 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 95.50 | 4150.85 | 2075.42 |
| 4 | 50.02 | 7925.31 | 1981.33 |
| 8 | 26.41 | 15009.81 | 1876.23 |
| 16 | 14.98 | 26465.27 | 1654.08 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 57.06 | 6946.70 | 1736.67 |
| 4 | 33.14 | 11960.34 | 1495.04 |
| 8 | 22.05 | 17980.00 | 1123.75 |
| 16 | 16.19 | 24490.72 | 765.33 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 30.89 | 12832.77 | 1604.10 |
| 4 | 19.20 | 20649.31 | 1290.58 |
| 8 | 14.07 | 28168.32 | 880.26 |
| 16 | 11.18 | 35454.92 | 553.98 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.11 | 24588.34 | 1536.78 |
| 4 | 11.01 | 3602.46 | 112.58 |
| 8 | 8.72 | 45876.64 | 716.82 |
| 16 | 9.09 | 435575.48 | 3402.93 |


***

:arrow_backward: [README.md](../../../README.md)

***
