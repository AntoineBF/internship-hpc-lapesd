# **NPB Results - Strong Scaling**

    The number of processors is increased while the problem size remains constant. 
    
    This also results in a reduced workload per processor.

[spreadsheets](../NBP-OSU_results_excel.xlsx) containing all the information.

| NPB applications | States |
|:---|:---|
| [ep](#ep-operation-type-random-numbers-generated) | :heavy_check_mark: |
| [ft](#ft-operation-type-floating-point) | :heavy_check_mark: |
| [is](#is-operation-type-keys-ranked) | :heavy_check_mark: |
| [cg](#cg-operation-type-floating-point) | :heavy_check_mark: |
| [mg](#mg-operation-type-floating-point) | :heavy_check_mark: |

Legend:

+ :heavy_check_mark: : finish

+ :heavy_minus_sign: : unfinish


## EP (Operation type: Random numbers generated):

```
number of processors required: all
```

![EP_Scaling](../../img/scaling/EP_scaling.png)


## FT (Operation type: floating point):

```
number of processors required: power-of-two number
```

![FT_Scaling](../../img/scaling/FT_scaling.png)


## IS (Operation type: keys ranked):

```
number of processors required: power-of-two number
```

![IS_Scaling](../../img/scaling/IS_scaling.png)


## CG (Operation type: floating point):

```
number of processors required: power-of-two number
```

![CG_Scaling](../../img/scaling/CG_scaling.png)


## MG (Operation type: floating point):

```
number of processors required: power-of-two number
```

![MG_Scaling](../../img/scaling/MG_scaling.png)


***

:arrow_backward: [README.md](../../README.md)

***
