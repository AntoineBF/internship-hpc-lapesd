# **NPB Results - AWS - 5th series** 

(CPUtype: `Intel Xeon Platinum 8124M`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.1`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 9.31 | 144.21 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.83 | 277.99 | 138.99 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.92 | 459.08 | 114.77 |
| 4 | 1.65 | 811.40 | 101.43 |
| 8 | 1.14 | 1177.47 | 73.59 |
| 16 | 2.68 | 501.74 | 15.68 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 0.65 | 2059.36 | 32.18 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 5.14 | 261.12 | 2.04 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 180.83 | 47.50 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 90.69 | 94.72 | 47.36 |
| 4 | 45.29 | 189.67 | 47.42 |
| 8 | 22.68 | 378.69 | 47.34 |
| 16 | 13.88 | 618.66 | 38.67 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 7.05 | 1219.22 | 38.10 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 3.59 | 2392.83 | 37.39 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.78 | 4837.64 | 37.79 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 224.67 | 638.04 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 54.20 | 2644.58 | 1322.29 |
| 4 | 28.20 | 5083.62 | 1270.90 |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 9.12 | 15720.73 | 982.55 |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 16.09 | 8910.03 | 1113.75 |
| 4 |  |  |  |
| 8 | 7.72 | 18579.82 | 580.62 |
| 16 |  |  |  |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 5.18 | 27680.57 | 432.51 |
| 16 | 3.91 | 36675.98 | 286.53 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 39.05 | 3986.76 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 20.61 | 7554.86 | 3777.43 |
| 4 | 11.57 | 13453.63 | 3363.41 |
| 8 | 8.87 | 17560.57 | 2195.07 |
| 16 | 6.61 | 23552.58 | 1472.04 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 11.33 | 13739.42 | 3434.86 |
| 4 | 5.30 | 29376.29 | 3672.04 |
| 8 |  |  |  |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 2.97 | 52337.07 | 1635.53 |
| 16 | 1.69 | 92123.36 | 1439.43 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.00 | 156329.59 | 1221.32 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 166.84 | 2375.83 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 93.32 | 4247.50 | 2123.75 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 48.79 | 8123.88 | 2030.97 |
| 4 | 25.98 | 15259.24 | 1907.40 |
| 8 | 15.23 | 26026.41 | 1626.65 |
| 16 | 13.66 | 29014.00 | 906.69 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 25.18 | 15742.11 | 1967.76 |
| 4 | 14.02 | 28280.96 | 1767.56 |
| 8 | 13.49 | 29377.58 | 918.05 |
| 16 | 7.96 | 49786.56 | 777.92 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 8.12 | 48831.23 | 381.49 |


***

:arrow_backward: [README.md](../../../README.md)

***
