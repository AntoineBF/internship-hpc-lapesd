# **NPB Results - AWS - 7th series** 

(CPUtype: `Intel Xeon Platinum 8124M`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.1`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 9.18 | 146.14 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.76 | 282.24 | 141.12 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.93 | 457.38 | 114.34 |
| 4 | 1.67 | 804.38 | 100.55 |
| 8 | 1.15 | 1170.97 | 73.19 |
| 16 | 2.29 | 587.17 | 18.35 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 0.65 | 2065.67 | 32.28 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 5.22 | 257.06 | 2.01 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 181.60 | 47.30 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 90.68 | 94.73 | 47.36 |
| 4 | 45.24 | 189.87 | 47.47 |
| 8 | 22.67 | 378.85 | 47.36 |
| 16 | 13.98 | 614.23 | 38.39 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 7.07 | 1214.60 | 37.96 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 3.60 | 2387.56 | 37.31 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.77 | 4857.60 | 37.95 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 217.82 | 658.10 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 52.54 | 2728.59 | 1364.30 |
| 4 | 27.45 | 5222.52 | 1305.63 |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 8.99 | 15941.78 | 996.36 |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.96 | 8984.36 | 1123.04 |
| 4 |  |  |  |
| 8 | 7.81 | 18354.78 | 573.59 |
| 16 |  |  |  |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 5.19 | 27645.95 | 431.97 |
| 16 | 3.88 | 36974.24 | 288.86 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 38.05 | 4091.87 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 20.42 | 7623.74 | 3811.87 |
| 4 | 11.47 | 13572.12 | 3393.03 |
| 8 | 8.92 | 17460.78 | 2182.60 |
| 16 | 6.49 | 23978.40 | 1498.65 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 11.54 | 13486.94 | 3371.74 |
| 4 | 5.35 | 29084.09 | 3635.51 |
| 8 |  |  |  |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 2.82 | 55203.90 | 1725.12 |
| 16 | 1.69 | 92068.47 | 1438.57 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.00 | 156249.71 | 1220.70 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 171.41 | 2312.48 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 92.60 | 4280.76 | 2140.38 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.30 | 8039.62 | 2009.91 |
| 4 | 25.89 | 15309.84 | 1913.73 |
| 8 | 15.16 | 26145.35 | 1634.08 |
| 16 | 13.74 | 28851.17 | 901.60 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 25.21 | 15720.52 | 1965.07 |
| 4 | 13.97 | 28370.05 | 1773.13 |
| 8 | 13.02 | 30453.84 | 951.68 |
| 16 | 7.97 | 49757.68 | 777.46 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 8.35 | 47482.14 | 370.95 |


***

:arrow_backward: [README.md](../../../README.md)

***
