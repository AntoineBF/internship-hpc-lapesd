# **NPB Results - AWS - template series** 

(CPUtype: `Intel Xeon Platinum 8124M`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.1`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 9.39 | 142.95 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.86 | 276.03 | 138.02 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.95 | 454.28 | 113.57 |
| 4 | 1.69 | 796.36 | 99.54 |
| 8 | 1.47 | 913.54 | 57.10 |
| 16 | 2.14 | 595.26 | 18.60 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 0.66 | 2046.22 | 31.97 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 5.16 | 259.90 | 2.03 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 180.99 | 47.46 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 90.80 | 94.60 | 47.30 |
| 4 | 45.32 | 189.56 | 47.39 |
| 8 | 22.73 | 377.98 | 47.25 |
| 16 | 13.64 | 629.87 | 39.37 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 7.03 | 1221.98 | 38.19 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 3.57 | 2408.62 | 37.63 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.76 | 4875.19 | 38.09 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 227.47 | 630.19 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 54.55 | 2627.60 | 1313.80 |
| 4 | 28.40 | 5047.96 | 1261.99 |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 9.14 | 15678.47 | 979.90 |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.70 | 9077.84 | 1134.73 |
| 4 |  |  |  |
| 8 | 7.67 | 18700.70 | 584.40 |
| 16 |  |  |  |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 5.21 | 27532.70 | 430.20 |
| 16 | 3.89 | 36874.16 | 288.08 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 39.41 | 3950.79 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 20.86 | 7464.84 | 3732.42 |
| 4 | 11.40 | 13657.26 | 3414.32 |
| 8 | 8.86 | 17578.68 | 2197.34 |
| 16 | 6.50 | 23939.20 | 1496.20 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 11.24 | 13814.85 | 3453.71 |
| 4 | 5.34 | 29198.59 | 3649.82 |
| 8 |  |  |  |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 3.00 | 51857.06 | 1620.53 |
| 16 | 1.68 | 92774.25 | 1449.60 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.02 | 153043.42 | 1195.65 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 173.11 | 2289.80 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 93.43 | 4242.53 | 2121.27 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.82 | 7957.02 | 1989.26 |
| 4 | 26.23 | 15113.37 | 1889.17 |
| 8 | 15.30 | 25900.12 | 1618.76 |
| 16 | 13.74 | 28849.57 | 901.55 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 25.14 | 15764.46 | 1970.56 |
| 4 | 14.18 | 27957.14 | 1747.32 |
| 8 | 13.38 | 29634.25 | 926.07 |
| 16 | 7.97 | 49756.03 | 777.44 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 8.23 | 48155.00 | 376.21 |


***

:arrow_backward: [README.md](../../../README.md)

***
