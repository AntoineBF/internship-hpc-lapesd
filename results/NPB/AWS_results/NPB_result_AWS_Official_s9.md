# **NPB Results - AWS - 9th series** 

(CPUtype: `Intel Xeon Platinum 8124M`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.1`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 9.34 | 143.65 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.86 | 276.29 | 138.15 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.89 | 464.21 | 116.05 |
| 4 | 1.68 | 801.10 | 100.14 |
| 8 | 1.12 | 1194.67 | 74.67 |
| 16 | 2.20 | 610.44 | 19.08 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 0.65 | 2072.31 | 32.38 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 4.86 | 276.44 | 2.16 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 181.31 | 47.38 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 91.04 | 94.35 | 47.18 |
| 4 | 45.35 | 189.41 | 47.35 |
| 8 | 22.70 | 378.40 | 47.30 |
| 16 | 13.94 | 616.37 | 38.52 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 7.10 | 1210.33 | 37.82 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 3.94 | 2182.73 | 34.11 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.76 | 4875.79 | 38.09 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 224.76 | 637.78 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 54.39 | 2635.77 | 1317.88 |
| 4 | 28.36 | 5055.07 | 1263.77 |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 9.10 | 15757.78 | 984.86 |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.82 | 9060.56 | 1132.57 |
| 4 |  |  |  |
| 8 | 7.66 | 18725.41 | 585.17 |
| 16 |  |  |  |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 5.17 | 27723.01 | 433.17 |
| 16 | 3.96 | 36241.15 | 283.13 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 39.31 | 3960.54 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 21.51 | 7237.96 | 3618.98 |
| 4 | 11.47 | 13578.51 | 3394.63 |
| 8 | 8.85 | 17596.52 | 2199.56 |
| 16 | 6.71 | 23191.76 | 1449.48 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 11.13 | 13991.04 | 3497.76 |
| 4 | 5.32 | 29292.75 | 3661.59 |
| 8 |  |  |  |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 2.81 | 55488.41 | 1734.01 |
| 16 | 1.71 | 91289.87 | 1426.40 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 0.99 | 156697.58 | 1224.20 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 173.53 | 2284.21 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 93.60 | 4234.79 | 2117.39 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 50.78 | 7805.48 | 1951.37 |
| 4 | 26.03 | 15229.41 | 1903.68 |
| 8 | 15.30 | 25902.87 | 1618.93 |
| 16 | 13.75 | 28831.96 | 901.00 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 25.12 | 15778.25 | 1972.28 |
| 4 | 13.93 | 28455.44 | 1778.47 |
| 8 | 13.29 | 29824.83 | 932.03 |
| 16 | 7.95 | 49875.41 | 779.30 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 8.07 | 49131.18 | 383.84 |


***

:arrow_backward: [README.md](../../../README.md)

***
