# **NPB Results - AWS - 8th series** 

(CPUtype: `Intel Xeon Platinum 8124M`, OS: `CentOS 7.8`, OpenMPI: `5.1.0a1`, GCC: `7.3.1`)

## **For IS (Operation type: keys ranked): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 9.15 | 146.68 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 4.76 | 282.06 | 141.03 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 2.93 | 457.45 | 114.36 |
| 4 | 1.71 | 783.95 | 97.99 |
| 8 | 1.14 | 1182.00 | 73.88 |
| 16 | 2.31 | 580.92 | 18.15 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 0.66 | 2040.79 | 31.89 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 5.41 | 248.03 | 1.94 |


## **For EP (Operation type: Random numbers generated): (all)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 180.97 | 47.47 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 90.60 | 94.81 | 47.41 |
| 4 | 45.52 | 188.72 | 47.18 |
| 8 | 22.68 | 378.67 | 47.33 |
| 16 | 13.98 | 614.41 | 38.40 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 7.60 | 1130.81 | 35.34 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 3.58 | 2401.54 | 37.52 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.78 | 4817.48 | 37.64 |


## **For CG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 217.63 | 658.68 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 52.44 |2733.72 | 1366.86 |
| 4 | 27.33 |5245.73 | 1311.43 |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 9.18 | 15612.58 | 975.79 |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 15.73 | 9112.28 | 1139.04 |
| 4 |  |  |  |
| 8 | 7.72 | 18565.72 | 580.18 |
| 16 |  |  |  |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 5.14 | 27888.53 | 435.76 |
| 16 | 3.90 | 36722.62 | 286.90 |


## **For MG (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 37.80 | 4118.96 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 20.04 | 7769.13 | 3884.57 |
| 4 | 11.42 | 13631.11 | 3407.78 |
| 8 | 8.87 | 17548.93 | 2193.62 |
| 16 | 6.47 | 24057.08 | 1503.57 |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 11.30 | 13773.60 | 3443.40 |
| 4 | 5.34 | 29171.30 | 3646.41 |
| 8 |  |  |  |
| 16 |  |  |  |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 | 2.79 | 55873.62 | 1746.05 |
| 16 | 1.70 | 91370.06 | 1427.66 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 1.01 | 154093.67 | 1203.86 |


## **For FT (Operation type: floating point): (power 2)**

### **Baseline**

| #MPI processes | Time in seconds | Mop/s total |
|:---|:---|:---|
| 1 | 171.41 | 2312.58 |

### **1 node**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 92.78 | 4272.51 | 2136.25 |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 |  |  |  |

### **2 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 49.98 | 7931.39 | 1982.85 |
| 4 | 26.26 | 15094.61 | 1886.83 |
| 8 | 15.25 | 25990.56 | 1624.41 |
| 16 | 13.83 | 28662.27 | 895.70 |

### **4 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 | 25.17 | 15749.29 | 1968.66 |
| 4 | 13.84 | 28636.82 | 1789.80 |
| 8 | 13.13 | 30185.62 | 943.30 |
| 16 | 7.95 | 49831.12 | 778.61 |

### **8 nodes**

| #MPI processes | Time in seconds | Mop/s total | Mop/s/process |
|:---|:---|:---|:---|
| 2 |  |  |  |
| 4 |  |  |  |
| 8 |  |  |  |
| 16 | 8.60 | 46101.63 | 360.17 |


***

:arrow_backward: [README.md](../../../README.md)

***
