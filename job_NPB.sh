#!/bin/bash

# Variables default
cluster=""
job=""
reservation=""
walltime=""
benchmark=""
class=""
core="1"
host="1"
ndir=""
subtype=""

help(){
  echo "Usage: bash $0 -c [cluster] -r [reservation] -w [walltime] -b [benchmark] -cl [class] -core [core]"
  echo "Arguments:"
  echo "    -c,   --cluster <value>       : the cluster name"
  #echo " -j, --job <value>: the job name. default: benckmark-class"
  echo "    -r,   --reservation <value>   : start time \"YYYY-MM-DD HH:mm:ss\""
  echo "    -w,   --walltime <value>      : the duration of the job"
  echo "    -b,   --benchmark <value>     : benchmark name. ex: bt;cg;dt;ep;ft;is;lu;mg;sp"
  echo "    -cl,  --class <value>         : class test. ex: S;W;A;B;C;D;E"
  echo "    -h,   --help                  : show this help."
  echo "    -core <value>                 : number of core; default=1"
  echo "		-host <value>									: number of host; default=1"
  echo "		-st, 	--subtype <value>				:	BT-IO parameter; ex: full, simple, fortran, epio, \"empty\"; default=\"empty\""
	exit 1
}

# Check the nomber of argument
if [ "$#" -eq 0 ] || [ "$1" = "-h" ] || [ "$1" = "--help" ] || [ $(( $# % 2 )) -eq 1 ]; then
  help
  exit 1
fi

while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-c|--cluster)
		cluster="$2" ;;
		-core)
		core="$2" ;;
		-st|--subtype)
		subtype="$2" ;;
		-r|--reservation)
		reservation="$2" ;;
		-w|--walltime)
		walltime="$2" ;;
		-b|--benchmark)
		benchmark="$2" ;;
		-cl|--class)
		class="$2" ;;
		-host)
		host="$2" ;;
		*) # Unkown argument
		echo "Unkown argument: $1"
		help
		;;
	esac
	shift # next arg
	shift # next val
done

job=$benchmark-$class-$core
output_file="$benchmark.$class.$cluster.c$core.x$host.stdout"
error_file="$benchmark.$class.$cluster.c$core.x$host.stderr"

if [ "$benchmark" = "bt" ]; then
	case $subtype in 
		full)
		subtype="mpi_io_full" ;;
		simple)
		subtype="mpi_io_simple" ;;
		fortran)
		subtype="fortran_io" ;;
		epio)
		subtype="ep_io" ;;
	esac
fi

if [ -z "$reservation" ] || [ -z "$walltime" ] || [ -z "$benchmark" ] || [ -z "$class" ]; then
	echo -e "\tmissing argument"
	exit 1
fi

if [ "$benchmark" = "bt" ] && [ -n "$subtype" ]; then
	job=$benchmark-$subtype-$class-$core
	output_file="$benchmark.$subtype.$class.$cluster.c$core.x$host.stdout"
	error_file="$benchmark.$subtype.$class.$cluster.c$core.x$host.stderr"
fi

ndir=$((core * host))
dir_res="result_$ndir.core"

# Show arguments
echo -e "\n### ARGUMENTS ###"
echo "Cluster: $cluster"
echo "Job: $job"
echo "Reservation: $reservation"
echo "Walltime: $walltime"
echo "Benchmark: $benchmark"
echo "Subtype: $subtype"
echo "Class: $class"
echo "output file: $output_file"
echo "error file: $error_file"
echo -e "#################\n"

echo -e "So:\n"

command="oarsub -p \"$cluster\" --name \"$job\" --reservation=\"$reservation\" --queue=default --resource=/host=$host/core=$core,walltime=$walltime \"mpirun -np $ndir ./bin/$benchmark.$class"

if [ "$benchmark" = "bt" ] && [ -n "$subtype" ]; then
	command="$command.$subtype"
fi

command="$command.x\" -d \"/home/abonfils/NPB3.4.2/NPB3.4-MPI\" -O \"$dir_res/$output_file\" -E \"$dir_res/$error_file\""

echo "$command"

eval "$command"
