# **Internship - HPC**
## **PERFORMANCE ANALYSIS OF HPC BENCHMARKS ON PUBLIC CLOUDS**

```
Academic year 2022 - 2023
12th may 2023 - 4th august 2023
```
| Grenoble INP-Polytech-Grenoble | UFSC - Federal University of Santa Catarina | LaPeSD - Distributed Systems Research Laboratory | 
|:--:|:--:|:--:| 
| ![Grenoble](./img/INP-Polytech-Grenoble.png) | ![UFSC](./img/UFSC.png) | ![LaPeSD](./img/LaPeSD.png) | 

---

## **TABLE OF CONTENT**

| Tasks | States | Dates |
|:---|:---|:---|
| [Learn more about AWS](#aws---amazon-web-service) | :heavy_check_mark: | 15/05 - 19/05 |
| [Learn more about MPI](#mpi-message-passing-interface) | :heavy_check_mark: | 19/05 - 22/05 |
| [Discovery: HPC@Cloud Toolkit](#hpccloud-toolkit) | :heavy_check_mark: | 22/05 - 29/05 |
| [Scalability](#scalability) | :heavy_check_mark: | 01/06 - 02/06 |
| [Discovery: NPB - NAS Parallel Benchmark](#npb---nas-parallel-benchmark) | :heavy_check_mark: | 01/06 - 06/06 |
| [Other Benchmarks](#other-benchmarks) | :heavy_check_mark: | 06/06 - 16/06 |
| [Discovery: Grid'5000](#grid5000) | :heavy_check_mark: | 02/06 - 07/06 |
| [Bare-metal cluster](#bare-metal-cluster) | :heavy_check_mark: | 03/06 - 04/06 & 22/06 - 23/06 |
| [Comparison processors](./Comparison_Processors.md) | :heavy_check_mark: | 19/06 - 23/06 & 27/06 |
| [Select problem classes for each NPB application](./Select_Classes.md) | :heavy_check_mark: | 19/06 - 23/06 & 26/06 - 27/06 & 05/07 |
| [Configuration of Grid'5000 environment](#configuration-of-grid5000-environment) | :heavy_check_mark: | 28/06 - 05/07 |
| [Determine tests](#determine-tests) | :heavy_check_mark: | 10/07 - 18/07 |
| [Tests on Grid'5000](#tests-on-grid5000) | :heavy_check_mark: | 18/07 - 21/07 |
| [Tests on AWS](#tests-on-aws) | :heavy_check_mark: | 24/07 - 28/07 |
| [Interpretation of results](#interpretation-of-results) | :heavy_check_mark: | 31/07 - 04/08 |

Legend:

+ :heavy_check_mark: : finish

+ :heavy_minus_sign: : unfinish

+ :x: : not available

---

## **AWS - Amazon Web Service**

First of all, you need to know what Cloud Computing is.

### **What is Cloud Computing?**

**Cloud computing** is the on-demand delivery of IT resources over the internet with pay-as-you-go pricing. On-demand delivery that the cloud computing platform has the resources you need, when you need them. 

### **What is Amazon Web Service (AWS)?**

**Amazon Web Service (AWS)** is a cloud computing platform. AWS provides a vast range of services across categories such as computing power, storage, databases, networking, security, machine learning, analytics, Internet of Things (IoT), and more. These services are designed to be highly available, scalable, and cost-effective, allowing users to pay for only the resources they consume, hence the term **pay-as-you-go pricing**.

*Notice*: You can found my [**"AWS Course" notes**](./AWS_course.md) in this gitlab repo.


## **MPI (Message Passing Interface)**

**MPI (Message Passing Interface)** is a library, or more exactly an API of High Level, for High Performance Computing (HPC) and this is a way of doing **distributed-memory parallel programming** on the message exchange paradigm. It enables independent programs to communicate with each other. MPI is a widely used tool in the world of supercomputing.

*Notice*: You can accessed of my [**"MPI exercices" gitlab**](https://gitlab.com/AntoineBF/mpi) via the link. this gitlab repo contains exercice topics from the university of Edinburg and EPCC, and my implemented codes.


## **HPC@Cloud Toolkit**

### **What is HPC@Cloud Toolkit?**

**HPC@Cloud Toolkit** is a provider-agnostic open-source software toolkit that facilitates the migration, testing, and execution of HPC applications in public clouds. HPC@Cloud Toolkit allows to HPC community to benefit from easily available public cloud resources with minimal effort, and presents an empirical approach to estimating cloud infrastructure costs for HPC workloads.

The toolkit aims to evaluate the cost-effectiveness of public cloud infrastructure for communication-intensive HPC workloads, explore fault-tolerance strategies optimized for public cloud clusters, weigh up the pros and cons of migrating HPC applications to public cloud platforms, and develop software tools to fill existing technical gaps.

### **Setting up a development environment**

For this, we must install `git`, `docker`, `python 3.11` and `poetry`.

### **First using**

First, we must clone the "HPC@Cloud Toolkit" repo.
```UNIX
git clone https://github.com/lapesd/hpcac-toolkit.git
cd hpcac-toolkit/
code .
```

We can see what the `Makefile` file contains.

```UNIX
cat Makefile
```

Then, we launch the HPC@Cloud infrastructure containers only.

```UNIX
make docker-run-dev
```

We can read the `README.md` file to know next steps.

```UNIX
cd hpcc_services/
cat README.md 
```

Next, we set up `poetry`.

```UNIX
poetry shell
poetry install
```

If you don't have a ssh public key (an `id_rsa.pub` file), you must generate one with the following command.

```UNIX
cd ~/.ssh
ssh-keygen
```

Then, you see name ssh pblic key, and put it in the `cluster_config.yaml` file (`public_key_name:`) in `/hpcac-toolkit/hpcc_services/` folder.

```UNIX
cat ~/.ssh/id_rsa.pub
cd <your_path>/hpcac-toolkit/hpcc_services
```

### **To create cluster**

To create cluster, we must to complete the `aws_secret_key` and `aws_access_key` values in the `cluster_config.yaml`. For this, we must to create an access key in: [AWS Website](https://aws.amazon.com/) > `Profile` > `Security credentials` > `Access key`.

| Profile | Security credentials |
|:---|:---|
| ![website > profile](./img/AWS_website.png) | ![Security credentials > Access key](./img/AWS_access_key.png) |

```UNIX
python manage.py migrate
python manage.py create_cluster_config cluster_config.yaml
python manage.py create_cluster test_cluster
```

Normally, you can see in your terminal a message like this:

```UNIX
master_node_public_ip = "XXX.XXX.XXX.XXX"
Successfully spawned a Cluster using the `XXX ClusterConfiguration`!

To copy files from your machine to the cloud cluster, use the `scp` command:

scp -r ~/Documents/4A/STAGE/jacobi-method ec2-user@XXX.XXX.XXX.XXX:/var/nfs_dir

The command above will copy the `jacobi-method` folder and all files inside it to the 
/var/nfs_dir shared cluster directory.

You can also execute commands in your cluster from your local machine using `ssh`:

ssh ec2-user@XXX.XXX.XXX.XXX make all -C /var/nfs_dir/jacobi-method

The command above will run the `make all -C /var/nfs_dir/jacobi` command, compiling the 
jacobi-method application (https://github.com/vanderlei-filho/jacobi-method), which can 
then be executed by the following:

ssh ec2-user@XXX.XXX.XXX.XXX mpirun --oversubscribe --with-ft ulfm -np 4 --hostfile /var/nfs_dir/hostfile /var/nfs_dir/jacobi-method/jacobi_ulfm -p 2 -q 2 -NB 128

Don't forget to edit an appropriate hostfile and copy it to the cluster.
You can use the `hostfile.openmpi.example` and `hostfile.mvapich2.example` files as templates.

Finally, if you want to access your cluster directly over the command-line, use SSH:

ssh ec2-user@XXX.XXX.XXX.XXX
```

*Notice*: The previously mentioned `hostfile.mvapich2.example` is located in the `/hpcac-toolkit/hpcc_services/hcl_blueprints/aws-spot/` folder.

:warning: **Don't forget these following commands** :warning:

```UNIX
python manage.py destroy_cluster
exit
cd ..
make docker-stop
```


## **Scalability**

**Scalability** is the measure of how effectively parallelization occurs, expressed as the ratio between the actual speedup and the ideal speedup achieved with a specific number of processors. We can see two scalability types:

| Types | Characteristics | Calculs | Ideal cases |
|:---|:---|:---|:---|
| Weak Scaling | Both the number of processors and the problem size are increased. This also results in a constant workload per processor. | Gustafson’s law: $Speedup=s+p \times N$ ; $^1Efficiency=\frac{t(1)}{t(N)}$ | Efficiency is equal to $1$. |
| Strong Scaling | The number of processors is increased while the problem size remains constant. This also results in a reduced workload per processor. | Amdahl’s law: $Speedup=\frac{1}{(s+\frac{p}{N})}$ ; $^1Speedup=\frac{t(1)}{t(N)}$ | Linear speedup equal to the number of processors. ($Speedup = N$) |

* `s`: the proportion of execution time spent on the serial part
* `p`: the proportion of execution time spent on the part that can be parallelized
* `N`: the number of processors
* `t(1)`: the computational time for running the software using one processor
* `t(N)`: the computational time running the same software with N processors

$^1$ If the amount of time needed to complete a serial task (not parallel) t(1), and the amount of time to complete the same unit of work with N processing elements (parallel task) is t(N), so we have two possibilities: in case of **strong scaling**, then $Speedup$ is equals to $\frac{t(1)}{t(N)}$, else, in case of **weak scaling**, $Efficiency$ is equals to $\frac{t(1)}{t(N)}$.

:warning: ***Update***: After the June 16 meeting, I won't be using Weak Scaling, because it is the most complicated due to the predefined problem class (NPB). I have to go for Strong Scaling.

For example, BT (Operation type: floating point):

| problem size | number of processors | Time in seconds | weak scaling efficiency = (T(1)/T(Np)) | problem size (N X N X N), N is Grid size | Total number of cells |
|:---|:---|:---|:---|:---|:---|
| S | 1 | 0.05 | 1 | 12 | 1 728 |
| W | 4 | 0.47 | 0.10638 | 24 | 13 824 |
| A | 9 | 6.58 | 0,00756 | 64 | 262 144 |
| B | 16 | 19.37 | 0,00258 | 102 | 1 061 208 |
| C | 25 | 50.15 | 0,00100 | 162 | 4 251 528 |
| D | 36 | 751.20 | 0,00007 | 408 | 67 917 312 |

We note that it respects the increase in the number of processors and the size of the problem, but **does not** respect a constant workload per processor.


## **NPB - NAS Parallel Benchmark**

The **NAS Parallel Benchmarks (NPB)** are a set of programs designed to help evaluate the performance of parallel supercomputers. The reference test suite uses different problem classes whose problem sizes are predefined in NPB. These classes are as follows: 
* **S** class: Small for quick tests.
* **W** class: Size for the 90's workstations.
* **A**,**B**,**C** classes: Standard problem tests. (approximately 4 times size increase from each of the previous classes)
* **D**,**E**,**F** classes: High problem tests. (approximately 16 times size increase from each of the previous classes)

*Notice*: As I only work on benchmarks implemented with MPI, I will only present these.

The benchmarks are derived from computational fluid dynamics (CFD) applications. There are **five kernels** and **three pseudo-applications**:

| five kernels | What does it do? | What is it testing? |
|:---|:---|:---|
| **IS** - **Integer Sort** (random memory access) | This kernel performs a sorting operation that is important in "particle method" code | It tests both integer computation speed and communication performance. |
| **EP** - **Embarrassingly Parallel** | It provides an estimation of the upper achievable limits for floating point performance (the performance without significant interprocessor communication), i.e a relatively little or negligible communication between processes. | It tests performances executing a set of parallel tasks and evaluating this ability to execute them efficiently, in a balanced and fast manner. |
| **CG** - **Conjugate Gradient** (irregular memory access and communication) | It is used to compute an approximation to the smallest eigenvalue of a large, sparse, symmetric positive definite matrix. | This kernel tests irregular long-distance communication using unstructured matrix vector multiplication. |
| **MG** - **Multi-Grid** (on a sequence of meshes, long- and short-distance communication, memory intensive) | It requires highly structured long-distance communication. | It tests both short and long-distance data communication. |
| **FT** - discrete 3D fast **Fourier Transform** (all-to-all communication) | It performs the essence of many spectral codes. | It is a "rigorous" test of long-distance communication performance. |

:warning: ***Update***: After the July 7 meeting, I will only use these five kernels only. The following applications up to [Tests](#tests) title will not be used.

| three pseudo-applications | They allow to resolve a synthetic system of no linear PDEs (partial differential equation). | They exist also in multizones versions. |
|:---|:---:|:---:|
| **LU** - **Lower-Upper** Gauss-Seidel solver | " | " |
| **SP** - **Scalar Penta-diagonal** solver | " | " |
| **BT** - **Block Tri-diagonal** solver | " | " |

![Problem sizes and parameters for each classes defined in NPB 3.4](./img/Problem_sizes_and_parameter.png)

*Problem sizes and parameters for each classes defined in NPB 3.4 (Empty cells indicate undefined problem sizes).*

There exist also NPB MultiZones versions, called **NPB-MZ**, which are designed to exploit multiple levels of parallelism in applications, and to test the effectiveness of multi-level and hybrid parallelization paradigms and tools. They use the multizone mesh system which we can explain as follows: A rectangular mesh is divided into a two-dimensional horizontal pavement of three-dimensional zones of approximately the same size as the original NPB.

![single-zone to multizone](./img/mutli-zone_mesh_sys.png)

*Conversion from single-zone to multi-zone NPBs*

There exist three types of reference problems derived from NPB single-zone pseudo-applications:

| NPB Multi-zone | Descriptions |
|:---|:---|
| **BT-MZ** | zones of unequal size within a problem class, with the number of zones increasing as the problem class grows. |
| **SP-MZ** | zones of equal size within a problem class, with the number of zones increasing as the problem class grows. |
| **LU-MZ** | zones of equal size within a problem class, with a fixed number of zones for all problem classes. |

![Problem sizes and parameters for each classes defined in NPB 3.4-MZ](./img/Problem_sizes_and_parameter_MZ.png)

*Problem sizes and parameters for each classes defined in NPB 3.4-MZ*

And finally, there are benchmarks for unstructured calculations, parallel I/O and data flow:

| Benchmarks | Descriptions |
|:---|:---|
| **BT-IO** - test of different parallel **I/O** techniques | To measure the system's ability to read and write data blocks simultaneously and in parallel. |
| **DT** - **Data Traffic** | To measure performance and capacity to handle heavy traffic, providing essential information for optimizing network performance. |

This DT application has the following requirement: The number of MPI processes must not be less than the number of nodes in the graph.

DT has an additional argument that is a graph. We have three possibility:
- **BH** for Black Hole
- **WH** for White Hole
- **SH** for SHuffle

| Problem classes \ Graphs | BH | WH | SH |
|:---:|:---:|:---:|:---:|
| S | 5 | 5 | 12 |
| W | 11 | 11 | 32 |
| A | 21 | 21 | 80 |
| B | 43 | 43 | 192 |

*Numbers of nodes in relation to problem classes and communication graphs.*

Moreover, BT-IO has an extra parameter called SUBTYPE, which can have the following values: full, simple, fortran or epio, or empty.

| SUBTYPE | Descriptions |
|:---|:---|
| **full** | MPI I/O with collective buffering. This means that data scattered in memory between processors is collected from a subset of the participating processors and reorganized before being written to a file to increase granularity. |
| **simple** | MPI I/O without collective buffering. This means that there is no reordering of data, so that numerous seek operations are required to write data to the file. |
| **fortran** | As **Simple**, but calls to the MPI I/O library are replaced by Fortran 77 file operations. |
| **epio** | This option doesn't comply with the benchmark requirements, because each participating process writes data belonging to its part of the domain to a separate file as a contiguous data stream. However, it does give a realistic measure of the maximum achievable I/O speed. |
| **empty** | To obtain the original BT |

### **Tests**

After downloading and extracting **NPB3.4.2** and **NPB3.4.2-MZ** archives, I tried to compile many tests. However, Fortran tests aren't compiled.

```UNIX
Error: Rank mismatch between actual argument at (1) and actual argument at (2) (scalar and rank-1)
make[2]: *** [Makefile:58 : bt.o] Erreur 1
```

After some research, I found a flag option (`-fallow-argument-mismatch`) to avoid the previous error.

You can see some of the results of running the **IS_A** and **BT-MZ_W** tests on my personal computer:

| NPB IS - CLASS A - 4 nodes | NPB BT-MZ - CLASS W - 4 nodes |
|:---|:---|
| ![IS.A.x](./img/IS.png) | ![BT-MZ.W.x](./img/BT-MZ.png) |


## **Bare-metal cluster**

### **What is Bare-metal?**

A Bare-metal cluster is a cluster that does not use virtualization. It is used for instances whose workload requires access to a set of hardware functions for the application running in a non-virtual environment. Either for licensing or other reasons.

### **On AWS**

On AWS, bare-metal instances have the suffix `metal` and have relatively high prices:

![Bare-metal prices](./img/baremetal_prices.png)

*Extract from the list of Bare-metal instances in ascending order of price (per hour) for **on-demand***

For **spot**, I found a [web site](https://instances.vantage.sh/aws/ec2/c5n.xlarge) that it contains instances characteristics like prices.

### **On Grid'5000**

On Grid'5000, We must to reserve the necessary resources. 

During the reservation, we must to indicate the duration of the reservation and any other specific requirements.

By default, the clusters don't use virtualization, so they're bare-metal clusters.


## **Other Benchmarks**

The aim is to find other benchmarks, in addition to NPB, with one main condition: these benchmarks must **be implemented with MPI**.

After some research, I found these following benchmarks:

| Name | Use MPI | Languages | Relevant | Tested on Grid'5000 | Result | Validation |
|:---|:---:|:---:|:---:|:---:|:---:|:---:|
| HPCC (HPC Challenge) | MPI | C | :x: : OK, but too old | :heavy_check_mark: | :heavy_minus_sign: | :x: |
| OSU Micro-Benchmarks | MPI | C, JAVA | :heavy_minus_sign: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| IMB (Intel MPI Benchmarks) | Intel MPI | C, C++ | :x: not realistic enough | :x: |  | :x: These are microbenchmarks. We are interested in more realistic applications. |
| Graph500 | MPI | C | :x: : uses too much memory space for the relevance of the results | :heavy_check_mark: | :heavy_check_mark: | :x: |
| HPCG (High-Performance Conjugate Gradient) | MPI | C++ | :x: | :x: |  | :x: |

I only keep two tests of point-to-point standard communication: `osu_latency` and `osu_bandwidth`.

If you want see my researchs on the other benchmarks (unvalidated), and more details about OSU Micro-Benchmarks: [*click here*](/other_benchmarks_(unvalidated)/README.md)


## **Grid'5000**

### **What is Grid'5000?**

Grid'5000 is a large-scale and flexible testbed for experiment-driven research in all areas of computer science, with a focus on parallel and distributed computing including Cloud, HPC and Big Data and AI.

### **A fair comparison**

For a fair comparison, it would be preferable to use clusters with similar processors in AWS and Grid'5000. 

So you need to find out the characteristics of the instances offered by both parties. Both offer clusters with CPUs from the Intel Xeon family. But AWS uses more and more new processor models, which makes the task hard.

You can see the [comparison processors page](./Comparison_Processors.md).

### **My use**

I run benchmarks on Grid'5000.

For this, I had to create a Grid'5000 account, then learn how **to create jobs**.

Before creating a job, I need to check that the cluster I want to use is free and that it will remain so for the duration of the job.

Next, I need to connect to the Grid 5000 access machines using ssh.

```UNIX
ssh <id_grid5000>@access.grid5000.co.uk
```

Once connected, you enter a specific site, for example the Grenoble site:

```UNIX
ssh grenoble
```

I recommend that you do the following to combine the two previous ssh connections into a single one.

In `~/.ssh/config`:

```
Host g5k
  User <id_grid5000>
  Host name access.grid5000.fr
  ForwardAgent no

Host *.g5k
  User <id_grid5000>
  ProxyCommand ssh g5k -W "$(basename %h .g5k):%p"
  ForwardAgent no
```

Now, you can use the following:

```UNIX
ssh grenoble.g5k
```

Then, you need to import files:

| What do you do? | Commands |
|:---|:---|
| Copy file from local to remote: | scp local_file remote_username@remote_ip:remote_file |
| Copy folder from local to remote: | scp -r local_folder remote_username@remote_ip:remote_folder |
| Copy file from local to remote: | scp remote_username@remote_ip:remote_file local_file |
| Copy folder from remote to local: | scp -r remote_username@remote_ip:remote_folder local_folder |

Finally, you can create a job.

```UNIX
oarsub -p <name_cluster> -n <name_job> --reservation="YYYY-MM-DD HH:mm:ss" --queue=default --resource=/host=1,walltime=H:m:s "mpirun -np 4 ./bin/bt.A.x" -d "<your_path>" -O "result/bt.A.stdout" -E "result/bt.A.stderr"
```

*Notice*: The best method to learn commands options is use the manual:

```UNIX
man oarsub
```

You can see information about jobs with:

```UNIX
oarstat -u
```

```UNIX
Job id     Name           User           Submission Date     S Queue
---------- -------------- -------------- ------------------- - ----------
2282761                   <id_grid5000>  2023-06-07 18:49:37 W default  
```

![First job on Grid'5000](./img/first_job_grid5000.png)

*My first job in Grid'5000*

If you need to add time to a job in progress, you can proceed as follows:

```UNIX
oarwalltime <id_job> <overtime>
```

Here are the results obtained on the `dahu` (CPUtype: `Intel Xeon Gold 6130`, OS: `debian11`, OpenMPI: `4.1.0`) cluster at the Grenoble site: [*LINK*](./results/)

I created a [bash script](./create_job.sh) to simplify job creation for NPB.


## **Configuration of Grid'5000 environment**

First of all, I need to know the environment created by HPC@Cloud to apply the same on Grid'5000.

Thanks to files on HPC@Cloud, I know Grid'5000 used this command line: `git clone --recursive https://github.com/open-mpi/ompi.git` to retrieve the last OpenMPI version.

I checked this with this commande line: `mpirun --version`; the result confirmes `5.1.0a1`.

Now, this is time to know the OS. I used these command line:

```UNIX
cat /etc/os-release
cat /proc/version
```

The results are:

```UNIX
NAME="Amazon Linux"
VERSION="2"
ID="amzn"
ID_LIKE="centos rhel fedora"
VERSION_ID="2"
...
```
and
```UNIX
Linux version 5.10.179-166.674.amzn2.x86_64 (mockbuild@ip-10-0-50-146) (gcc10-gcc (GCC) 10.4.1 20221124 (Red Hat 10.4.0-1), GNU ld version 2.35.2-9.amzn2.0.1) #1 SMP Mon May 8 16:54:25 UTC 2023
```
,respectively.

We can see more information interesting: Amazon Linux 2 is a linux distribution linked / derived to CentOS (So, RHEL (RedHat)) and Fedora.

So, an equivalent can be a **CentOS** or Fedora distribution with a x86_64 architecture.

According to an AWS document, Amazon Linux 2 features a **high level of EPEL compatibility** with **CentOS 7**. This is small detail but we know it now.

Grid'5000 maintains several reference environments directly available for deployment in all sites. Among them, there are **CentOS 7**, **8** and **CentOS Stream 8** and **9**.

When I deploy a minimal CentOS 7.8-based environment on Grid'5000, there are big differences, such as the version of GCC (gcc, mpif90 and mpicc) and the version of OpenMPI.

| Grid'5000 (CentOS 7.8) | Grid'5000 (Debian 11) | AWS |
|:---|:---|:---|
| GCC 4.8.5 | GCC 10.2.1 | GCC 7.3.1 |
| none | OpenMPI 4.1.0 | OpenMPI 5.1.0a1 |

You can see [my steps](./Environment_creation.md) to create an customazed environment with `CentOS 7.8` of Grid'5000.

After the update, the differences are smaller and some values are similar:
| Grid'5000 (CentOS 7.8) | AWS |
|:---|:---|
| (gcc version 4.8.5 20150623 (Red Hat 4.8.5-44) (GCC) ) | (gcc10-gcc (GCC) 10.4.1 20221124 (Red Hat 10.4.0-1), GNU ld version 2.35.2-9.amzn2.0.1) |
| mpirun (OpenMPI) 5.1.0a1 | mpirun (OpenMPI) 5.1.0a1 |
| ID="centos" & ID_LIKE="rhel fedora" | ID_LIKE="centos rhel fedora" |
| GCC 7.3.0 | GCC 7.3.1 |

*Notice*: We don't need to use `-fallow-argument-mismatch` with this environment.


## **Determine Tests**

### **Procedure and Scripts**

I will follow this table on Grid'5000 (the last column may not be used):

| #Nodes | 1 | 2 | 4 | 8 |
|:---:|:---:|:---:|:---:|:---:|
| #MPI processes / Cores | 2 | 2 | 2 | 2 |
| #MPI processes / Cores | 4 | 4 | 4 | 4 |
| #MPI processes / Cores | 8 | 8 | 8 | 8 |
| #MPI processes / Cores | 16 | 16 | 16 | 16 |

So, for the total number of MPI processes for each case:

| #MPI processes \ #Nodes | 1 | 2 | 4 | 8 |
|:---:|:---:|:---:|:---:|:---:|
| 2 | 2 | 4 | 8 | 16 |
| 4 | 4 | 8 | 16 | 32 |
| 8 | 8 | 16 | 32 | 64 |
| 16 | 16 | 32 | 64 | 128 |


To test, I will use hostfiles. You can see a [bash script](./useful_NPB_files/for_what_i_need/generate_hostfile.sh) to generate them.

I will also use an other [bash script](./useful_NPB_files/for_what_i_need/exec.sh) that it executes the 5 applications with the four different number of core, with a given number of node.

### **Correspondence table**

|  | Grid'5000 | AWS |
|:---|:---|:---|
| Node | Dahu-X | Instance / Master + Worker(s) |
| Core | Ressource | vCPU |
| Slot | CPU Core | CPU Core |

### **Results and Decisions**

After running all the possibilities on Grid'5000, I was able to determine which tests to keep for AWS.

Following the results obtained on AWS, I had to make compromises between the results obtained on Grid'5000 and those obtained on AWS in order to obtain nice efficiency curves for both the tests carried out on Grid'5000 and AWS.

## **Tests on Grid'5000**

### **How to run on Grid'5000 cluster?**

To execute NPB application, I connected on my Grid'5000 account:
```UNIX
ssh grenoble.g5k
```

Then, I created an interactive deployment job:
```UNIX
oarsub -I -t deploy --name "NPB applications" --resource=/host=8,walltime=1 -p "eth_rate=10"
```

Next, I deployed my custom environment on all reserved nodes:
```UNIX
kadeploy3 myCentos7-env -k ~/.ssh/id_rsa.pub -f $OAR_NODEFILE
``` 

Then,I connected on one node among them (in this example, the dahu-3 node):
```UNIX
ssh abonfils@dahu-3.grenoble.grid5000.fr
```

Next, I created hostfiles:
```UNIX
cd npb/
rm -rf hostfolder/
bash generate_hostfile.sh 8 1 2 3 4 5 7 12 25
```

[generate_hostfile.sh](/useful_NPB_files/for_what_i_need/generate_hostfile.sh)

Finally, I launched applications according to the number of node (in this example, all applications with 4 nodes):
```UNIX
cd NPB3.4-MPI/
bash exec.sh g5k 4
```

[exec.sh](/useful_NPB_files/for_what_i_need/exec.sh)

In exec.sh file, I use this mpirun line command (in this example, it executes EP application with C class, 4 nodes, 8 MPI processes, so, 32 processes in total):
```UNIX
mpirun -np 32 --mca orte_rsh_agent oarsh -machinefile ../hostfolder/hostfile.ompi.g5k4/hostfile.ompi8 --mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib --mca btl_tcp_if_include enp24s0f0 bin/ep.C.x
```

### **Results**

We can see these results in [Grid5000_results](/results/NPB/Grid5000_results/) foldler.

*Notice for Tests on **G5K** and **AWS***: 

- If I need to run one test and I doubt on mpirun options, I can use [cmd_mpirun.sh](./useful_NPB_files/for_what_i_need/cmd_mpirun.sh).

- I create a [parsing bash script](/useful_NPB_files/for_what_i_need/parsing_result.sh) to simplify result files. You can see an example of [result file](./results/parsing_example/results_S9.txt) and a [parsed result file](./results/parsing_example/parsed_S9.txt).

## **Tests on AWS**

### **How to run on AWS cluster?**

First of all, I create a cluster with HPC@Cloud Toolkit.

When I was using several instances, I had to put my application files in `/var/nfs_dir/` folder so that all instances could access them.

I was using a [bash script](./useful_NPB_files/for_what_i_need/exec_aws.sh) to launch only tests I needed.

### **Results**

We can see these results in [AWS_results](/results/NPB/AWS_results/) foldler.

## **OSU tests**

To launch OSU tests, I create and use [exec_osu.sh](./useful_NPB_files/for_what_i_need/exec_osu.sh) bash script. These [results](/results/OSU/) are useful for interpreting NPB results.


## **Interpretation of results**

We can see the interpretation of the OSU tests results, the NPB applications results and the final conclusion with this [link](./Interpretation_of_results.md).


## **Sources**

### **HPC@Cloud Toolkit**

* [HPC@Cloud Toolkit Github](https://github.com/lapesd/hpcac-toolkit)

### **NAS Parallel Benchmarks**

* [NAS Parallel Benchmarks](https://www.nas.nasa.gov/software/npb.html)

* [the NAS Parallel Benchmarks - report](https://www.nas.nasa.gov/assets/nas/pdf/techreports/1994/rnr-94-007.pdf)

* [Problem Sizes and Parameters in NAS Parallel Benchmarks](https://www.nas.nasa.gov/software/npb_problem_sizes.html)

* [NAS Parallel Benchmarks I/O Version 2.4](https://www.nas.nasa.gov/assets/nas/pdf/techreports/2003/nas-03-002.pdf)

* [NAS Parallel Benchmarks, Multi-Zone Versions](https://www.nas.nasa.gov/assets/nas/pdf/techreports/2003/nas-03-010.pdf)

### **Scalability**

* [Scalability definition](https://hpc-wiki.info/hpc/Scaling)

### **AWS**

* [Amazon Instances](https://aws.amazon.com/fr/ec2/instance-types/)

* [c5n amazon instance characteristics](https://instances.vantage.sh/aws/ec2/c5n.xlarge)

* [Information on bare-metal clusters](https://aws.amazon.com/fr/blogs/containers/getting-started-with-eks-anywhere-on-bare-metal/)

* [Prices of bare-metal instances](https://aws.amazon.com/fr/ec2/pricing/on-demand/)

* [Amazon Linux 2 Compatibility](https://docs.aws.amazon.com/linux/al2023/ug/compare-with-al2.html)

* [Service Quotas: Spot Instances](https://us-east-1.console.aws.amazon.com/servicequotas/home/services/ec2/quotas/L-34B43A08)

* [Service Quotas: On-Demand Instances](https://us-east-1.console.aws.amazon.com/servicequotas/home/services/ec2/quotas/L-1216C47A)

### **Grid’5000**

* [Grid’5000 user space](https://api.grid5000.fr/stable/users/)

* [Grid’5000: getting started](https://www.grid5000.fr/w/Getting_Started)

* [View Grid'5000 clusters/nodes availability](https://www.grid5000.fr/w/Status)
    - Access to **drawgantt** page, i.e a node allocation agenda.
    - Access to **monika** page, i.e a node resource status table.

* [Grid’5000: OAR Properties](https://www.grid5000.fr/w/OAR_Properties#Hardware)

* [Grid’5000: Hardware in all sites](https://www.grid5000.fr/w/Hardware)

* [Grid’5000: Advanced Kadeploy](https://www.grid5000.fr/w/Advanced_Kadeploy)

* [Grid’5000: Environment creation](https://www.grid5000.fr/w/Environment_creation)

* [How to install GCC on CentOS 7](https://linuxhostsupport.com/blog/how-to-install-gcc-on-centos-7/)

* [Run MPI on Grid'5000](https://www.grid5000.fr/w/Run_MPI_On_Grid%275000)

* [MCA (Module Component Architecture) parameters](https://www.open-mpi.org/faq/?category=tuning#mca-params)

### **Others**

* [Fortran flag -fallow-argument-mismatch](https://gcc.gnu.org/onlinedocs/gfortran/Fortran-Dialect-Options.html)

* [os-release details](https://www.freedesktop.org/software/systemd/man/os-release.html)

***
