# Select problem classes for each NPB application

*Notice*: I have determined problem classes for each NPB applications based on [tests running](./results/NPB/NPB_result_4.md) on Grid'5000. I used **an empirical approach** to select these classes. I tested on Grid'5000, more precisely, **one dahu node** with `debian 11`, `OpenMPI 4.1.0`, `GCC 10.2.1`, and I used **2 criteria**: the execution time is **greater than** one second and **less than** 10 minutes.

:warning: ***Update*** (After the June 16 meeting): As I'm only using strong scalability, I only need one class each application because the problem size needs to be **constant**. For the second selection phase, I chosen the classes from the previous selection and I tested again on **the new environment based on `CentoS 7.8`** with `OpenMPI 5.1.0a1`, `GCC 7.3.0`and the same criteria. Nevertheless, when the execution time was too short, I tried with the next class up, even if it's not in the previous selection, like for **BTIO - simple**. Also, if the execution time was too high, I tried with the previous class, like **EP**. And if the application doesn't work with the previous selected class, I also tried with the previous class, like **IS**.

| five kernels | Classes selection (Phase I) | Classes selection (Phase II) | Validation |
|:---|:---|:---|:---|
| **IS** - **Integer Sort** | C, D | **C** | :heavy_check_mark: |
| **EP** - **Embarrassingly Parallel** | A, B, C, D | **C** | :heavy_check_mark: |
| **CG** - **Conjugate Gradient** | B, **C** | **C** | :heavy_check_mark: |
| **MG** - **Multi-Grid** | B, **C** | **C** | :heavy_check_mark: |
| **FT** - discrete 3D fast **Fourier Transform** | B, **C** | **C** | :heavy_check_mark: |

:warning: ***Update***: After the July 7 meeting, I will only use these five kernels only. The following applications won't be used.

---

| three pseudo-applications | Classes selection (Phase I) | Classes selection (Phase II) | Validation |
|:---|:---:|:---:|:---|
| **LU** - **Lower-Upper** Gauss-Seidel solver | W, A, B, **C** | **C** | :heavy_check_mark: |
| **SP** - **Scalar Penta-diagonal** solver | A, B, **C** | **C** | :heavy_check_mark: |
| **BT** - **Block Tri-diagonal** solver | A, B, **C** | **C** | :heavy_check_mark: |

---

| NPB Multi-zone | Classes selection (Phase I) | Classes selection (Phase II) | Validation |
|:---|:---|:---|:---|
| **BT-MZ** | A, B, **C** | **C** | :heavy_check_mark: |
| **SP-MZ** | A, B, **C** | **C** | :heavy_check_mark: |
| **LU-MZ** | A, B, **C** | **C** | :heavy_check_mark: |

---

For these cases, it is different because they **generate files** whose size is **proportional** to the problem class. Still using an empirical approach, I tested these applications and determined that:

* Class S: 0.83 MB
* Class W: 22.12 MB
* Class A: 419.43 MB
* Class B: 1697.93 MB
* Class C: 6802.44 MB

So, I used the previous criteria, plus size criterion.

| Benchmarks for parallel I/O | Classes selection (Phase I) | Classes selection (Phase II) | Validation |
|:---|:---|:---|:---|
| **BT-IO** - **full** | A, B, **C** | **C** | :heavy_check_mark: |
| **BT-IO** - **simple** | W, A, B | **C** | :heavy_check_mark: |
| **BT-IO** - **fortran** | A, B, **C** | **C** | :heavy_check_mark: |
| **BT-IO** - **epio** | A, B, **C** | **C** | :heavy_check_mark: |

:warning: With these choices, I will don't forget to delete the output file after each test.

*Notice*: These choices have consequences for the **memory capacity** of the instance to be selected.

***

:arrow_backward: [README.md](./README.md)

***
