#!/bin/bash

# This script executes commands to create BT-IO executables.
subtypes=("full" "simple" "fortran" "epio")
classes=("S" "W" "A" "B" "C" "D")

for subtype in "${subtypes[@]}"; do
  for class in "${classes[@]}"; do
    eval "make bt CLASS=$class SUBTYPE=$subtype"
  done
done
