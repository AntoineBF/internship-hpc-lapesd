#!/bin/bash

# This script allows us to generate all hostfiles we need.

process_list=("1" "2" "4" "8" "16")
node_list=("1" "2" "4" "8") # "16")

# Create a folder if it doesn't already exist and enter it.
eval "mkdir -p hostfolder && cd hostfolder"

provider=""

# If the provider hasn't been specified, it declares an argument error.
if [ "$#" -eq 0 ]; then
	eval "echo \"Error1: Argument is empty -> [aws or g5k].\""
	eval "echo \"If is g5k, you must add the number of nodes and a power of two (up to ${node_list[-1]} and it must be equal to the number of nodes) IP address suffixes.\""
	exit 1
fi

provider=$1

# Create hostfiles for AWS
if [ "$provider" = "aws" ]; then

	file=0
	addr="10.0.0."
	incr=10

	# Create hostfiles for AWS
	for node in "${node_list[@]}"; do
		eval "mkdir hostfile.ompi.aws$node && cd hostfile.ompi.aws$node"
		#eval "pwd"
		for process in "${process_list[@]}"; do
			incr=10
			file="hostfile.ompi$process"
			eval "touch $file"
			for slot in $(seq 1 "$node"); do
				#eval "pwd"
				eval "echo \"$addr$incr slots=$process max-slots=$process\" >> $file"
				((incr+=1))
			done
		done
		eval "cd .."
	done
	eval "echo \"$0 is done for AWS hostfiles.\""
elif [ "$provider" = "g5k" ]; then

	nbadr=$2
	# Check the consistency of IP address suffix numbers
	if [[ ! " ${node_list[@]} " =~ " $nbadr " ]]; then
		eval "echo \"Error2: Argument is empty -> [aws or g5k].\""
		eval "echo \"If is g5k, you must add the number of nodes and a power of two (up to ${node_list[-1]} and it must be equal to the number of nodes) IP address suffixes.\""
		exit 1
	fi
	addr="172.16.20."
	incr=1
	
	# Stock the IP address suffixes
	for param in "${@:2}"; do
		# Add the argument to the array
		adrsuf_list+=("$param")
	done

	# Check the number of IP address suffixes
	if [ "${#adrsuf_list[@]}" -ne "$((nbadr+1))" ]; then
		eval "echo \"Error3: Argument is empty -> [aws or g5k].\""
		eval "echo \"If is g5k, you must add the number of nodes and a power of two (up to ${node_list[-1]} and it must be equal to the number of nodes) IP address suffixes.\""
		exit 1
	fi

	# Create hostfiles for Grid'5000 (Grenoble: dahu cluster)
	for node in "${node_list[@]}"; do
		if [ "$node" -le "$nbadr" ]; then
			eval "mkdir hostfile.ompi.g5k$node && cd hostfile.ompi.g5k$node"
			#eval "pwd"
			for process in "${process_list[@]}"; do
				incr=1
				file="hostfile.ompi$process"
				eval "touch $file"
				for slot in $(seq 1 "$node"); do
					#eval "pwd"
					eval "echo \"$addr${adrsuf_list[incr]} slots=$process max-slots=$process\" >> $file"
					((incr+=1))
				done
			done
			eval "cd .."
		fi
	done

	## Create hostfiles for Grid'5000 (Grenoble: dahu cluster)
	#for node in "${node_list[@]}"; do
	#	if [ "$node" -le "$nbadr" ]; then
	#		eval "mkdir machinefile$node && cd machinefile$node"
	#		#eval "pwd"
	#		for process in "${process_list[@]}"; do
	#			incr=1
	#			file="machinefile$process"
	#			eval "touch $file"
	#			for slot in $(seq 1 "$node"); do
	#				#eval "pwd"
	#				for ((i=1; i<=$process; i++)); do
	#					eval "echo \"dahu-${adrsuf_list[incr]}.grenoble.grid5000.fr\" >> $file"
	#				#" slots=$process max-slots=$process
	#				done
	#				((incr+=1))
	#			done
	#		done
	#		eval "cd .."
	#	fi
	#done

	eval "echo \"$0 is done for Grid'5000 hostfiles.\""
else
	eval "echo \"Error4: Wrong argument -> [aws or g5k].\""
	exit 1
fi
