#!/bin/bash

provider=$1
node=$2
file=$3
repetition=$4

#addrhost=${ip addr show | grep -oP '(?<=inet\s)\d+(\.\d+){3}' | grep -v '127.0.0.1'}
#echo "$addrhost"

if [ "$#" -lt 3 ]; then
	echo "Error: Argument is empty -> [aws or g5k] [#node] [output_file] (#repetition)."
	exit 1
fi

# Check if $3 is empty
if [ -z "$3" ]; then
    repetition=1
fi

echo "\$$repetition repetition(s)"

run_with_hosts() {
	local node=$1
	local core=$2
	local app=$3
	#local filepath="osu-micro-benchmarks-7.1-1/c/mpi/pt2pt/standard/"
	local filepath="osu-micro-benchmarks-7.1-1/c/mpi/collective/blocking/"
	local np=$((core*node))
	if [ $provider = "aws" ]; then
		eval "mpirun -np $np --hostfile /var/nfs_dir/osu/hostfolder/hostfile.ompi.$provider$node/hostfile.ompi$core --mca btl_tcp_if_include eth0 $filepath$app >> $file.txt"
	elif [ $provider = "g5k" ]; then
		eval "mpirun -np $np --hostfile ~/npb/hostfolder/hostfile.ompi.$provider$node/hostfile.ompi$core --mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib --mca btl_tcp_if_include enp24s0f0 $filepath$app >> $file.txt"
	else
		echo "Error: Wrong provider -> [aws or g5k] [#node] [output_file] (#repetition)."
		exit 1
	fi
}

for ((i=1; i<=$repetition; i++)); do
	#apps=("osu_bw" "osu_latency" "osu_latency_mp")
	apps=("osu_alltoall" "osu_bcast" "osu_allreduce" "osu_reduce")
	for app in "${apps[@]}"; do
		run_with_hosts 2 1 "$app"
	done
done