#!/bin/bash

# this scripts allows us deploy our environment (and launch tests) on grid'5000.

node=$1

if [ "$#" -eq 0 ]; then
	echo "Error: Argument is empty -> [#node N] [IP adress suffix 1] ... [IP adress suffix N]."
	exit 1
fi

args=()

for ((i=1; i<=$#; i++)); do
	args+=("${!i}")
done

#command="kadeploy3 -e myCentos7-NPB -k -m dahu-"
##command="kadeploy3 -a centEnv.yaml"
#
#if [ "${#args[@]}" -gt 1 ]; then
#	command="$command["
#	for ((i=1; i<${#args[@]}; i++)); do
#    	command="$command\"${args[i]}\""
#	    if [ $i -lt $(( ${#args[@]} - 1 )) ]; then
#	        command="$command,"
#	    fi
#	done
#	command="$command]"
#else
#	command="$command${args[1]}"
#fi
#
#command="$command.grenoble.grid5000.fr"

eval "kadeploy3 myCentos7-env -k ~/.ssh/id_rsa.pub -f $OAR_NODEFILE"

eval "$command"
if [ $? -eq 0 ]; then
	echo "-> Done with success."
else
	echo "-> Something is wrong."
fi

#eval "sshpass -p "toto" ssh user1@dahu-${args[1]}.grenoble.grid5000.fr"
eval "ssh abonfils@dahu-${args[1]}.grenoble.grid5000.fr"

eval "cd npb/"
command="bash generate_hostfile.sh g5k $node"
#command="bash generate_hostfile_g5k.sh g5k $node"
#for ((i=1; i<${#args[@]}; i++)); do
#	command="$command ${args[i]}"
#done
#command="bash generate_hostfile.sh g5k $node"
for ((i=1; i<${#args[@]}; i++)); do
	command="$command ${args[i]}"
done

eval "$command"
if [ $? -eq 0 ]; then
	echo "-> Done with success."
else
	echo "-> Something is wrong."
fi

eval "cd NPB3.4-MPI/"

eval "bash exec.sh g5k $node >> g5k$node.txt"

eval "exit"

echo "$0 is done."
