#!/bin/bash

provider=$1
node=$2

#addrhost=${ip addr show | grep -oP '(?<=inet\s)\d+(\.\d+){3}' | grep -v '127.0.0.1'}
#echo "$addrhost"

if [ "$#" -le 1 ]; then
	echo "Error: Argument is empty -> [aws or g5k] [#node]."
	exit 1
fi

program_list=("is" "ep" "cg" "mg" "ft")
core_list=("2" "4" "8" "16")

echo "Cleaning..."
eval "make veryclean"

if [ "$provider" = "aws" ]; then

	for core in "${core_list[@]}"; do
	
		echo "Recreating..."
		eval "make suite"
	
		for program in "${program_list[@]}"; do
			echo "Program execution: $program with $core per node and $node node(s)"
			
			# Test launch
			res=$((core*node))
			command="mpirun -np $res --hostfile ../hostfolder/hostfile.ompi.$provider$node/hostfile.ompi$core bin/$program.C.x"
			#command="echo \"$command\""
			#command="sleep $core"
			eval "$command"
			
			# Checking the process return code
			if [ $? -eq 0 ]; then
				echo "$command: done with success."
			else
				echo "$command: something is wrong."
			fi
			
			echo "Waiting before to launch the next program..."
		done
		
		# Once all applications has been launched with a certain number of core, it removes (and it recreates these executables at the start of the main loop).
		echo "Cleaning..."
		eval "make veryclean"
	
	done
elif [ "$provider" = "g5k" ]; then

	for core in "${core_list[@]}"; do
	
		echo "Recreating..."
		eval "make suite"
	
		for program in "${program_list[@]}"; do
			echo "Program execution: $program with $core per node and $node node(s)"
			
			# Test launch
			res=$((core*node))
			command="mpirun -np $res --mca orte_rsh_agent \"oarsh\" -machinefile ../hostfolder/hostfile.ompi.g5k$node/hostfile.ompi$core --mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib --mca btl_tcp_if_include enp24s0f0 bin/$program.C.x"
			#--mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib --mca btl_tcp_if_exclude ib0,lo
			#command="echo \"$command\""
			#command="sleep $core"
			eval "$command"
			
			# Checking the process return code
			if [ $? -eq 0 ]; then
				echo "$command: done with success."
			else
				echo "$command: something is wrong."
			fi
			
			echo "Waiting before to launch the next program..."
		done
		
		# Once all applications has been launched with a certain number of core, it removes (and it recreates these executables at the start of the main loop).
		echo "Cleaning..."
		eval "make veryclean"
	
	done
elif [ "$provider" = "g5k_b" ]; then

	echo "Recreating..."
	eval "make suite"

	for program in "${program_list[@]}"; do
		echo "Program execution: $program with $core per node and $node node(s)"

        # Test launch
        command="mpirun -np 1 --mca orte_rsh_agent \"oarsh\" -machinefile ../hostfolder/hostfile.ompi.g5k1/hostfile.ompi1 --mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib --mca btl_tcp_if_include enp24s0f0 bin/$program.C.x"
        #--mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib --mca btl_tcp_if_exclude ib0,lo
        #command="echo \"$command\""
        #command="sleep $core"
        eval "$command"

        # Checking the process return code
        if [ $? -eq 0 ]; then
                echo "$command: done with success."
        else
                echo "$command: something is wrong."
        fi

        echo "Waiting before to launch the next program..."
    done

    # Once all applications has been launched with a certain number of core, it removes (and it recreates these executables at the start of the main loop).
    echo "Cleaning..."
    eval "make veryclean"

elif [ "$provider" = "aws_b" ]; then

	echo "Recreating..."
	eval "make suite"

	for program in "${program_list[@]}"; do
		echo "Program execution: $program with $core per node and $node node(s)"

        # Test launch
        command="mpirun -np 1 --hostfile ../hostfolder/hostfile.ompi.aws1/hostfile.ompi1 bin/$program.C.x"
        #command="echo \"$command\""
        #command="sleep $core"
        eval "$command"

        # Checking the process return code
        if [ $? -eq 0 ]; then
                echo "$command: done with success."
        else
                echo "$command: something is wrong."
        fi

        echo "Waiting before to launch the next program..."
    done

    # Once all applications has been launched with a certain number of core, it removes (and it recreates these executables at the start of the main loop).
    echo "Cleaning..."
    eval "make veryclean"

else
	eval "echo \"Error: Wrong argument -> [aws or g5k] [#node].\""
	exit 1
fi
