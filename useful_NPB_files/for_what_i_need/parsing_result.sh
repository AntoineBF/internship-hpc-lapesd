#!/bin/bash

# Check the number of argument.
if [ $# -ne 2 ]; then
    echo "Usage: $0 input_file output_file"
    exit 1
fi

# Receive input and output file names.
input_file="$1"
output_file="$2"
tmp_file="TmP.txt"
tmp2_file="TmP2.txt"
classe=("IS" "EP" "CG" "MG" "FT")

# Check if the input file exists.
if [ ! -f "$input_file" ]; then
    echo "The input file '$input_file' doesn't exist."
    exit 1
fi

# Browse the input file line by line.
while IFS= read -r line; do
    # Chech if the input file contains one of wished expressions.
    if [[ "$line" == *"Benchmark Completed"* ]] || [[ "$line" == *"Total processes ="* ]] || [[ "$line" == *"Time in seconds ="* ]] || [[ "$line" == *"Mop/s total"* ]] || [[ "$line" == *"Mop/s/process"* ]]; then
        # Add the linge to the output file.
        echo "$line" >> "$tmp_file"
    fi
done < "$input_file"

eval "tr -s '\n Time in seconds =' < $tmp_file > $tmp2_file"

sed -i 's/ Benchmark Completed.//g' "$tmp2_file"
sed -i 's/ Benchmark Completed//g' "$tmp2_file"
sed -i ':a;N;$!ba;s/\n Total proceses = / | /g' "$tmp2_file"
sed -i ':a;N;$!ba;s/\n Time in seconds = / | /g' "$tmp2_file"
sed -i ':a;N;$!ba;s/\n Mop\/s\/proces = / | /g' "$tmp2_file"
sed -i ':a;N;$!ba;s/\n Mop\/s total = / | /g' "$tmp2_file"
sed -i 's/$/ | /' "$tmp2_file"

for expr in "${classe[@]}"; do
    while IFS= read -r line; do
        # Chech if the input file contains one of wished expressions.
        if [[ "$line" == *"$expr"* ]]; then
            # Add the linge to the output file.
            echo "$line" >> "$output_file"
        fi
    done < "$tmp2_file"
done

eval "rm -f $tmp_file $tmp2_file "

echo "The parsing is done. output file: $output_file."
