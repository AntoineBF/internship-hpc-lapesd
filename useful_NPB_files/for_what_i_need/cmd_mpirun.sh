#!/bin/bash

# This script allows us to create our mpirun command line.
# It displays the template if no arguments are entered. 

if [ -z "$1" ]; then
	echo "bash cmd.sh <number of core> <number of node/instance> <name of application> <aws or g5k>"
	exit 1
fi

core=$1
node=$2
program=$3
provider=$4

res=$((core*node))

command="mpirun -np $res --hostfile ../hostfolder/hostfile.ompi.$provider$node/hostfile.ompi$core bin/$program.C.x"

eval "echo \"For AWS: $command\""

command="mpirun -np $res --mca orte_rsh_agent \"oarsh\" -machinefile ../hostfolder/hostfile.ompi.$provider$node/hostfile.ompi$core --mca pml ^ucx --mca mtl ^psm2,ofi --mca btl ^ofi,openib --mca btl_tcp_if_include enp24s0f0 bin/$program.C.x"
#../hostfolder/machinefile$node/machinefile$core

eval "echo \"For G5K: $command\""
