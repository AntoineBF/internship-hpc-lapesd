#!/bin/bash

#provider=$1 #useless
node=$1
file=$2
repetition=$3

#addrhost=${ip addr show | grep -oP '(?<=inet\s)\d+(\.\d+){3}' | grep -v '127.0.0.1'}
#echo "$addrhost"

if [ "$#" -lt 2 ]; then
	#echo "Error: Argument is empty -> [aws or g5k] [#node] [output_file]."
	echo "Error: Argument is empty -> [#node] [output_file] (#repetition)."
	exit 1
fi

# Check if $3 is empty
if [ -z "$3" ]; then
    repetition=1
fi

echo "\$$repetition repetition(s)"

clean_and_recreate() {
	echo "Cleaning..."
	eval "make veryclean"
	echo "Recreating..."
	eval "make suite"
}

run_with_hosts() {
	local node=$1
	local core=$2
	local app=$3
	local np=$((core*node))
	eval "mpirun -np $np --hostfile ../hostfolder/hostfile.ompi.aws$node/hostfile.ompi$core bin/$app.C.x >> $file.txt"
}

for ((i=1; i<=$repetition; i++)); do
	case $node in
		1)
			clean_and_recreate
			apps=("is" "cg" "ft" "ep" "mg")
			for app in "${apps[@]}"; do
				run_with_hosts 1 1 "$app"
			done
			clean_and_recreate
			apps=("is" "cg" "ft" "ep" "mg")
			for app in "${apps[@]}"; do
				run_with_hosts 1 2 "$app"
			done
			clean_and_recreate
			apps=("cg" "ep")
			for app in "${apps[@]}"; do
				run_with_hosts 1 4 "$app"
			done
			clean_and_recreate
			run_with_hosts 1 8 "ep"
			clean_and_recreate
			apps=("ep" "mg")
			for app in "${apps[@]}"; do
				run_with_hosts 1 16 "$app"
			done

			;;
		2)
			clean_and_recreate
			apps=("is" "ft" "mg")
			for app in "${apps[@]}"; do
				run_with_hosts 2 2 "$app"
			done

			clean_and_recreate
			apps=("is" "ft" "mg")
			for app in "${apps[@]}"; do
				run_with_hosts 2 4 "$app"
			done

			clean_and_recreate
			apps=("is" "ft" "cg")
			for app in "${apps[@]}"; do
				run_with_hosts 2 8 "$app"
			done

			clean_and_recreate
			apps=("is" "ft" "ep")
			for app in "${apps[@]}"; do
				run_with_hosts 2 16 "$app"
			done
			;;
		4)
			clean_and_recreate
			run_with_hosts 4 2 "cg"

			clean_and_recreate
			apps=("mg" "cg")
			for app in "${apps[@]}"; do
				run_with_hosts 4 8 "$app"
			done

			clean_and_recreate
			apps=("is" "ep" "ft" "mg")
			for app in "${apps[@]}"; do
				run_with_hosts 4 16 "$app"
			done
			;;
		8)
			clean_and_recreate
			run_with_hosts 8 8 "cg"

			clean_and_recreate
			apps=("is" "ft" "ep" "mg" "cg")
			for app in "${apps[@]}"; do
				run_with_hosts 8 16 "$app"
			done
			;;
		*)
			#echo "Error: Wrong argument -> [aws or g5k] [#node] [output_file]."
			echo "Error: Wrong argument -> [#node] [output_file] (#repetition)."
			exit 1
			;;
	esac
done

echo "Cleaning..."
eval "make veryclean"
